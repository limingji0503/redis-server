package org.redis.server.monitor;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.JmxReporter;
import com.codahale.metrics.MetricRegistry;
import org.redis.server.conf.Const;
import sun.management.Agent;

import java.util.concurrent.TimeUnit;

/**
 * @author roger on 15/12/31
 */
public class MetricsCenter {
    private final static MetricRegistry registry = new MetricRegistry();
    private final static ConsoleReporter consoleReporter = ConsoleReporter.forRegistry(registry).build();
    private final static JmxReporter jmxReporter = JmxReporter.forRegistry(registry).build();

    public static MetricRegistry get() {
        return registry;
    }

    public static void startConsoleReporter(long period, TimeUnit unit) {
        consoleReporter.start(period, unit);
    }

    public static void startJmxReporter() {
        jmxReporter.start();
    }

    public static void stopConsoleReported() {
        consoleReporter.close();
    }

    public static void stopJmxReporter() {
        jmxReporter.stop();
    }

    public static void startRemoteJmxServer() {
        final StringBuilder args = new StringBuilder();
        args.append("com.sun.management.jmxremote.port=").append(Const.REDIS_SERVER_JMX_REMOTE_PORT).append(",");
        args.append("com.sun.management.jmxremote.authenticate=false").append(",");
        args.append("com.sun.management.jmxremote.ssl=false").append(",");
        try {
            Agent.premain(args.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
