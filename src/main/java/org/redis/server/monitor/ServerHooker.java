package org.redis.server.monitor;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.Maps;
import org.redis.server.interceptor.Hooker;
import org.redis.server.server.RedisServer;
import org.redis.server.serverapi.IRedisCommands;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author roger on 15/12/31
 */
public class ServerHooker implements Hooker {
    private static MetricRegistry registry = MetricsCenter.get();
    private static Map<String, Meter> meters = Maps.newHashMap();

    static {
        Method[] methods = IRedisCommands.class.getMethods();
        for (Method method : methods) {
            meters.put(method.getName(),
                    registry.meter(MetricRegistry.name(RedisServer.class, method.getName())));
        }
    }

    @Override
    public void before(Object proxy, Method method, Object[] args) {
        // do nothing
    }

    @Override
    public void after(Object proxy, Method method, Object[] args) {
        Meter meter;
        if ((meter = meters.get(method.getName())) != null) {
            meter.mark();
        }
    }
}
