package org.redis.server.interceptor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class Interceptor implements InvocationHandler {
 
    Object obj;
	Hooker hooker;

    public Interceptor(Object obj, Hooker hooker) {
        this.obj = obj;
		this.hooker = hooker;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable {
        Object result = null;
        try {

            hooker.before(proxy, method, args);

            result = method.invoke(this.obj, args);

            hooker.after(proxy, method, args);

        } catch (InvocationTargetException e) {
            throw e.getCause();
        }

        return result;
    }

    public static Object newProxy(Object obj, Hooker hooker) {
    	return Proxy.newProxyInstance(obj.getClass().getClassLoader(), 
    			obj.getClass().getInterfaces(), new Interceptor(obj, hooker));
    }
}
