package org.redis.server.interceptor;

import java.lang.reflect.Method;

public interface Hooker {
	
	void before(Object proxy, Method method, Object[] args);
	
	void after(Object proxy, Method method, Object[] args);
	
}
