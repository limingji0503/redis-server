package org.redis.server.util;

import java.io.*;
import java.util.Map;
import java.util.Properties;

/**
 * 
 * @author LUOJIE
 */
public class Property {
	private volatile Properties config;
	private final String path;

	public Property(String path) {
		this.path = path;
		config = loadConfig(path);
	}

	private OutputStream getOutputStream(String configPath) {
		try {
			OutputStream output = null;
			if (new java.io.File(configPath).exists()) {
				output = new FileOutputStream(configPath);
			}
			return output;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String get(String key) {
		if (config == null) return null;
		return config.getProperty(key);
	}
	
	public void set(String key, String value) {
		config.setProperty(key, value);
	}
	
	public void load(Map<String, String> properties) {
		config.putAll(properties);
	}

	public void persistent(String writeLog) {
		OutputStream output = null;
		try {
			output = getOutputStream(path);
			if (output != null)
				config.store(output, writeLog);
		} catch (IOException ignored) {
		} finally {
			if (output!=null) {
				try {
					output.close();
				} catch (IOException ignored) {
				}
			}
		}
	}

	final Properties loadConfig(String filePath) {
		InputStream in;

		in = Property.class.getResourceAsStream(File.separator + filePath);
		Properties props = new Properties();
		try {
			if (new File(filePath).exists()) {
				in = new BufferedInputStream(new FileInputStream(filePath));
			}
			if (in == null) return null;
			props.load(in);
		} catch (IOException e) {
			return null;
		}

		return props;
	}
}
