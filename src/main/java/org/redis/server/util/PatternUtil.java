package org.redis.server.util;

import java.util.Set;

import com.google.common.collect.Sets;

/**
 * 
 * @author LUOJIE
 *
 */
public class PatternUtil {
	public static final char INTERROGATION = '?';
	public static final char ASTERISK = '*';
	public static final char L_SQUARE_BRANKETS = '[';
	public static final char R_SQUARE_BRANKETS = ']';
	public static final char HYPHEN = '-';

	public static boolean match(String element, String pattern) {

		int i = 0, j = 0;
		Set<Integer> set = Sets.newHashSet();

		while (i < element.length() && j < pattern.length()) {
			char ch = pattern.charAt(j);

			/**System.out.println(element.charAt(i) + "|" + pattern.charAt(j));**/

			if (INTERROGATION == ch) {
				i++;
				j++;
			} else if (ASTERISK == ch) {
				if (j != pattern.length() - 1) {
					if (pattern.charAt(j + 1) == ASTERISK)
						j++;
					if (pattern.charAt(j + 1) == element.charAt(i)) {
						j++;
						i--;
					}
				}
				i++;
			} else if (L_SQUARE_BRANKETS == ch) {
				j++;
				while (j < pattern.length()) {
					if (pattern.charAt(j) == R_SQUARE_BRANKETS)
						break;
					if (pattern.charAt(j) == HYPHEN) {
						int start = pattern.charAt(j - 1), end = pattern.charAt(j + 1);
						if (start > end || end == R_SQUARE_BRANKETS) return false;
						for (int k = start; k <= end; k++) set.add(k);
					}
					set.add((int) pattern.charAt(j));
					if (j++ == pattern.length() - 1)
						return false;
				}
			} else if (R_SQUARE_BRANKETS == ch) {
				if (set.contains((int) element.charAt(i))) {
					i++;
					j++;
				} else
					return false;
				set.clear();
			} else if (element.charAt(i) != ch) {
				return false;
			} else {
				i++;
				j++;
			}

			if (i == element.length() - 1 && j < pattern.length() - 1) {
				if (pattern.charAt(j) == L_SQUARE_BRANKETS)
					continue;
				if (pattern.charAt(j) == ASTERISK)
					continue;
				return false;
			}
		}

		return true;
	}

}
