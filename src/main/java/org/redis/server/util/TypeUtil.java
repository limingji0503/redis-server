package org.redis.server.util;

/**
 * Created by mingji on 9/12/17.
 */
public class TypeUtil {
    public static int compareStingObjects(Object obj1, Object obj2) {
        if (obj1 == obj2) {
            return 0;
        }
        if (obj1 == null || obj2 == null) {
            throw new NullPointerException("Object input is null object1: " + obj1 + " object2: " + obj2);
        }
        String s1 = (String) obj1;
        String s2 = (String) obj2;
        return s1.compareTo(s2);
    }
}
