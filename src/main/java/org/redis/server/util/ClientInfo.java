package org.redis.server.util;


import org.jboss.netty.channel.Channel;

/**
 * @author LuoJie
 */
public class ClientInfo {
    
    private static final ThreadLocal<ClientInfo> LOCAL = new ThreadLocal<ClientInfo>() {
        @Override
        protected ClientInfo initialValue() {
            return new ClientInfo();
        }
    };
    
    public static ClientInfo getCurrentClient() {
        return LOCAL.get();
    }
    
    public static void clear() {
        LOCAL.remove();
    }
    
    private String clientIp;
    private String clientPort;
    private String serverIp;
    private String serverPort;
    private Channel channel;

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getClientPort() {
        return clientPort;
    }

    public void setClientPort(String clientPort) {
        this.clientPort = clientPort;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public String getServerPort() {
        return serverPort;
    }

    public void setServerPort(String serverPort) {
        this.serverPort = serverPort;
    }


    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Channel getChannel() {

        return channel;
    }
    
    @Override
    public int hashCode() {
    	return super.hashCode();
    }
}
