package org.redis.server.util;

import java.net.InetSocketAddress;

import org.jboss.netty.channel.Channel;

/**
 * @author LuoJie
 *
 */
public class ClientInfoUtils {
    
    public static void record(Channel channel) {
        InetSocketAddress remoteAddress = (InetSocketAddress) channel.getRemoteAddress();
        InetSocketAddress localAddress = (InetSocketAddress) channel.getLocalAddress();
        
        ClientInfo client = ClientInfo.getCurrentClient();
        client.setClientIp(remoteAddress.getAddress().getHostAddress());
        client.setClientPort(String.valueOf(remoteAddress.getPort()));
        client.setServerIp(localAddress.getAddress().getHostAddress());
        client.setServerPort(String.valueOf(localAddress.getPort()));
        client.setChannel(channel);
    }

}
