package org.redis.server.util;

import com.google.common.collect.Maps;

import java.lang.Thread.State;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.*;

/**
 * A simple tool to execute a method as a thread.
 * @author LUOJIE
 */
public class Go {
	private static final OuterGo go = new OuterGo();
	private static final Map<String, Task> registry = Maps.newConcurrentMap();
	private static volatile ThreadPoolExecutor executor = null;

	public static synchronized OuterGo startThreadPool() {
		executor = (executor == null) ? new ThreadPoolExecutor(10, 600, 60L,
				TimeUnit.SECONDS, new LinkedTransferQueue<Runnable>()) : executor;
		return go;
	}

	public static OuterGo stopThreadPool() {
		executor.shutdownNow();
		registry.clear();
		executor = null;
		return go;
	}

	/**
	 * Run a static method of a class as a thread
	 * @param threadName name of thread
	 * @param clazz class type of the method
	 * @param method method name
	 * @return
	 * @throws Exception
	 */
	public static OuterGo x(String threadName, Class<?> clazz, String method) throws Exception {
		if (registry.containsKey(threadName))
			throw new Exception("Registry error! There is already a thread named: " + threadName);
		
		Task t = new Task(threadName, clazz, method);
		if (executor != null) {
			executor.submit(t);
		} else t.start();
		Exception e = null;
		if ((e = t.getException()) !=null)
			throw new Exception(e);
		return go;
	}
	
	/**
	 * Run a member method of a class as a thread
	 * @param threadName name of thread
	 * @param obj the object of the method
	 * @param method method name
	 * @return
	 * @throws Exception
	 */
	public static OuterGo x(String threadName, Object obj, String method) throws Exception {
		if (registry.containsKey(threadName))
			throw new Exception("Registry error! There is already a thread named: " + threadName);
		
		Task t = new Task(threadName, obj, method);
		if (executor != null) {
			executor.submit(t);
		} else t.start();
		Exception e = null;
		if ((e = t.getException()) !=null)
			throw new Exception(e);
		return go;
	}
	
	/**
	 * Run a static method of a class as a thread
	 * @param threadName name of thread
	 * @param clazz class type of the method
	 * @param method method name
	 * @param para parameters of the method you invoke
	 * @return
	 * @throws Exception
	 */
	public static OuterGo x(String threadName, Class<?> clazz, String method, Object... para) throws Exception {
		if (registry.containsKey(threadName))
			throw new Exception("Registry error! There is already a thread named: " + threadName);
		
		Task t = new Task(threadName, clazz, method, para);
		if (executor != null) {
			executor.submit(t);
		} else t.start();
		Exception e = null;
		if ((e = t.getException()) !=null)
			throw new Exception(e);
		return go;
	}
	
	/**
	 * Run a member method of a class as a thread
	 * @param threadName name of thread
	 * @param obj object of the method
	 * @param method method name
	 * @param para parameters of the method you invoke
	 * @return
	 * @throws Exception
	 */
	public static OuterGo x(String threadName, Object obj, String method, Object... para) throws Exception {
		if (registry.containsKey(threadName))
			throw new Exception("Registry error! There is already a thread named: " + threadName);
		
		Task t = new Task(threadName, obj, method, para);
		if (executor != null) {
			executor.submit(t);
		} else t.start();
		Exception e = null;
		if ((e = t.getException()) !=null)
			throw new Exception(e);
		return go;
	}
	
	
	/**
	 * Run a static method of a class as a daemon thread
	 * @param threadName name of thread
	 * @param clazz class type of the method
	 * @param method method name
	 * @return
	 * @throws Exception
	 */
	public static OuterGo xDaemon(String threadName, Class<?> clazz, String method) throws Exception {
		if (registry.containsKey(threadName))
			throw new Exception("Registry error! There is already a thread named: " + threadName);
		
		Task t = new Task(threadName, clazz, method);
		t.daemon(true);
		if (executor != null) {
			executor.submit(t);
		} else t.start();
		Exception e = null;
		if ((e = t.getException()) !=null)
			throw new Exception(e);
		return go;
	}
	
	/**
	 * Run a member method of a class as a daemon thread
	 * @param threadName name of thread
	 * @param obj the object of the method
	 * @param method method name
	 * @return
	 * @throws Exception
	 */
	public static OuterGo xDaemon(String threadName, Object obj, String method) throws Exception {
		if (registry.containsKey(threadName))
			throw new Exception("Registry error! There is already a thread named: " + threadName);
		
		Task t = new Task(threadName, obj, method);
		t.daemon(true);
		if (executor != null) {
			executor.submit(t);
		} else t.start();
		Exception e = null;
		if ((e = t.getException()) !=null)
			throw new Exception(e);
		
		return go;
	}
	
	/**
	 * Run a static method of a class as a daemon thread
	 * @param threadName name of thread
	 * @param clazz class type of the method
	 * @param method method name
	 * @param para parameters of the method you invoke
	 * @return
	 * @throws Exception
	 */
	public static OuterGo xDaemon(String threadName, Class<?> clazz, String method, Object... para) throws Exception {
		if (registry.containsKey(threadName))
			throw new Exception("Registry error! There is already a thread named: " + threadName);
		
		Task t = new Task(threadName, clazz, method, para);
		t.daemon(true);
		if (executor != null) {
			executor.submit(t);
		} else t.start();
		Exception e = null;
		if ((e = t.getException()) !=null)
			throw new Exception(e);
		return go;
	}
	
	/**
	 * Run a member method of a class as a daemon thread
	 * @param threadName name of thread
	 * @param obj object of the method
	 * @param method method name
	 * @param para parameters of the method you invoke
	 * @return
	 * @throws Exception
	 */
	public static OuterGo xDaemon(String threadName, Object obj, String method, Object... para) throws Exception {
		if (registry.containsKey(threadName))
			throw new Exception("Registry error! There is already a thread named: " + threadName);
		
		Task t = new Task(threadName, obj, method, para);
		t.daemon(true);
		if (executor != null) {
			executor.submit(t);
		} else t.start();
		Exception e = null;
		if ((e = t.getException()) !=null)
			throw new Exception(e);
		
		return go;
	}
	
	public static OuterGo inturrupt(String threadName) {
		if (registry.containsKey(threadName)) {
			registry.get(threadName).interrupt();
		}
		return go;
	}
	
	public static Thread getThread(String threadName) {
		if (registry.containsKey(threadName)) {
			return registry.get(threadName);
		}
		return null;
	}
	
	public static State state(String threadName) {
		if (registry.containsKey(threadName)) {
			return registry.get(threadName).getState();
		}
		return null;
	}
	
	public static class OuterGo {
		
		private OuterGo() {
			
		}
		
		public OuterGo x(String threadName, Class<?> clazz, String method) throws Exception {
			return Go.x(threadName, clazz, method);
		}
		
		public OuterGo x(String threadName, Object obj, String method) throws Exception {
			return Go.x(threadName, obj, method);
		}
		
		public OuterGo x(String threadName, Class<?> clazz, String method, Object... para) throws Exception {
			return Go.x(threadName, clazz, method, para);
		}
		
		public OuterGo x(String threadName, Object obj, String method, Object... para) throws Exception {
			return Go.x(threadName, obj, method, para);
		}
		
		public OuterGo xDaemon(String threadName, Class<?> clazz, String method) throws Exception {
			return Go.xDaemon(threadName, clazz, method);
		}
		
		public OuterGo xDaemon(String threadName, Object obj, String method) throws Exception {
			return Go.xDaemon(threadName, obj, method);
		}
		
		public OuterGo inturrpt(String threadName) {
			return Go.inturrupt(threadName);
		}
	}

	public static class Task extends Thread {
		final Object o;
		final Method m;
		final Class<?> clazz;
		final String name;
		final Object[] para;
		
		private volatile Exception err;
		
		public Task(String threadName, Class<?> clazz, String method) throws Exception {
			this(threadName, clazz, method, new Object[0]);
		}

		public Task(String threadName, Object obj, String method) throws Exception {
			this(threadName, obj, method, new Object[0]);
		}
		
		public Task(String threadName, Class<?> clazz, String method, Object[] para) throws Exception {
			this.para = para;
			this.name = threadName;
			this.clazz = clazz;
			this.err = null;
			Class<?>[] clazzs = new Class<?>[para.length];
			
			int i = 0;
			for (Object o : para) {
				clazzs[i++] = o.getClass();
			}
			
			m = clazz.getMethod(method, clazzs);
			o = null; 

			setName(name);

			if (name != null)
				registry.put(name, this);
		}
		
		public Task(String threadName, Object obj, String method, Object[] para) throws Exception {
			this.para = para;
			this.name = threadName;
			this.clazz = null;
			Class<?>[] clazzs = new Class<?>[para.length];
			
			int i = 0;
			for (Object o : para) {
				clazzs[i++] = o.getClass();
			}
			
			m = obj.getClass().getMethod(method, clazzs);
			o = obj; 

			setName(name);

			if (name != null)
				registry.put(name, this);
		}

		public void run() {
			try {
				if (o != null)
					m.invoke(o, para);
				else if (clazz != null)
					m.invoke(clazz, para);
			} catch (Exception e) {
				err = e;
			} finally {
				if (name != null)
					registry.remove(name);
			}
		}


		public Exception getException() {
			return err;
		}
		
		public Task setException(Exception e) {
			err = e;
			return this;
		}
		
		public Task name(String name) {
			setName(name);
			return this;
		}
		
		public Task daemon(boolean on) {
			setDaemon(on);
			return this;
		}
		
		public Task interruptTask() {
			interrupt();
			return this;
		}
	}
}
