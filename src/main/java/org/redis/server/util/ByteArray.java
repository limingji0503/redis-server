package org.redis.server.util;

import java.util.List;

/**
 *
 * @author LUOJIE
 */
public class ByteArray {
    static final String NUMBER_PATTERN = "[0-9]+";
    byte[] bArray;

    public ByteArray() {
        bArray = new byte[0];
    }

    public ByteArray(byte[] bArray) {
        this.bArray = bArray;
    }

    public byte[] toBytes() {
        return bArray;
    }

    public String toString() {
        return new String(bArray);
    }

    public ByteArray append(final byte a) {
        byte[] a0 = new byte[1];
        a0[0] = a;
        bArray = add(bArray, a0, new byte[0]);
        return this;
    }

    public ByteArray append(final byte[] a) {
        bArray = add(bArray, a, new byte[0]);
        return this;
    }

    public ByteArray append(final List<byte[]> bytes) {
        for (byte[] b : bytes) {
            bArray = add(bArray, b, new byte[0]);
        }
        return this;
    }

    public ByteArray append(final byte[] a, final byte[] b) {
        bArray = add(bArray, a, b);
        return this;
    }

    public static byte[] add(final byte[] a, final byte[] b, final byte[] c) {
        byte[] result = new byte[a.length + b.length + c.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        System.arraycopy(c, 0, result, a.length + b.length, c.length);
        return result;
    }

    public static byte[] intToBytes(int n) {
        return longToBytes((long)n);
    }

    public static byte[] longToBytes(long n) {
        int size = 0;
        long buf = n;

        if (n == 0) {
            size = 1;
        } else for (; buf !=0; size++) {
            buf /= 10;
        }
        if (n < 0) {
            buf = n * -1;
            size++;
        } else {
            buf = n;
        }

        byte[] b = new byte[size];
        for (int i = 1; i <= size; i++) {
            b[size - i] = (byte)((buf % 10) + 48);
            buf /= 10;
        }

        if (n < 0) b[0] = 45;
        return b;
    }

    public static int bytesToInt(byte[] bytes) {
        return (int)bytesTolong(bytes);
    }

    public static long bytesTolong(byte[] bytes) {
        long i = 0;
        boolean isNegative = false;
        for (int j = 0; j < bytes.length; j++) {
            if (j == 0 && bytes[j] == 45) {
                isNegative = true;
                continue;
            }
            if (bytes[j] < 48 || bytes[j] > 57) {
                throw new NumberFormatException("byte element is not a number: " + new Character((char)bytes[j]));
            }
            i = 10 * i + bytes[j] - 48;
        }
        return isNegative ? -1 * i : i;
    }

    public static boolean isNumber(byte[] bytes) {
        for (int j = 0; j < bytes.length; j++) {
            if (j == 0 && bytes[j] == 45) {
                continue;
            }
            if (bytes[j] < 48 || bytes[j] > 57) {
                return false;
            }
        }
        return true;
    }

    public static byte[][] appendList(byte[] first, List<byte[]> list) {
        byte[][] dyadicBytes = new byte[list.size() + 1][];
        dyadicBytes[0] = first;
        int i = 1;
        for (byte[] bytes : list) {
            dyadicBytes[i++] = bytes;
        }
        return dyadicBytes;
    }

    public static boolean isNumber(String str) {
        if (str != null) return str.matches(NUMBER_PATTERN);
        else return false;
    }
}