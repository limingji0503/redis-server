package org.redis.server.util;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedTransferQueue;

/**
 * This queue is used for excuting actions implemented by user,
 * and the actions will be excuted when queue size reach a threshold
 * or after the interval user gived.
 * @author roger on 15/12/28
 */
public abstract class AsyncActionsQueue<E> {
    private Queue<E> queue = new LinkedTransferQueue<>();
    private long size, interval;
    private volatile boolean alive;

    public AsyncActionsQueue(long size, long interval) {
        this.size = size;
        this.interval = interval;
        this.alive = true;
    }

    public abstract void action(List<E> list);

    public AsyncActionsQueue<E> start() {
        new Thread() {
            @Override
            public void run() {
                while (alive) {
                    List<E> elements = Lists.newArrayList();
                    long startTime = System.currentTimeMillis();
                    try {
                        long sleepTime = 0L;
                        for (;;) {
                            E element = queue.poll();
                            if (element == null) {
                                if (System.currentTimeMillis() - startTime > interval) {
                                    break;
                                }
                                if (sleepTime > 50L) sleepTime = 0L;
                                sleep(sleepTime += 10);
                                continue;
                            }
                            elements.add(element);
                            if (elements.size() > size) {
                                break;
                            }
                        }
                        action(elements);
                    } catch(InterruptedException ignored) {
                    }
                }
            }
        }.start();
        return this;
    }

    public void put(E e) {
        queue.offer(e);
    }

    public void stop() {
        alive = false;
    }

}
