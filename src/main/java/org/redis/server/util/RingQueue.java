package org.redis.server.util;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A thread-safe queue with locks as least as possible, which mainly
 * works through some atomic variables.It uses a ring buffer by array
 * to store data. Slighter performance degradation in high concurrency
 * situation.
 * 
 * @author LUOJIE
 * @version 1.0.0 2015-09-20
 * @param <T>
 */
public class RingQueue<T> {
	private AtomicLong consumerOffset;
	private AtomicLong producerOffset;
	private Wait wait;
	private volatile T[] buffer;
	private int capacity;

	public RingQueue() {
		this(Short.MAX_VALUE);
	}

	@SuppressWarnings("unchecked")
	public RingQueue(int capacity) {
		this.capacity = (capacity <= 0 || capacity > Short.MAX_VALUE)
				? Short.MAX_VALUE : capacity;
		buffer = (T[]) new Object[this.capacity];
		consumerOffset = new AtomicLong(0);
		producerOffset = new AtomicLong(0);
		wait = new Wait();
	}

	/**
	 * A encapsulation of java lock by condition.
	 * Used for achieving producer-consumer queue.
	 * 
	 * @author LUOJIE
	 * @version 1.0.0 2015-09-20
	 * 
	 */
	public static class Wait {
		private AtomicBoolean consumerBlocked;
		private AtomicBoolean producerBlocked;
		private ReentrantLock lock;
		private Condition waitForConsumer;
		private Condition waitForProducer;
		
		public Wait() {
			consumerBlocked = new AtomicBoolean(false);
			producerBlocked = new AtomicBoolean(false);
			lock = new ReentrantLock();
			waitForConsumer = lock.newCondition();
			waitForProducer = lock.newCondition();
		}
		
		/**
		 * Causes current consumer thread to wait until
		 * the specified producer thread to signaled or
		 * it is interrupted.
		 * 
		 * @throws InterruptedException
		 */
		public void waitForConsumer() throws InterruptedException {
			try {
				lock.lockInterruptibly();
				// to avoid dead lock
				if (!isConsumerBlocked()) {
					producerBlocked.set(true);
					waitForConsumer.await();
				}
			} finally {
				lock.unlock();
			}
		}
		
		/**
		 * Causes current producer thread to wait until
		 * the specified consumer thread to signaled or
		 * it is interrupted.
		 * 
		 * @throws InterruptedException
		 */
		public void waitForProducer() throws InterruptedException {
			try {
				lock.lockInterruptibly();
				// to avoid dead lock
				if (!isProducerBlocked()) {
					consumerBlocked.set(true);
					waitForProducer.await();
				}
			} finally {
				lock.unlock();
			}
		}
		
		/*
		 * Wakes up all the waiting consumer threads.
		 */
		public void wakeConsumer() {
			try {
				lock.lock();
				waitForProducer.signalAll();
				consumerBlocked.set(false);
			} finally {
				lock.unlock();
			}
		}
		
		/**
		 * Wakes up all the waiting producer threads.
		 */
		public void wakeProducer() {
			try {
				lock.lock();
				waitForConsumer.signalAll();
				producerBlocked.set(false);
			} finally {
				lock.unlock();
			}
		}
		
		/**
		 * Returns {@code true} if the producer threads
		 * is waiting.
		 * 
		 * @return {@code true} if the producer threads
		 * is waiting.
		 */
		public boolean isProducerBlocked() {
			return producerBlocked.get();
		}
		
		/**
		 * Returns {@code true} if the consumer threads
		 * is waiting.
		 * 
		 * @return {@code true} if the consumer threads
		 * is waiting.
		 */
		public boolean isConsumerBlocked() {
			return consumerBlocked.get();
		}
	}

	private int waitForConsumerOffset() {
		int i = (int) (consumerOffset.getAndIncrement() % capacity);
		return i;
	}

	private int waitForProducerOffset() {
		int i = (int) (producerOffset.getAndIncrement() % capacity);
		return i;
	}

	/**
	 * Get and remove the value from head, it is
	 * a no-blocked method. It will return null while
	 * the queue is empty or there's not a value at
	 * current index.
	 */
	public T poll() {
		if (remainingCapacity() <= 0) {
			if (wait.isProducerBlocked()) {
				wait.wakeProducer();
			}
			return null;
		}

		int offset = waitForConsumerOffset();
		T value = buffer[offset];
		if (value != null) {
			buffer[offset] = null;
		}
		if (wait.isProducerBlocked()) {
			wait.wakeProducer();
		}
		return value;
	}

	/**
	 * Get and remove the value from head. If
	 * the queue is empty, the method will be 
	 * blocked until producer puts any value 
	 * into the queue.  
	 * @throws InterruptedException
	 */
	public T pop() throws InterruptedException {
		if (remainingCapacity() <= 0) {
			wait.waitForProducer();
		}
		
		return poll();
	}

	/**
     * Inserts the specified element at the tail 
     * of this queue. Inserts failed when there's 
     * no space available.
     * 
	 * 
	 * @param t
	 * @return b If the value has been inserted
	 * into the queue.
	 * @throws NullPointerException
	 */
	public boolean put(T t) {
		if (t == null) throw new NullPointerException();
		
		int offset = waitForProducerOffset();
		if (buffer[offset] != null) {
			if (wait.isConsumerBlocked()) {
				wait.wakeConsumer();
			}
			return false;
		}
		buffer[offset] = t;
		if (wait.isConsumerBlocked()) {
			wait.wakeConsumer();
		}
		return true;
	}

	/**
     * Inserts the specified element at the tail 
     * of this queue, waiting if necessary for 
     * space to become available.
	 * 
	 * @param t
	 * @throws NullPointerException, InterruptedException
	 */
	public void push(T t) throws InterruptedException {
		if (remainingCapacity() >= capacity) {
			wait.waitForConsumer();
		}

		put(t);
	}

	/**
     * Returns the number of additional elements that this queue can ideally
     * (in the absence of memory or resource constraints) accept without
     * blocking. This is always equal to the initial capacity of this queue
     * less the current {@code size} of this queue.
     */
	public int remainingCapacity() {
		return (int) (producerOffset.get() - consumerOffset.get());
	}

	@Override
	public String toString() {
        StringBuffer result = new StringBuffer("[");
        if (buffer.length == 0) {
            result.append("]");
            return result.toString();
        }
        result.append(buffer[0]);

        if (buffer.length > 0)
            for (int i = 1; i < buffer.length; i++) {
                result.append(", " + buffer[i]);
            }
        result.append("]");
        return result.toString();
    }

	/**
	 * Return the {@code capacity} of this queue.
	 * @return the {@code capacity} of this queue.
	 */
	public int capacity() {
		return capacity;
	}
}