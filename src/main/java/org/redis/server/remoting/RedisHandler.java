package org.redis.server.remoting;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.jboss.netty.channel.*;
import org.redis.server.handlers.CommandHandler;
import org.redis.server.handlers.HandlerMap;
import org.redis.server.util.ClientInfo;
import org.redis.server.exception.NotImplementedCommandException;
import org.redis.server.protocol.Command;
import org.redis.server.protocol.Request;
import org.redis.server.protocol.Response;
import org.redis.server.serverapi.AbstractServer;
import org.redis.server.util.ClientInfoUtils;
import org.redis.server.util.LogUtils;
import redis.clients.util.SafeEncoder;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;

/**
 *  @author LuoJie
 */
public abstract class RedisHandler extends SimpleChannelUpstreamHandler {

    private Logger logger = Logger.getLogger(RedisHandler.class);
	protected AbstractServer redisServer;
    private final ExecutorService executor;
    protected HandlerMap handlerMap;

    public RedisHandler(AbstractServer redisServer, ExecutorService executor, HandlerMap handlerMap) {
        this.redisServer = redisServer;
        this.executor = executor;
        this.handlerMap = handlerMap;
    }

    public HandlerMap getHandlerMap() {
        return handlerMap;
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, final MessageEvent e) throws Exception {
        final Channel channel = ctx.getChannel();
        try {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    ClientInfoUtils.record(channel);

                    Request request = (Request) e.getMessage();
                    Response response = new Response();
                    try {
                        Command cmd = request.getCommand();
                        if (cmd == null) {
                            response.setType(Response.Type.ERROR);
                            response.addValue("unknown command.");
                        } else {
                            byte[] key = request.getKey();
                            List<byte[]> values = request.getValues();

                            boolean cmdFound = false;
                            CommandHandler handler = handlerMap.getHandler(cmd);
                            if (handler != null) {
                                cmdFound = handler.handleCommands(cmd, key, values, response);
                            }
                            if (!cmdFound) {
                                response.setType(Response.Type.SINGLE);
                                response.addValue("Command " + cmd + " not found!");
                            } else {
                                redisServer.aof(request);
                            }
                        }
                    } catch (Exception e) {
                        response.setType(Response.Type.ERROR);
                        if (e instanceof NotImplementedCommandException) {
                            response.addValue(request.getCommand() + " not implemented yet!");
                        } else {
                            response.addValue(ExceptionUtils.getMessage(e));
                        }
                    } finally {
                        if (channel.isOpen()) {
                            channel.write(response);
                        }
                        ClientInfo.clear();
                    }

                }
            });
        } catch (RejectedExecutionException ex) {
            Response response = new Response();
            response.setType(Response.Type.ERROR);
            response.addValue("Redis Biz thread pool is full!");
            channel.write(response);
        }
    }

    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        InetSocketAddress remoteSocket = (InetSocketAddress) ctx.getChannel().getRemoteAddress();
        InetAddress address = remoteSocket.getAddress();
        String remoteIp = address == null ? "unresolved" : address.getHostAddress();
        int port = remoteSocket.getPort();
        logger.error("Redis protocol error! Request from [" + remoteIp + ":" + port + "]", e.getCause());

        Throwable cause = e.getCause();
        if (cause instanceof IOException) {
            LogUtils.console(cause);
            return;
        }
        ctx.getChannel().write(new Response(Response.Type.ERROR).addValue(cause.getMessage()));
    }

    protected String encode(byte[] data) {
        return SafeEncoder.encode(data);
    }
}
