package org.redis.server.remoting;

import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import org.redis.server.protocol.Request;

/**
 * @author LuoJie
 */
public class RedisDecoder extends FrameDecoder {
    
    private static Logger logger = Logger.getLogger(RedisDecoder.class);
    
    private static final byte ASTERISK = '*';
    private static final byte DOLLAR = '$';
    private static final byte SEP_LF = '\n';
    
    private static final int WAIT_FOR_REQUEST = 1;
    private static final int WAIT_FOR_FIELD = 2;
    private static final int WAIT_FOR_CONTENT = 3;
    
    private Request request;
    private int status = WAIT_FOR_REQUEST;
    
    /**
                        status
                        1
     *3     a request   2
     $3     a field     3
     SET    a content   2
     $5                 3
     mykey              2
     $7                 3
     myvalue            1
     
    */

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer) throws Exception {
        
        if (buffer.readableBytes() < 1) {
            return null;
        }
        
        if (status == WAIT_FOR_REQUEST) {
            return processRequest(buffer);
        }
        else if (status == WAIT_FOR_FIELD) {
            return processField(buffer);
        }
        else if (status == WAIT_FOR_CONTENT) {
            return processContent(buffer);
        }
        
        return null;
    }
    
    public Object processRequest(ChannelBuffer buffer) {
        
        buffer.markReaderIndex();
        
        if (buffer.readByte() != ASTERISK) {
            logger.debug("processRequest error, * not found, skip bytes");
            return resetFrame(buffer);
        }
        
        try {
            int fieldNum = readLength(buffer);
            if (fieldNum < 0) {
                buffer.resetReaderIndex();
                return null;
            }
            request = new Request();
            request.setFieldNum(fieldNum);
            
            status = WAIT_FOR_FIELD;
        } catch (Exception e) {
            logger.error("processRequest error", e);
        }
        return null;
    }
    
    public Object processField(ChannelBuffer buffer) {
        
        buffer.markReaderIndex();
        
        if (buffer.readByte() != DOLLAR) {
            logger.error("processField error, $ not found, skip bytes");
            return resetFrame(buffer);
        }
        
        try {
            int fieldLength = readLength(buffer);
            if (fieldLength < 0) {
                buffer.resetReaderIndex();
                return null;
            }
            request.setNextFieldLength(fieldLength);
            
            status = WAIT_FOR_CONTENT;
        } catch (Exception e) {
            logger.error("processField error", e);
            return resetFrame(buffer);
        }
        return null;
    }
    
    public Object processContent(ChannelBuffer buffer) {
        int fieldLength = request.getNextFieldLength();
        
        // wait for more bytes
        if (buffer.readableBytes() < fieldLength + 2) {
            return null;
        }
        
        byte[] dest = new byte[fieldLength];
        buffer.readBytes(dest);
        buffer.readBytes(2);
        
        if (request.getFieldIndex() == 0) {
            String command = new String(dest);
            try {
                request.setCommand(command);
            } catch (Exception e) {
                logger.warn("unknown command " + command);
            }
        }
        else if (request.getFieldIndex() == 1) {
            request.setKey(dest);
        }
        else if (request.getFieldIndex() < request.getFieldNum()) {
            request.addValue(dest);
        }
        request.incrFieldIndex();
        
        if (request.getFieldIndex() == request.getFieldNum()) {
            status = WAIT_FOR_REQUEST;
            return request;
        }
        status = WAIT_FOR_FIELD;
        
        return null;
    }
    
    /**
     * read fieldNum or filedLength
     * @param buffer
     * @return
     *      -1 if not enough bytes received
     */
    private int readLength(ChannelBuffer buffer) {
        // read to \n
        int lfIndex = buffer.indexOf(buffer.readerIndex(), buffer.writerIndex(), SEP_LF);
        
        // wait for more bytes
        if (lfIndex < 0) {
            return -1;
        }
        
        byte[] dest = new byte[lfIndex - buffer.readerIndex() - 1];
        buffer.readBytes(dest);
        buffer.readBytes(2);
        
        return Integer.parseInt(new String(dest));
    }
    
    /**
     * reset to the initial state
     * @param buffer
     * @return
     */
    private Object resetFrame(ChannelBuffer buffer) {
        status = WAIT_FOR_REQUEST;
        return skipIllegalBytes(buffer);
    }
    
    /**
     * skip to the start of a request
     * @param buffer
     * @return
     */
    private Object skipIllegalBytes(ChannelBuffer buffer) {
        int startIndex = buffer.indexOf(buffer.readerIndex(), buffer.writerIndex(), ASTERISK);
        
        // skip all
        if (startIndex < 0) {
            buffer.skipBytes(buffer.readableBytes());
            return null;
        }
        
        buffer.skipBytes(startIndex - buffer.readerIndex());
        return null;
    }
    
}
