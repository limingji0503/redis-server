package org.redis.server.remoting;

import org.jboss.netty.buffer.ChannelBufferOutputStream;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.redis.server.protocol.PubSub;
import org.redis.server.util.ByteArray;
import org.redis.server.protocol.Response;

import java.util.List;

/**
 *  @author LuoJie
 */
public class RedisEncoder extends OneToOneEncoder {
    public static final byte PLUS = '+';
    public static final byte MINUS = '-';
    public static final byte COLON = ':';
    public static final byte ASTERISK = '*';
    public static final byte DOLLAR = '$';
    public static final byte[] MINUS_ONE = "-1".getBytes();
	public static final byte[] CRLF = "\r\n".getBytes();

    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
        
        ChannelBufferOutputStream bout = new ChannelBufferOutputStream(
                ChannelBuffers.dynamicBuffer(ctx.getChannel().getConfig().getBufferFactory()));
    	
    	Response response = (Response) msg;
    	List<byte[]> values = response.getValues();
		byte[] result;

    	try {
    		switch (response.getType()) {
    		case SINGLE:
    		    bout.write(PLUS);
    		    bout.write(values.get(0));
    		    bout.write(CRLF);
    			break;
    		case ERROR:
                bout.write(MINUS);
                bout.write(values.get(0));
                bout.write(CRLF);
    			break;
    		case INT:
                bout.write(COLON);
                bout.write(values.get(0));
                bout.write(CRLF);
    			break;
    		case MBULK:
				if ((result = PubSub.encode(values)) != null) {
					bout.write(result);
					break;
				}
                bout.write(ASTERISK);
                bout.write(ByteArray.intToBytes(values.size()));
                bout.write(CRLF);
    			// No break
    		case BULK:
    			for (byte[] val : values) {
    				if (val == null) {
    	                bout.write(DOLLAR);
    	                bout.write(MINUS_ONE);
    	                bout.write(CRLF);
    					continue;
    				}
    				bout.write(DOLLAR);
                    bout.write(ByteArray.intToBytes(val.length));
                    bout.write(CRLF);
                    bout.write(val);
                    bout.write(CRLF);
    			}
    			break;
			case SKIP:
				break;
			default:
			break;
    		}
			bout.flush();
		} finally {
			bout.close();
		}
    	
    	return bout.buffer();
    }
}
