package org.redis.server.server;

import com.google.common.collect.Lists;
import org.redis.server.aof.AOFScheduler;
import org.redis.server.conf.Const;
import org.redis.server.conf.RedisConfig;
import org.redis.server.exception.NotImplementedCommandException;
import org.redis.server.protocol.Request;
import org.redis.server.rpcimpl.BinaryHandler;
import org.redis.server.serverapi.AbstractServer;
import org.redis.server.type.RedisDB;
import org.redis.server.type.RedisList;
import org.redis.server.typeapi.BinaryRedisHash;
import org.redis.server.typeapi.BinaryRedisList;
import org.redis.server.typeapi.BinaryRedisSet;
import org.redis.server.typeapi.BinaryRedisString;
import org.redis.server.util.PatternUtil;
import redis.clients.jedis.BinaryClient;
import redis.clients.jedis.SortingParams;

import java.util.*;

/**
 * @author LuoJie
 */
public class RedisServer extends AbstractServer {
    private RedisDB db = new RedisDB();
    private AOFScheduler aofScheduler;

    public RedisServer(int port) {
        this(Collections.singletonList("0.0.0.0"), port);
        /** this(Arrays.asList("0.0.0.0"), port); */
    }

    public RedisServer(List<String> bindAddresses, int port) {
        super(bindAddresses, port, BinaryHandler.class);
        RedisConfig conf = RedisConfig.getConfig();
        aofScheduler = new AOFScheduler(super.handler.getHandlerMap(), db, conf);
    }

    @Override
    public byte[] info() {
        throw new NotImplementedCommandException();
    }

    @Override
    public byte[] auth() {
        throw new NotImplementedCommandException();
    }

    /**
     * string
     */
    @Override
    public List<byte[]> mget(byte[]... keys) {
        throw new NotImplementedCommandException();
    }

    @Override
    public String set(byte[] key, byte[] value) {
        BinaryRedisString string = db.getString(key, true);
        string.set(value);
        return "OK";
    }

    @Override
    public byte[] get(byte[] key) {
        BinaryRedisString string = db.getString(key, false);
        return string == null ? null : string.get().getBytes();
    }

    @Override
    public String mset(byte[]... keysvalues) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long msetnx(byte[]... keysvalues) {
        throw new NotImplementedCommandException();
    }

    @Override
    public byte[] getSet(byte[] key, byte[] value) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long strlen(byte[] key) {
        BinaryRedisString string = db.getString(key, false);
        return string == null ? 0 : string.strlen();
    }

    @Override
    public long setnx(byte[] key, byte[] value) {
        throw new NotImplementedCommandException();
    }

    @Override
    public String setex(byte[] key, int seconds, byte[] value) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long decrBy(byte[] key, long integer) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long decr(byte[] key) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long incrBy(byte[] key, long integer) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long incr(byte[] key) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long append(byte[] key, byte[] value) {
        BinaryRedisString string = db.getString(key, true);
        return string.append(value);
    }

    @Override
    public byte[] substr(byte[] key, int start, int end) {
        throw new NotImplementedCommandException();
    }

    /**
     * hash
     */

    @Override
    public long hset(byte[] key, byte[] field, byte[] value) {
        BinaryRedisHash hash = db.getHash(key, true);
        return (hash == null) ? 0 : hash.hset(field, value) ? 1L : 0;
    }

    @Override
    public byte[] hget(byte[] key, byte[] field) {
        BinaryRedisHash hash = db.getHash(key, false);
        return (hash == null) ? new byte[0] : hash.hget(field);
    }

    @Override
    public long hsetnx(byte[] key, byte[] field, byte[] value) {
        BinaryRedisHash hash = db.getHash(key, false);
        return (hash == null) ? 0 : hash.hsetTnx(field, value) ? 1L : 0;
    }

    @Override
    public List<byte[]> hmget(byte[] key, byte[]... fields) {
        BinaryRedisHash hash = db.getHash(key, true);
        return (hash == null) ? new ArrayList<byte[]>() : hash.hmget(fields);
    }

    @Override
    public String hmset(byte[] key, byte[]... hash) {
        return null;
    }

    @Override
    public long hincrBy(byte[] key, byte[] field, long value) {
        BinaryRedisHash hash = db.getHash(key, false);
        return (hash == null) ? 0L : hash.hincrby(field, value).getLong();
    }

    @Override
    public Float hincrByFloat(byte[] key, byte[] field, float value) {
        BinaryRedisHash hash = db.getHash(key, false);
        return (hash == null) ? 0 : hash.hincrbyFloat(field, value).getFloat();
    }

    @Override
    public boolean hexists(byte[] key, byte[] field) {
        BinaryRedisHash hash = db.getHash(key, false);
        return (hash != null) && hash.hexists(field);
    }

    @Override
    public long hdel(byte[] key, byte[]... field) {
        BinaryRedisHash hash = db.getHash(key, false);
        return (hash == null) ? 0 : hash.hdel(field);
    }

    @Override
    public long hlen(byte[] key) {
        BinaryRedisHash hash = db.getHash(key, false);
        return (hash == null) ? 0 : hash.hlen();
    }

    @Override
    public Set<String> hkeys(byte[] key) {
        BinaryRedisHash hash = db.getHash(key, false);
        return (hash == null) ? new HashSet<String>() : hash.hkeys();
    }

    @Override
    public Collection<byte[]> hvals(byte[] key) {
        BinaryRedisHash hash = db.getHash(key, false);
        return (hash == null) ? new ArrayList<byte[]>() : hash.hvals();
    }

    @Override
    public List<byte[]> hgetAll(byte[] key) {
        BinaryRedisHash hash = db.getHash(key, false);
        return (hash == null) ? new ArrayList<byte[]>() : hash.hgetAll();
    }

    @Override
    public List<byte[]> hscan(byte[] key, byte[]... commands) {
        BinaryRedisHash hash = db.getHash(key, false);
        return (hash == null) ? new ArrayList<byte[]>() : hash.hscan(commands);
    }

    /**
     * list
     */

    @Override
    public long rpush(byte[] key, byte[]... string) {
        return db.getList(key, true).rpush(string);
    }

    @Override
    public long lpush(byte[] key, byte[]... string) {
        BinaryRedisList list = db.getList(key, true);
        return (list == null) ? -1 : list.lpush(string);
    }

    @Override
    public long llen(byte[] key) {
        BinaryRedisList list = db.getList(key, false);
        return (list == null) ? -1 : list.llen();
    }

    @Override
    public List<byte[]> lrange(byte[] key, int start, int end) {
        BinaryRedisList list = db.getList(key, false);
        return (list == null) ? new ArrayList<byte[]>(0) : list.lrange(start, end);
    }

    @Override
    public String ltrim(byte[] key, int start, int end) {
        throw new NotImplementedCommandException();
    }

    @Override
    public byte[] lindex(byte[] key, int index) {
        throw new NotImplementedCommandException();
    }

    @Override
    public String lset(byte[] key, int index, byte[] value) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long lrem(byte[] key, int count, byte[] value) {
        throw new NotImplementedCommandException();
    }

    @Override
    public byte[] lpop(byte[] key) {
        BinaryRedisList list = db.getList(key, false);
        return (list == null) ? new byte[0] : list.lpop();
    }

    @Override
    public byte[] blpop(byte[] key) {
        BinaryRedisList list = db.getList(key, false);
        return (list == null) ? new byte[0] : list.blpop(0);
    }

    @Override
    public byte[] rpop(byte[] key) {
        BinaryRedisList list = db.getList(key, false);
        return (list == null) ? new byte[0] : list.rpop();
    }

    @Override
    public long lpushx(byte[] key, byte[] string) {
        Object obj = db.get(key);
        if (!obj.getClass().equals(RedisList.class)) return 0L;
        byte[][] values = {string};
        return ((RedisList) obj).lpush(values);
    }

    @Override
    public long rpushx(byte[] key, byte[] string) {
        Object obj = db.get(key);
        if (!obj.getClass().equals(RedisList.class)) return 0L;
        byte[][] values = {string};
        return ((RedisList) obj).rpush(values);
    }

    @Override
    public long linsert(byte[] key, BinaryClient.LIST_POSITION where, byte[] pivot, byte[] value) {
        throw new NotImplementedCommandException();
    }

    /**
     * set
     */
    @Override
    public long sadd(byte[] key, byte[]... member) {
        return db.getSet(key, true).sadd(member);
    }

    @Override
    public Set<String> smembers(byte[] key) {
        BinaryRedisSet set = db.getSet(key, false);
        return set == null ? null : set.smemebers();
    }

    @Override
    public long srem(byte[] key, byte[]... member) {
        BinaryRedisSet set = db.getSet(key, false);
        if (set == null) return 0;
        else return set.srem(member);
    }

    @Override
    public byte[] spop(byte[] key) {
        BinaryRedisSet set = db.getSet(key, false);
        return set == null ? null : set.spop();
    }

    @Override
    public long scard(byte[] key) {
        BinaryRedisSet set = db.getSet(key, false);
        if (set == null) return 0;
        else return set.scard();
    }

    @Override
    public boolean sismember(byte[] key, byte[] member) {
        BinaryRedisSet set = db.getSet(key, false);
        return set == null ? false : set.sismember(member);
    }

    @Override
    public byte[] srandmember(byte[] key) {
        return new byte[0];
    }

    @Override
    public List<String> srandmember(byte[] key, int n, boolean isNoArgs) {
        BinaryRedisSet set = db.getSet(key, false);
        return set == null ? null : set.srandmember(n, isNoArgs);
    }

    @Override
    public void sclear(byte[] key) {
        BinaryRedisSet set = db.getSet(key, false);
        if (set != null) {
            set.sclear();
        }
    }

    /**
     * zset
     */

    @Override
    public long zadd(byte[] key, double score, byte[] member) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long zadd(byte[] key, Map<Double, byte[]> scoreMembers) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrange(byte[] key, int start, int end) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long zrem(byte[] key, byte[]... member) {
        throw new NotImplementedCommandException();
    }

    @Override
    public double zincrby(byte[] key, double score, byte[] member) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long zrank(byte[] key, byte[] member) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long zrevrank(byte[] key, byte[] member) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrevrange(byte[] key, int start, int end) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrangeWithScores(byte[] key, int start, int end) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrevrangeWithScores(byte[] key, int start, int end) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long zcard(byte[] key) {
        throw new NotImplementedCommandException();
    }

    @Override
    public double zscore(byte[] key, byte[] member) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long zcount(byte[] key, double min, double max) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long zcount(byte[] key, byte[] min, byte[] max) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrangeByScore(byte[] key, double min, double max) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrangeByScore(byte[] key, byte[] min, byte[] max) {
        return null;
    }

    @Override
    public List<byte[]> zrangeByScore(byte[] key, double min, double max, int offset, int count) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrangeByScoreWithScores(byte[] key, double min, double max) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrangeByScoreWithScores(byte[] key, double min, double max, int offset, int count) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrangeByScoreWithScores(byte[] key, byte[] min, byte[] max) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrangeByScoreWithScores(byte[] key, byte[] min, byte[] max, int offset, int count) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrevrangeByScore(byte[] key, double max, double min) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrevrangeByScore(byte[] key, double max, double min, int offset, int count) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrevrangeByScore(byte[] key, byte[] max, byte[] min) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrevrangeByScore(byte[] key, byte[] max, byte[] min, int offset, int count) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrevrangeByScoreWithScores(byte[] key, double max, double min) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrevrangeByScoreWithScores(byte[] key, double max, double min, int offset, int count) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrevrangeByScoreWithScores(byte[] key, byte[] max, byte[] min) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> zrevrangeByScoreWithScores(byte[] key, byte[] max, byte[] min, int offset, int count) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long zremrangeByRank(byte[] key, int start, int end) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long zremrangeByScore(byte[] key, double start, double end) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long zremrangeByScore(byte[] key, byte[] start, byte[] end) {
        throw new NotImplementedCommandException();
    }

    /**
     * key
     */

    @Override
    public List<byte[]> keys(byte[] key) {
        Set<String> keySet = db.getAllKeys();
        List<byte[]> res = Lists.newArrayList();
        String patterns = new String(key);
        for (String k : keySet) {
            if (PatternUtil.match(k, patterns))
                res.add(k.getBytes());
        }
        return res;
    }

    @Override
    public long del(byte[] key, List<byte[]> values) {
        long i = 0;
        i += db.delete(key) ? 1 : 0;
        if (values == null) return i;
        else for (byte[] val : values) {
            i += db.delete(val) ? 1 : 0;
        }
        return i;
    }

    @Override
    public long objectRefcount(byte[] key) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long objectIdletime(byte[] key) {
        throw new NotImplementedCommandException();
    }

    @Override
    public byte[] objectEncoding(byte[] key) {
        throw new NotImplementedCommandException();
    }

    @Override
    public boolean exists(byte[] key) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long persist(byte[] key) {
        return 0;
    }

    @Override
    public byte[] type(byte[] key) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long expire(byte[] key, int seconds) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long expireAt(byte[] key, long unixTime) {
        throw new NotImplementedCommandException();
    }

    @Override
    public long ttl(byte[] key) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> sort(byte[] key) {
        throw new NotImplementedCommandException();
    }

    @Override
    public List<byte[]> sort(byte[] key, SortingParams sortingParameters) {
        throw new NotImplementedCommandException();
    }

    @Override
    public void aof(Request request) {
        aofScheduler.acceptCmd(request);
    }

    @Override
    public void start() {
        aofScheduler.loadAOF();
        aofScheduler.startAOF();
        super.start();
    }

    public static void main(String[] args) {
        String val = RedisConfig.getConfig().get(Const.REDIS_SERVER_PORT);
        int port = val == null ? Const.REDIS_SERVER_DEFAULT_PORT : Integer.valueOf(val);
        new RedisServer(port).start();
    }

}
