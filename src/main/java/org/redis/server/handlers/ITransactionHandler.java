package org.redis.server.handlers;

import org.redis.server.protocol.Command;
import org.redis.server.protocol.Response;

import java.util.List;

/**
 * @author LuoJie
 * @date 2015/11/15.
 */
public abstract class ITransactionHandler implements CommandHandler {
    @Override
    public boolean handleCommands(Command cmd, byte[] key, List<byte[]> values, Response response) {
        return handleTransactionCommands(cmd, key, values, response);
    }

    protected abstract boolean handleTransactionCommands(Command cmd, byte[] key, List<byte[]> values, Response response);
}
