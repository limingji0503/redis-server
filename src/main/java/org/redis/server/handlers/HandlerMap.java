package org.redis.server.handlers;

import com.google.common.collect.Maps;
import org.redis.server.protocol.Command;

import java.util.Map;

/**
 * @author LuoJie
 * @date 2015/11/15.
 */
public final class HandlerMap {
    public final Map<Command, CommandHandler> handlerMap;

    public HandlerMap(final IConnectionHadnler conectonHandler,
                      final IHashHandler hashHandler,
                      final IKeyHandler keyHandler,
                      final IListHandler listHandler,
                      final IPubSubHandler pbHandler,
                      final IServerHandler serveHandler,
                      final ISetHandler setHandler,
                      final ISortedSetHandler sSetHandler,
                      final IStringHandler strHandler,
                      final ITransactionHandler tranHandler) {

        handlerMap = Maps.newHashMap();
        initKey(keyHandler);
        initString(strHandler);
        initList(listHandler);
        initHash(hashHandler);
        initSet(setHandler);
        initSortedSet(sSetHandler);
        initPubSub(pbHandler);
        initConnection(conectonHandler);
        initServer(serveHandler);
        initTransaction(tranHandler);
    }

    public CommandHandler getHandler(Command cmd) {
        return handlerMap.get(cmd);
    }

    private void initTransaction(ITransactionHandler tranHandler) {
        handlerMap.put(Command.DISCARD, tranHandler);
        handlerMap.put(Command.EXEC, tranHandler);
        handlerMap.put(Command.MULTI, tranHandler);
        handlerMap.put(Command.UNWATCH, tranHandler);
        handlerMap.put(Command.WATCH, tranHandler);
    }

    private void initServer(IServerHandler serveHandler) {
        handlerMap.put(Command.BGREWRITEAOF, serveHandler);
        handlerMap.put(Command.BGSAVE, serveHandler);
        handlerMap.put(Command.CONFIG, serveHandler);
        handlerMap.put(Command.DBSIZE, serveHandler);
        handlerMap.put(Command.DEBUG, serveHandler);
        handlerMap.put(Command.FLUSHALL, serveHandler);
        handlerMap.put(Command.FLUSHDB, serveHandler);
        handlerMap.put(Command.INFO, serveHandler);
        handlerMap.put(Command.LASTSAVE, serveHandler);
        handlerMap.put(Command.MONITOR, serveHandler);
        handlerMap.put(Command.SAVE, serveHandler);
        handlerMap.put(Command.SHUTDOWN, serveHandler);
        handlerMap.put(Command.SLAVEOF, serveHandler);
        handlerMap.put(Command.SLOWLOG, serveHandler);
        handlerMap.put(Command.SYNC, serveHandler);
    }

    private void initConnection(IConnectionHadnler conectonHandler) {
        handlerMap.put(Command.AUTH, conectonHandler);
        handlerMap.put(Command.ECHO, conectonHandler);
        handlerMap.put(Command.PING, conectonHandler);
        handlerMap.put(Command.QUIT, conectonHandler);
        handlerMap.put(Command.SELECT, conectonHandler);
    }

    private void initPubSub(IPubSubHandler pbHandler) {
        handlerMap.put(Command.PSUBSCRIBE, pbHandler);
        handlerMap.put(Command.PUBLISH, pbHandler);
        handlerMap.put(Command.PUNSUBSCRIBE, pbHandler);
        handlerMap.put(Command.SUBSCRIBE, pbHandler);
        handlerMap.put(Command.UNSUBSCRIBE, pbHandler);
    }

    private void initSortedSet(ISortedSetHandler sSetHandler) {
        handlerMap.put(Command.ZADD, sSetHandler);
        handlerMap.put(Command.ZCARD, sSetHandler);
        handlerMap.put(Command.ZCOUNT, sSetHandler);
        handlerMap.put(Command.ZINCRBY, sSetHandler);
        handlerMap.put(Command.ZRANGE, sSetHandler);
        handlerMap.put(Command.ZRANGEBYSCORE, sSetHandler);
        handlerMap.put(Command.ZRANK, sSetHandler);
        handlerMap.put(Command.ZREM, sSetHandler);
        handlerMap.put(Command.ZREMRANGEBYRANK, sSetHandler);
        handlerMap.put(Command.ZREMRANGEBYSCORE, sSetHandler);
        handlerMap.put(Command.ZREVRANGE, sSetHandler);
        handlerMap.put(Command.ZREVRANGEBYSCORE, sSetHandler);
        handlerMap.put(Command.ZREVRANK, sSetHandler);
        handlerMap.put(Command.ZSCORE, sSetHandler);
        handlerMap.put(Command.ZUNIONSTORE, sSetHandler);
        handlerMap.put(Command.ZINTERSTORE, sSetHandler);
    }

    private void initSet(ISetHandler setHandler) {
        handlerMap.put(Command.SADD, setHandler);
        handlerMap.put(Command.SCARD, setHandler);
        handlerMap.put(Command.SDIFF, setHandler);
        handlerMap.put(Command.SDIFFSTORE, setHandler);
        handlerMap.put(Command.SINTER, setHandler);
        handlerMap.put(Command.SINTERSTORE, setHandler);
        handlerMap.put(Command.SISMEMBER, setHandler);
        handlerMap.put(Command.SMEMBERS, setHandler);
        handlerMap.put(Command.SMOVE, setHandler);
        handlerMap.put(Command.SPOP, setHandler);
        handlerMap.put(Command.SRANDMEMBER, setHandler);
        handlerMap.put(Command.SREM, setHandler);
        handlerMap.put(Command.SUNION, setHandler);
        handlerMap.put(Command.SUNIONSTORE, setHandler);
    }

    private void initHash(IHashHandler hashHandler) {
        handlerMap.put(Command.HDEL, hashHandler);
        handlerMap.put(Command.HEXISTS, hashHandler);
        handlerMap.put(Command.HGET, hashHandler);
        handlerMap.put(Command.HGETALL, hashHandler);
        handlerMap.put(Command.HINCRBY, hashHandler);
        handlerMap.put(Command.HKEYS, hashHandler);
        handlerMap.put(Command.HLEN, hashHandler);
        handlerMap.put(Command.HMGET, hashHandler);
        handlerMap.put(Command.HMSET, hashHandler);
        handlerMap.put(Command.HSET, hashHandler);
        handlerMap.put(Command.HSETNX, hashHandler);
        handlerMap.put(Command.HVALS, hashHandler);
    }

    private void initList(IListHandler listHandler) {
        handlerMap.put(Command.BLPOP, listHandler);
        handlerMap.put(Command.BRPOP, listHandler);
        handlerMap.put(Command.BRPOPLPUSH, listHandler);
        handlerMap.put(Command.LINDEX, listHandler);
        handlerMap.put(Command.LINSERT, listHandler);
        handlerMap.put(Command.LLEN, listHandler);
        handlerMap.put(Command.LPOP, listHandler);
        handlerMap.put(Command.LPUSH, listHandler);
        handlerMap.put(Command.LPUSHX, listHandler);
        handlerMap.put(Command.LRANGE, listHandler);
        handlerMap.put(Command.LREM, listHandler);
        handlerMap.put(Command.LSET, listHandler);
        handlerMap.put(Command.LTRIM, listHandler);
        handlerMap.put(Command.RPOP, listHandler);
        handlerMap.put(Command.RPOPLPUSH, listHandler);
        handlerMap.put(Command.RPUSH, listHandler);
        handlerMap.put(Command.RPUSHX, listHandler);
    }

    private void initString(IStringHandler strHandler) {
        handlerMap.put(Command.APPEND, strHandler);
        handlerMap.put(Command.DECR, strHandler);
        handlerMap.put(Command.DECRBY, strHandler);
        handlerMap.put(Command.GET, strHandler);
        handlerMap.put(Command.GETBIT, strHandler);
        handlerMap.put(Command.GETRANGE, strHandler);
        handlerMap.put(Command.GETSET, strHandler);
        handlerMap.put(Command.INCR, strHandler);
        handlerMap.put(Command.INCRBY, strHandler);
        handlerMap.put(Command.MGET, strHandler);
        handlerMap.put(Command.MSET, strHandler);
        handlerMap.put(Command.MSETNX, strHandler);
        handlerMap.put(Command.SET, strHandler);
        handlerMap.put(Command.SETBIT, strHandler);
        handlerMap.put(Command.SETEX, strHandler);
        handlerMap.put(Command.SETNX, strHandler);
        handlerMap.put(Command.SETRANGE, strHandler);
        handlerMap.put(Command.STRLEN, strHandler);
    }

    private void initKey(IKeyHandler keyHandler) {
        handlerMap.put(Command.DEL, keyHandler);
        handlerMap.put(Command.EXISTS, keyHandler);
        handlerMap.put(Command.EXPIRE, keyHandler);
        handlerMap.put(Command.EXPIREAT, keyHandler);
        handlerMap.put(Command.KEYS, keyHandler);
        handlerMap.put(Command.MOVE, keyHandler);
        handlerMap.put(Command.OBJECT, keyHandler);
        handlerMap.put(Command.PERSIST, keyHandler);
        handlerMap.put(Command.RANDOMKEY, keyHandler);
        handlerMap.put(Command.RENAME, keyHandler);
        handlerMap.put(Command.RENAMENX, keyHandler);
        handlerMap.put(Command.SORT, keyHandler);
        handlerMap.put(Command.TTL, keyHandler);
        handlerMap.put(Command.TYPE, keyHandler);
    }
}
