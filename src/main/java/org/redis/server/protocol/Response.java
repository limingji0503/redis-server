package org.redis.server.protocol;

import redis.clients.util.SafeEncoder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author LuoJie
 *
    如果是单行回复，那么第一个字节是「+」
    如果回复的内容是错误信息，那么第一个字节是「-」
    如果回复的内容是一个整型数字，那么第一个字节是「:」
    如果是bulk回复，那么第一个字节是「$」
    如果是multi-bulk回复，那么第一个字节是「*」
 */
public class Response implements Serializable {
    private static final long serialVersionUID = 1L;

    public static enum Type {
        SINGLE, ERROR, INT, BULK, MBULK, SKIP;

        public final byte[] raw;
        Type() {
            raw = SafeEncoder.encode(this.name());
        }
    }

    private Type type;
    private List<byte[]> values = new ArrayList<byte[]>();

    public Response() {}
    public Response(Type type) {
        this.type = type;
    }

    public List<byte[]> getValues() {
        return values;
    }

    public Response addNullValue() {
        values.add(null);
        return this;
    }

    public Response addValue(byte[] val) {
        values.add(val);
        return this;
    }

    public Response addValue(String val) {
        return addValue(val.getBytes());
    }

    public Response addValue(Long val) {
        return addValue(String.valueOf(val));
    }

    public Response addValue(Integer val) {
        return addValue(String.valueOf(val));
    }

    public Response addValue(List<String> list) {
        if (list != null) {
            for (String k : list) {
                addValue(k.getBytes());
            }
        }
        return this;
    }

    public Response addValue(Set<String> set) {
        if (set == null) {
            return this;
        }
        for (String val : set) {
            addValue(val);
        }
        return this;
    }

    public Response addValue(Map<String, String> map) {
        if (map != null) {
            for (String k : map.keySet()) {
                addValue(k.getBytes());
                addValue(map.get(k).getBytes());
            }
        }
        return this;
    }

    public void setValues(List<byte[]> values) {
        this.values = values;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
