package org.redis.server.protocol;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LuoJie on 2015/12/29.
 */
public class SimpleRequest {
    private Command cmd;
    private byte[] key;
    private List<byte[]> vals = new ArrayList<>();

    public SimpleRequest() {

    }

    public SimpleRequest(Command command, byte[] key, List<byte[]> values) {
        this.cmd = command;
        this.key = key;
        this.vals = values;
    }

    public Command getCmd() {
        return cmd;
    }

    public byte[] getKey() {
        return key;
    }

    public List<byte[]> getVals() {
        return vals;
    }

    public void setCmd(Command cmd) {
        this.cmd = cmd;
    }

    public void setKey(byte[] key) {
        this.key = key;
    }

    public void setVals(List<byte[]> vals) {
        this.vals = vals;
    }
}
