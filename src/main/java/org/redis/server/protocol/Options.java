package org.redis.server.protocol;

import redis.clients.util.SafeEncoder;

/**
 *
 * @author LUOJIE
 */
public enum Options {
    WITHSCORES, LIMIT;

    public final byte[] raw;

    Options() {
        raw = SafeEncoder.encode(this.name());
    }
}
