package org.redis.server.protocol;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author LuoJie
 */
public class Request implements Serializable {
    private static final long serialVersionUID = 1L;

    private Command command;
    private byte[] key;
    private List<byte[]> values = new ArrayList<>();

    private int fieldNum;
    private int fieldIndex;
    private int nextFieldLength;

    public Request() {
    }

    public void setCommand(String command) {
        this.command = Command.valueOf(command.toUpperCase());
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public byte[] getKey() {
        return key;
    }

    public void setKey(byte[] key) {
        this.key = key;
    }

    public List<byte[]> getValues() {
        return values;
    }

    public void addValue(byte[] val) {
        this.values.add(val);
    }

    public int getFieldNum() {
        return fieldNum;
    }

    public void setFieldNum(int fieldNum) {
        this.fieldNum = fieldNum;
    }

    public int getFieldIndex() {
        return fieldIndex;
    }

    public void incrFieldIndex() {
        fieldIndex++;
    }

    public int getNextFieldLength() {
        return nextFieldLength;
    }

    public void setNextFieldLength(int nextFieldLength) {
        this.nextFieldLength = nextFieldLength;
    }

    @Override
    public String toString() {
        return "Request [command=" + command + "]";
    }

    public SimpleRequest toSimple() {
        return new SimpleRequest(command, key, values);
    }
}
