package org.redis.server.protocol;

import org.redis.server.util.ByteArray;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 
 * @author LUOJIE
 */
public abstract class PubSub {
	public static final byte PLUS = '+';
	public static final byte MINUS = '-';
	public static final byte COLON = ':';
	public static final byte ASTERISK = '*';
	public static final byte DOLLAR = '$';
	public static final byte[] MINUS_ONE = "-1".getBytes();
	public static final byte[] CRLF = "\r\n".getBytes();

	public static List<byte[]> construct(Command command, int size, String... keys) {
		List<byte[]> list = new ArrayList<byte[]>();
		byte[] cmd = command.name().toLowerCase().getBytes();
		int i = 0;
		while (i < keys.length) {
			switch (command) {
			case SUBSCRIBE:
			case PSUBSCRIBE:
				list.add(cmd);
				list.add(keys[i].getBytes());
				list.add(String.valueOf(i + 1).getBytes());
				break;
			case UNSUBSCRIBE:
			case PUNSUBSCRIBE:
				list.add(cmd);
				list.add(keys[i].getBytes());
				list.add(String.valueOf(size - (i + 1)).getBytes());
				break;
			case PUBLISH:
			case MESSAGE:
				if (!list.contains(cmd))
					list.add(cmd);
				list.add(keys[i].getBytes());
				break;
			default:
				break;
			}
			i++;
		}
		return list;
	}
	
	public static List<byte[]> construct(Command command, int size, byte[]... keys) {
		List<byte[]> list = new ArrayList<byte[]>();
		byte[] cmd = command.name().toLowerCase().getBytes();
		int i = 0;
		while (i < keys.length) {
			switch (command) {
			case SUBSCRIBE:
				list.add(cmd);
				list.add(keys[i]);
				list.add(String.valueOf(i + 1).getBytes());
				break;
			case UNSUBSCRIBE:
				list.add(cmd);
				list.add(keys[i]);
				list.add(String.valueOf(size - (i + 1)).getBytes());
				break;
			case PUBLISH:
			case MESSAGE:
				if (!list.contains(cmd))
					list.add(cmd);
				list.add(keys[i]);
				break;
			default:
				break;
			}
			i++;
		}
		return list;
	}

	/**
	 * Pubsub协议编码
	 */
	public static byte[] encode(List<byte[]> vals) {
		if(vals.size() <= 0) {
			return null;
		}
		
		String command = new String(vals.get(0));
		if (!command.equals(Command.SUBSCRIBE.name().toLowerCase())
				&& !command.equals(Command.UNSUBSCRIBE.name().toLowerCase())) {
			return null;
		}

		ByteArray response = new ByteArray();
		Pattern pattern = Pattern.compile("[0-9]{1,}");
		int i = 0;
		for (byte[] val : vals) {
			if (i % 3 == 0) {
				response.append(ASTERISK);
				response.append("3".getBytes());
				response.append(CRLF);
			}
			i++;
			
			if (val == null) {
				response.append(DOLLAR);
				response.append(MINUS_ONE);
				response.append(CRLF);
				continue;
			}

			if (pattern.matcher((CharSequence) new String(val)).matches()) {
				response.append(COLON);
				response.append(val);
				response.append(CRLF);
				continue;
			}

			response.append(DOLLAR);
			response.append(String.valueOf(val.length).getBytes());
			response.append(CRLF);
			response.append(val);
			response.append(CRLF);
		}
		return response.toBytes();
	}
}
