package org.redis.server.abstractAOF;

/**
 * @author roger on 15/12/29
 */
public class AOFConst {

    public static final String REDIS_AOF_FILENAME = "redis.aof";

    public static final String REDIS_AOF_REWRITE_FILENAME = "redis.aof.rewrite";

    public static final String YES = "yes";

    public static final String NO = "no";

    public static final int REDIS_AOF_INTERVAL = 3000;

    public static final int REDIS_AOF_FLUSHSIZE = 5000;

    public static final long REDIS_AOF_REWRITE_INTERVAL = 90000;
}
