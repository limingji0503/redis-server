package org.redis.server.abstractAOF;

import org.redis.server.typeapi.BinaryRedisDB;
import org.redis.server.protocol.SimpleRequest;

import java.util.List;

/**
 * @author roger on 15/12/28
 */
public interface IAOF {

    void writeAOF(List<SimpleRequest> requests);

    void loadAOF(BinaryRedisDB db);

    void rewrite(BinaryRedisDB db) throws  Exception;

    void close() throws Exception;
}
