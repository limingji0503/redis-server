package org.redis.server.abstractAOF;

import com.google.common.collect.Sets;
import org.redis.server.protocol.Command;

import java.util.Set;

/**
 * @author roger on 15/12/28
 */
public class ModifyCmds {
    public static final Set<Command> set = Sets.newHashSet();

    private ModifyCmds() {

    }

    public static boolean isModification(Command cmd) {
        return set.contains(cmd);
    }

    static {
        set.add(Command.ZADD);
        set.add(Command.ZINCRBY);
        set.add(Command.ZREM);
        set.add(Command.ZREMRANGEBYRANK);
        set.add(Command.ZREMRANGEBYSCORE);

        set.add(Command.SADD);
        set.add(Command.SDIFFSTORE);
        set.add(Command.SINTERSTORE);
        set.add(Command.SMOVE);
        set.add(Command.SPOP);
        set.add(Command.SREM);
        set.add(Command.SUNIONSTORE);

        set.add(Command.HDEL);
        set.add(Command.HEXISTS);
        set.add(Command.HGET);
        set.add(Command.HGETALL);
        set.add(Command.HINCRBY);
        set.add(Command.HKEYS);
        set.add(Command.HLEN);
        set.add(Command.HMGET);
        set.add(Command.HMSET);
        set.add(Command.HSET);
        set.add(Command.HSETNX);
        set.add(Command.HVALS);

        set.add(Command.BLPOP);
        set.add(Command.BRPOP);
        set.add(Command.BRPOPLPUSH);
        set.add(Command.LINSERT);
        set.add(Command.LPOP);
        set.add(Command.LPUSH);
        set.add(Command.LPUSHX);
        set.add(Command.LREM);
        set.add(Command.LSET);
        set.add(Command.LTRIM);
        set.add(Command.RPOP);
        set.add(Command.RPOPLPUSH);
        set.add(Command.RPUSH);
        set.add(Command.RPUSHX);
        
        set.add(Command.APPEND);
        set.add(Command.DECR);
        set.add(Command.DECRBY);
        set.add(Command.GETSET);
        set.add(Command.INCR);
        set.add(Command.INCRBY);
        set.add(Command.MSET);
        set.add(Command.MSETNX);
        set.add(Command.SET);
        set.add(Command.SETBIT);
        set.add(Command.SETEX);
        set.add(Command.SETNX);
        set.add(Command.SETRANGE);

        set.add(Command.DEL);
        set.add(Command.RENAME);
        set.add(Command.RENAMENX);
        set.add(Command.TTL);
    }
}
