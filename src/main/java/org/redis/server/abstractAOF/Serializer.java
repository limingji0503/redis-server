package org.redis.server.abstractAOF;

import org.redis.server.protocol.SimpleRequest;

/**
 * @author roger on 15/12/28
 */
public interface Serializer {

    SimpleRequest decode(String line);

    String encode(SimpleRequest request);
}
