package org.redis.server.conf;

/**
 * @author roger on 15/12/28
 */
public final class Const {
    public static final String REDIS_CONF_LOCAL_PATH = "redis.conf";
    public static final int REDIS_SERVER_DEFAULT_PORT = 65535;
    public static final byte[] BLANCK_BYTES = new byte[0];
    public static final int REDIS_SERVER_JMX_REMOTE_PORT = 12356;

    public static final byte[] REDIS_POSITIVE_INF = "inf".getBytes();
    public static final byte[] REDIS_NEGETIVE_INF = "-inf".getBytes();

    public static final String REDIS_CONF_ZOO_ADDR = "redis.conf.zoo.addr";
    public static final String REDIS_CONF_ZOO_NODE = "redis.conf.zoo.node";

    public static final String REDIS_AOF_OPEN = "redis.aof.open";
    public static final String REDIS_AOF_FILENAME = "redis.aof.filename";
    public static final String REDIS_AOF_INTERVAL = "redis.aof.interval";
    public static final String REDIS_AOF_FLUSHSIZE = "redis.aof.flushsize";
    public static final String REDIS_AOF_REWRITE_INTERVAL = "redis.aof.rewrite.interval";

    public static final String REDIS_SERVER_PORT = "redis.server.port";
}
