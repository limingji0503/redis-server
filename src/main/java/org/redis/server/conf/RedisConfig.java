package org.redis.server.conf;

import org.redis.server.util.Property;

/**
 * @author roger on 15/12/28
 */
public class RedisConfig extends Property {
    private static final RedisConfig config = new RedisConfig();

    private RedisConfig() {
        super(Const.REDIS_CONF_LOCAL_PATH);
    }

    public static RedisConfig getConfig() {
        return config;
    }
}
