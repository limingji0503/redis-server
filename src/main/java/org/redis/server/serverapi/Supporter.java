package org.redis.server.serverapi;

import org.redis.server.protocol.Request;

/**
 * @author LuoJie on 2015/12/28.
 */
public interface Supporter {

    /**
     * Baclup redis data by redis request which modifies redis data.
     * @param request Redis Request.
     */
    void aof(Request request);
}
