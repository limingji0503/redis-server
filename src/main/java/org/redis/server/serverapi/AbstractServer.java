package org.redis.server.serverapi;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.ChannelGroupFuture;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.util.internal.LinkedTransferQueue;
import org.redis.server.remoting.RedisDecoder;
import org.redis.server.remoting.RedisEncoder;
import org.redis.server.remoting.RedisHandler;

import java.lang.reflect.Constructor;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author LuoJie
 *
 */
public abstract class AbstractServer implements IRedisCommands, Supporter {
    public Logger logger = Logger.getLogger(AbstractServer.class);
    protected final RedisHandler handler;
	private ServerBootstrap bootstrap;
    private ChannelGroup channelGroup;
    private ThreadPoolExecutor worker;
    private List<String> bindAddresses;
    private int port;

	public AbstractServer(int port, Class<? extends RedisHandler> handlerType) {
        this(new ArrayList<String>(), port, handlerType, 10, 800);
	}

    public AbstractServer(String bindAddress, int port, Class<? extends RedisHandler> handlerType) {
        this(Collections.singletonList(bindAddress), port, handlerType, 10, 800);
    }

    /**
     *
     * @param bindAddresses 绑定的地址
     * @param port          端口
     * @param handlerType   cariaHandler类型
     */
    public AbstractServer(List<String> bindAddresses, int port, Class<? extends RedisHandler> handlerType) {
        this(bindAddresses, port, handlerType, 10, 800);
    }

    /**
     *
     * @param bindAddresses 绑定的地址
     * @param port          端口
     * @param handlerType   cariaHandler类型
     * @param initThread    初始线程数
     * @param maxThread     最大线程数
     */
    public AbstractServer(List<String> bindAddresses,  int port, Class<? extends RedisHandler> handlerType, int initThread, int maxThread) {
        this.port = port;
        this.bindAddresses = bindAddresses;
        this.worker = new ThreadPoolExecutor(initThread, maxThread, 60L, TimeUnit.SECONDS, new LinkedTransferQueue<Runnable>(),
                new BasicThreadFactory.Builder().namingPattern("RedisHandlerWorker-%d").build());
        RedisHandler invokeHandler = null;
        if (handlerType != null) {
            try {
                Constructor<? extends RedisHandler> c = handlerType.getConstructor(AbstractServer.class, ExecutorService.class);
                invokeHandler = c.newInstance(this, worker);
            } catch (Exception e) {
                logger.error(ExceptionUtils.getStackTrace(e));
            }
        }
        if (invokeHandler == null) {
            throw new NullPointerException("Failed to invoke handler, " +
                    "parameters of handler<init> must be: AbstractBinaryRedisServer, ThreadPoolExecutor");
        }
        this.handler = invokeHandler;
    }

	public void start() {
		bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));

        bootstrap.setOption("child.tcpNoDelay", true);
        bootstrap.setOption("child.keepAlive", true);
        bootstrap.setOption("sendBufferSize", 1024 * 1024);
        bootstrap.setOption("receiveBufferSize", 1024 * 1024);

        bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
            public ChannelPipeline getPipeline() throws Exception {
                ChannelPipeline pipeline = Channels.pipeline();

                pipeline.addLast("decoder", new RedisDecoder());
				pipeline.addLast("handler", handler);
                pipeline.addLast("encoder", new RedisEncoder());

                return pipeline;
            }
        });
        
        List<SocketAddress> socketAddresses = new ArrayList<SocketAddress>();
        if (bindAddresses.isEmpty()) {
            socketAddresses.add(new InetSocketAddress(port));
        } else {
            for (String bindAddress : bindAddresses) {
                socketAddresses.add(new InetSocketAddress(bindAddress, port));
            }
        }
        
        channelGroup = new DefaultChannelGroup("RedisServer-ChannelGroup");
        
        for (SocketAddress socketAddress : socketAddresses) {
            logger.warn("redis server bind address " + socketAddress);

            Channel serverChannel = bootstrap.bind(socketAddress);
            channelGroup.add(serverChannel);
        }
	}

	public void stop() {
		ChannelGroupFuture channelGroupFuture = channelGroup.close();
        channelGroupFuture.awaitUninterruptibly();
        if (!channelGroupFuture.isCompleteSuccess()) {
            throw new RuntimeException("Close channels failed!");
        }

        bootstrap.releaseExternalResources();
	}

    public ThreadPoolExecutor getWorker() {
        return worker;
    }
}
