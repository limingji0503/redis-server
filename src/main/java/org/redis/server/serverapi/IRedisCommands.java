package org.redis.server.serverapi;

import redis.clients.jedis.BinaryClient;
import redis.clients.jedis.SortingParams;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author LuoJie
 * @date 2015/11/14.
 */
public interface IRedisCommands {

    byte[] info();

    byte[] auth();

    List<byte[]> mget(byte[]... keys);

    String mset(byte[]... keysvalues);

    long msetnx(byte[]... keysvalues);

    byte[] blpop(byte[] key);

    List<byte[]> keys(byte[] key);

    long del(byte[] key, List<byte[]> values);

    String set(byte[] key, byte[] value);

    byte[] get(byte[] key);

    long strlen(final byte[] key);

    boolean exists(byte[] key);

    long persist(final byte[] key);

    byte[] type(byte[] key);

    long expire(byte[] key, int seconds);

    long expireAt(byte[] key, long unixTime);

    long ttl(byte[] key);

    byte[] getSet(byte[] key, byte[] value);

    long setnx(byte[] key, byte[] value);

    String setex(byte[] key, int seconds, byte[] value);

    long decrBy(byte[] key, long integer);

    long decr(byte[] key);

    long incrBy(byte[] key, long integer);

    long incr(byte[] key);

    long append(byte[] key, byte[] value);

    byte[] substr(byte[] key, int start, int end);

    long hset(byte[] key, byte[] field, byte[] value);

    byte[] hget(byte[] key, byte[] field);

    long hsetnx(byte[] key, byte[] field, byte[] value);

    List<byte[]> hmget(byte[] key, byte[]... fields);

    String hmset(byte[] key, byte[]... hash);

    long hincrBy(byte[] key, byte[] field, long value);

    Float hincrByFloat(byte[] key, byte[] field, float value);

    boolean hexists(byte[] key, byte[] field);

    long hdel(byte[] key, byte[]... field);

    long hlen(byte[] key);

    Set<String> hkeys(byte[] key);

    Collection<byte[]> hvals(byte[] key);

    List<byte[]> hgetAll(byte[] key);

    List<byte[]> hscan(byte[] key, byte[]... commands);

    long rpush(byte[] key, byte[]... string);

    long lpush(byte[] key, byte[]... string);

    long llen(byte[] key);

    List<byte[]> lrange(byte[] key, int start, int end);

    String ltrim(byte[] key, int start, int end);

    byte[] lindex(byte[] key, int index);

    String lset(byte[] key, int index, byte[] value);

    long lrem(byte[] key, int count, byte[] value);

    byte[] lpop(byte[] key);

    byte[] rpop(byte[] key);

    long sadd(byte[] key, byte[]... member);

    Set<String> smembers(byte[] key);

    long srem(byte[] key, byte[]... member);

    byte[] spop(byte[] key);

    long scard(byte[] key);

    List<String> srandmember(byte[] key, int n, boolean isNoArgs);

    boolean sismember(byte[] key, byte[] member);

    byte[] srandmember(byte[] key);

    void sclear(byte[] key);

    long zadd(byte[] key, double score, byte[] member);

    long zadd(byte[] key, Map<Double, byte[]> scoreMembers);

    List<byte[]> zrange(byte[] key, int start, int end);

    long zrem(byte[] key, byte[]... member);

    double zincrby(byte[] key, double score, byte[] member);

    long zrank(byte[] key, byte[] member);

    long zrevrank(byte[] key, byte[] member);

    List<byte[]> zrevrange(byte[] key, int start, int end);

    List<byte[]> zrangeWithScores(byte[] key, int start, int end);

    List<byte[]> zrevrangeWithScores(byte[] key, int start, int end);

    long zcard(byte[] key);

    double zscore(byte[] key, byte[] member);

    List<byte[]> sort(byte[] key);

    List<byte[]> sort(byte[] key, SortingParams sortingParameters);

    long zcount(byte[] key, double min, double max);

    long zcount(byte[] key, byte[] min, byte[] max);

    List<byte[]> zrangeByScore(byte[] key, double min, double max);

    List<byte[]> zrangeByScore(byte[] key, byte[] min, byte[] max);

    List<byte[]> zrangeByScore(byte[] key, double min, double max, int offset,
                               int count);

    List<byte[]> zrangeByScoreWithScores(byte[] key, double min, double max);

    List<byte[]> zrangeByScoreWithScores(byte[] key, double min, double max,
                                         int offset, int count);

    List<byte[]> zrangeByScoreWithScores(byte[] key, byte[] min, byte[] max);

    List<byte[]> zrangeByScoreWithScores(byte[] key, byte[] min, byte[] max,
                                         int offset, int count);

    List<byte[]> zrevrangeByScore(byte[] key, double max, double min);

    List<byte[]> zrevrangeByScore(byte[] key, double max, double min,
                                  int offset, int count);

    List<byte[]> zrevrangeByScore(byte[] key, byte[] max, byte[] min);

    List<byte[]> zrevrangeByScore(byte[] key, byte[] max, byte[] min,
                                  int offset, int count);

    List<byte[]> zrevrangeByScoreWithScores(byte[] key, double max, double min);

    List<byte[]> zrevrangeByScoreWithScores(byte[] key, double max, double min,
                                            int offset, int count);

    List<byte[]> zrevrangeByScoreWithScores(byte[] key, byte[] max, byte[] min);

    List<byte[]> zrevrangeByScoreWithScores(byte[] key, byte[] max, byte[] min,
                                            int offset, int count);

    long zremrangeByRank(byte[] key, int start, int end);

    long zremrangeByScore(byte[] key, double start, double end);

    long zremrangeByScore(byte[] key, byte[] start, byte[] end);

    long linsert(byte[] key, BinaryClient.LIST_POSITION where, byte[] pivot, byte[] value);

    long objectRefcount(byte[] key);

    long objectIdletime(byte[] key);

    byte[] objectEncoding(byte[] key);

    long lpushx(byte[] key, byte[] string);

    long rpushx(byte[] key, byte[] string);
}
