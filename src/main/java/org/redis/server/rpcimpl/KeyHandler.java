package org.redis.server.rpcimpl;

import org.redis.server.handlers.IKeyHandler;
import org.redis.server.protocol.Command;
import org.redis.server.protocol.Response;
import org.redis.server.serverapi.AbstractServer;

import java.util.List;

/**
 * @author LuoJie
 * @date 2015/11/15.
 */
public class KeyHandler extends IKeyHandler {
    private AbstractServer redisServer;

    public KeyHandler(AbstractServer redisServer) {
        this.redisServer = redisServer;
    }

    @Override
    protected boolean handleKeyCommands(Command cmd, byte[] key, List<byte[]> values, Response response) {
        if (Command.KEYS == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'keys' command");
                return true;
            }
            List<byte[]> vals = redisServer.keys(key);
            response.setType(Response.Type.MBULK);
            response.setValues(vals);
        } else if (Command.DEL == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'del' command");
                return true;
            }
            Long val = redisServer.del(key, values);
            response.setType(Response.Type.INT);
            response.addValue(val);
        } else {
            return false;
        }
        return true;
    }
}
