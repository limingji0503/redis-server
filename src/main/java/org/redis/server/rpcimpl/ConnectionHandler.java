package org.redis.server.rpcimpl;

import org.redis.server.handlers.IConnectionHadnler;
import org.redis.server.protocol.Command;
import org.redis.server.protocol.Response;
import org.redis.server.serverapi.AbstractServer;
import org.redis.server.type.RedisDB;

import java.util.List;

/**
 * @author LuoJie
 * @date 2015/11/15.
 */
public class ConnectionHandler extends IConnectionHadnler {
    private AbstractServer redisServer;

    public ConnectionHandler(AbstractServer redisServer) {
        this.redisServer = redisServer;
    }
    @Override
    protected boolean handleConnetionCommands(Command cmd, byte[] key, List<byte[]> values, Response response) {
        if (Command.PING == cmd) {
            response.setType(Response.Type.SINGLE);
            response.addValue("PONG");
        }
        else if (Command.AUTH == cmd) {
            byte[] val = redisServer.auth();
            response.setType(Response.Type.SINGLE);
            response.addValue(val);
        }
        else if (Command.ECHO == cmd) {
            response.setType(Response.Type.SINGLE);
            response.addValue(RedisDB.OK.OK);
        }
        else if (Command.QUIT == cmd) {
            response.setType(Response.Type.SINGLE);
            response.addValue(RedisDB.OK.OK);
        }
        else {
            return false;
        }
        return true;
    }
}
