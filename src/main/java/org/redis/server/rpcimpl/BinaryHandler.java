package org.redis.server.rpcimpl;

import org.redis.server.handlers.HandlerMap;
import org.redis.server.remoting.RedisHandler;
import org.redis.server.serverapi.AbstractServer;

import java.util.concurrent.ExecutorService;

/**
 * Created by roger on 15/11/9.
 */
public class BinaryHandler extends RedisHandler {
    public BinaryHandler(AbstractServer redisServer, ExecutorService executor) {
        super(redisServer, executor,
                new HandlerMap(new ConnectionHandler(redisServer),
                        new HashHandler(redisServer),
                        new KeyHandler(redisServer),
                        new ListHandler(redisServer),
                        new PubSubHandler(redisServer),
                        new ServerHandler(redisServer),
                        new SetHandler(redisServer),
                        new SortedSetHandler(redisServer),
                        new StringHandler(redisServer),
                        new TransactionHandler(redisServer)));
    }
}
