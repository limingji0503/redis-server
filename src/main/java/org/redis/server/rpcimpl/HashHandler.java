package org.redis.server.rpcimpl;

import com.google.common.collect.Lists;
import org.redis.server.handlers.IHashHandler;
import org.redis.server.protocol.Command;
import org.redis.server.protocol.Response;
import org.redis.server.serverapi.AbstractServer;
import org.redis.server.util.ByteArray;

import java.util.List;
import java.util.Set;

/**
 * @author LuoJie
 * @date 2015/11/15.
 */
public class HashHandler extends IHashHandler {
    private AbstractServer redisServer;

    public HashHandler(AbstractServer redisServer) {
        this.redisServer = redisServer;
    }

    @Override
    protected boolean handleHashCommands(Command cmd, byte[] key, List<byte[]> values, Response response) {
        if (Command.HDEL == cmd) {
            if (key == null || values == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'hdel' command");
                return true;
            }
            Long val = redisServer.hdel(key, values.toArray(new byte[values.size()][]));
            response.setType(Response.Type.INT);
            if (val == null) response.addNullValue();
            else response.addValue(val);
        } else if (Command.HEXISTS == cmd) {
            if (key == null || values == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'hexists' command");
                return true;
            }
            boolean val = redisServer.hexists(key, values.get(0));
            response.setType(Response.Type.INT);
            response.addValue(val ? 1L : 0L);
        } else if (Command.HGET == cmd) {
            if (key == null || values == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'hget' command");
                return true;
            }
            byte[] val = redisServer.hget(key, values.get(0));
            if (val == null) {
                response.setType(Response.Type.BULK);
                response.addNullValue();
            } else {
                response.setType(Response.Type.BULK);
                response.addValue(val);
            }
        } else if (Command.HGETALL == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'hgetall' command");
                return true;
            }
            List<byte[]> vals = redisServer.hgetAll(key);
            if (vals == null) {
                response.setType(Response.Type.MBULK);
                response.addNullValue();
            } else {
                response.setType(Response.Type.MBULK);
                response.setValues(vals);
            }
        } else if (Command.HINCRBY == cmd) {
            if (key == null || values == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'hincrby' command");
                return true;
            }
            Long val = null;
            try {
                val = redisServer.hincrBy(key, values.get(0),
                        ByteArray.bytesToInt(values.get(1)));
            } catch (NumberFormatException e) {
                response.setType(Response.Type.ERROR);
                response.addValue(e.getMessage());
                return true;
            }
            if (val == null) {
                response.setType(Response.Type.INT);
                response.addNullValue();
            } else {
                response.setType(Response.Type.INT);
                response.addValue(val);
            }
        } else if (Command.HINCRBYFLOAT == cmd) {
            if (key == null || values == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'hincrby' command");
                return true;
            }
            Float val = null;
            try {
                val = redisServer.hincrByFloat(key, values.get(0),
                        Float.valueOf(new String(values.get(1))));
            } catch (NumberFormatException e) {
                response.setType(Response.Type.ERROR);
                response.addValue(e.getMessage());
                return true;
            }
            if (val == null) {
                response.setType(Response.Type.INT);
                response.addNullValue();
            } else {
                response.setType(Response.Type.INT);
                response.addValue(String.valueOf(val));
            }
        } else if (Command.HKEYS == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'hkeys' command");
                return true;
            }
            Set<String> vals = redisServer.hkeys(key);
            if (vals == null) {
                response.setType(Response.Type.MBULK);
                response.addNullValue();
            } else {
                response.setType(Response.Type.MBULK);
                List<byte[]> res = Lists.newLinkedList();
                for (String field : vals) {
                    res.add(field.getBytes());
                }
                response.setValues(res);
            }
        } else if (Command.HLEN == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'hlen' command");
                return true;
            }
            Long val = redisServer.hlen(key);
            if (val == null) {
                response.setType(Response.Type.SINGLE);
                response.addNullValue();
            } else {
                response.setType(Response.Type.INT);
                response.addValue(val);
            }
        } else if (Command.HMSET == cmd) {
            if (key == null || values == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'mset' command");
                return true;
            }
            List<byte[]> val = redisServer.hmget(key, values.toArray(new byte[values.size()][]));
            if (val == null) {
                response.setType(Response.Type.MBULK);
                response.addNullValue();
            } else {
                response.setType(Response.Type.MBULK);
                response.setValues(val);
            }
        } else if (Command.HSET== cmd) {
            if (key == null || values == null || values.size() != 2) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'hset' command");
                return true;
            }
            Long val = redisServer.hset(key, values.get(0), values.get(1));
            if (val == null) {
                response.setType(Response.Type.INT);
                response.addNullValue();
            } else {
                response.setType(Response.Type.INT);
                response.addValue(val);
            }
        } else if (Command.HSETNX == cmd) {
            if (key == null || values == null || values.size() != 2) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'hsetnx' command");
                return true;
            }
            Long val = redisServer.hsetnx(key, values.get(0), values.get(1));
            if (val == null) {
                response.setType(Response.Type.INT);
                response.addNullValue();
            } else {
                response.setType(Response.Type.INT);
                response.addValue(val);
            }
        } else if (Command.HVALS == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'hvals' command");
                return true;
            }
            List<byte[]> vals = (List<byte[]>)redisServer.hvals(key);
            if (vals == null) {
                response.setType(Response.Type.MBULK);
                response.addNullValue();
            } else {
                response.setType(Response.Type.MBULK);
                response.setValues(vals);
            }
        } else if (Command.HSCAN == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'hscan' command");
                return true;
            }
            List<byte[]> vals = redisServer.hscan(key, values.toArray(new byte[values.size()][]));
            if (vals == null) {
                response.setType(Response.Type.MBULK);
                response.addNullValue();
            } else {
                response.setType(Response.Type.MBULK);
                response.setValues(vals);
            }
        } else {
            return false;
        }
        return true;
    }
}
