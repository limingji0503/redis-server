package org.redis.server.rpcimpl;

import org.redis.server.handlers.IPubSubHandler;
import org.redis.server.protocol.Command;
import org.redis.server.protocol.Response;
import org.redis.server.serverapi.AbstractServer;

import java.util.List;

/**
 * @author LuoJie
 * @date 2015/11/15.
 */
public class PubSubHandler extends IPubSubHandler {
    private AbstractServer redisServer;

    public PubSubHandler(AbstractServer redisServer) {
        this.redisServer = redisServer;
    }
    @Override
    protected boolean handlePubSubCommands(Command cmd, byte[] key, List<byte[]> values, Response response) {
        return false;
    }
}
