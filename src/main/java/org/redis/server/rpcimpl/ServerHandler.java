package org.redis.server.rpcimpl;

import org.redis.server.handlers.IServerHandler;
import org.redis.server.protocol.Command;
import org.redis.server.protocol.Response;
import org.redis.server.serverapi.AbstractServer;

import java.util.List;

/**
 * @author LuoJie
 * @date 2015/11/15.
 */
public class ServerHandler extends IServerHandler {
    private AbstractServer redisServer;

    public ServerHandler(AbstractServer redisServer) {
        this.redisServer = redisServer;
    }
    @Override
    protected boolean handleServerCommands(Command cmd, byte[] key, List<byte[]> values, Response response) {
        return false;
    }
}
