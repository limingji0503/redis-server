package org.redis.server.rpcimpl;

import org.redis.server.serverapi.AbstractServer;
import org.redis.server.handlers.IStringHandler;
import org.redis.server.protocol.Command;
import org.redis.server.protocol.Response;
import org.redis.server.type.RedisDB;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LuoJie
 * @date 2015/11/15.
 */
public class StringHandler extends IStringHandler {
    private AbstractServer redisServer;

    public StringHandler(AbstractServer redisServer) {
        this.redisServer = redisServer;
    }

    @Override
    protected boolean handleStringCommands(Command cmd, byte[] key, List<byte[]> values, Response response) {
        return true;
    }
//        /***
//         * BITCOUNT BITOP INCRBYFLOAT PSETEX  Redis文档存在，但是杰哥不承认常用的命令。
//         */
//        if (Command.APPEND == cmd) {
//            if (key == null || values.size() != 1) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'append' command");
//                return true;
//            }
//            int length = redisServer.append(key, values.get(0));
//            response.setType(Response.Type.INT);
//            response.addValue(length);
//            return true;
//        } else if (Command.DECR == cmd) {
//            if (key == null || values.size() != 0) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'decr' command");
//                return true;
//            }
//            String val = redisServer.get(key);
//            if (val == null) {
//                redisServer.set(key, "-1".getBytes());
//                response.setType(Response.Type.INT);
//                response.addValue(-1);
//                return true;
//            } else {
//                try {
//                    long valLong = Long.parseLong(val);
//                    valLong -= 1;
//                    redisServer.set(key, (valLong + "").getBytes());
//                    response.setType(Response.Type.INT);
//                    response.addValue(valLong);
//                    return true;
//                } catch (NumberFormatException e) {
//                    response.setType(Response.Type.ERROR);
//                    response.addValue("WRONGTYPE Operation against a key holding the wrong kind of value");
//                    return true;
//                }
//            }
//        } else if (Command.DECRBY == cmd) {
//            if (key == null || values.size() != 1) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'decrby' command");
//                return true;
//            }
//            Long decrNum;
//            try {
//                decrNum = Long.parseLong(new String(values.get(0)));
//            } catch (NumberFormatException e) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR value is not an integer or out of range");
//                return true;
//            }
//
//            String val = redisServer.get(key);
//            if (val == null) {
//                val = "0";
//            }
//            long valLong;
//            try {
//                valLong = Long.parseLong(val);
//            } catch (NumberFormatException e) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("WRONGTYPE Operation against a key holding the wrong kind of value");
//                return true;
//            }
//            long newVal = valLong - decrNum;
//            redisServer.set(key, (newVal + "").getBytes());
//            response.setType(Response.Type.INT);
//            response.addValue(newVal);
//            return true;
//        } else if (Command.GET == cmd) {
//            if (key == null || !values.isEmpty()) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'get' command");
//                return true;
//            }
//            String val = redisServer.get(key);
//            if (val == null) {
//                response.setType(Response.Type.BULK);
//                response.addNullValue();
//            } else {
//                response.setType(Response.Type.BULK);
//                response.addValue(val);
//            }
//            return true;
//        } else if (Command.GETBIT == cmd) {
//            if (key == null || values.size() != 1) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'getbit' command");
//                return true;
//            }
//            try {
//                int pos = Integer.parseInt(new String(values.get(0)));
//                String val = redisServer.getBit(key, pos);
//                response.setType(Response.Type.BULK);
//                response.addValue(val);
//                return true;
//            } catch (NumberFormatException e) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR bit offset is not an integer or out of range");
//                return true;
//            }
//        } else if (Command.GETRANGE == cmd) {
//            if (key == null || values.size() != 2) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'getrange' command");
//                return true;
//            }
//            int startPos, endPos;
//            try {
//                startPos = Integer.parseInt(new String(values.get(0)));
//                endPos = Integer.parseInt(new String(values.get(1)));
//            } catch (NumberFormatException e) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR value is not an integer or out of range");
//                return true;
//            }
//            String val = redisServer.getRange(key, startPos, endPos);
//            if (val == null) {
//                val = "";
//            }
//            response.setType(Response.Type.BULK);
//            response.addValue(val);
//            return true;
//        } else if (Command.GETSET == cmd) {
//            if (key == null || values.size() != 1) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'getset' command");
//                return true;
//            }
//            String val = redisServer.get(key);
//            redisServer.set(key, values.get(0));
//            if (val == null) {
//                response.setType(Response.Type.BULK);
//                response.addNullValue();
//            } else {
//                response.setType(Response.Type.BULK);
//                response.addValue(val);
//            }
//            return true;
//        } else if (Command.INCR == cmd) {
//            if (key == null && values.size() != 0) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'incr' command");
//                return true;
//            }
//            String val = redisServer.get(key);
//            if (val == null) {
//                redisServer.set(key, new String("1").getBytes());
//                response.setType(Response.Type.INT);
//                response.addValue(1);
//                return true;
//            }
//            try {
//                int valInt = Integer.parseInt(val);
//                valInt += 1;
//                redisServer.set(key, (valInt + "").getBytes());
//                response.setType(Response.Type.INT);
//                response.addValue(valInt);
//                return true;
//            } catch (NumberFormatException e) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR value is not an integer or out of range");
//                return true;
//            }
//        } else if (Command.INCRBY == cmd) {
//            if (key == null || values.size() != 1) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'incrby' command");
//                return true;
//            }
//            String val = redisServer.get(key);
//            if (val == null) {
//                val = "0";
//            }
//            int valInt, incrNum;
//            try {
//                valInt = Integer.parseInt(val);
//                incrNum = Integer.parseInt(new String(values.get(0)));
//            } catch (NumberFormatException e) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR value is not an integer or out of range");
//                return true;
//            }
//            redisServer.set(key, (valInt + incrNum + "").getBytes());
//            response.setType(Response.Type.INT);
//            response.addValue(valInt + incrNum);
//            return true;
//        } else if (Command.MGET == cmd) {
//            if (key == null) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'mget' command");
//                return true;
//            }
//            List<String> list = new ArrayList<>();
//            values.add(key);
//            for (byte[] val : values) {
//                list.add(redisServer.get(val));
//            }
//            response.setType(Response.Type.MBULK);
//            response.addValue(list);
//            return true;
//        } else if (Command.MSET == cmd) {
//            if (key == null || values.size() % 2 != 1) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for MSET");
//                return true;
//            }
//            values.add(key);
//            for (int i = 0; i < values.size(); i += 2) {
//                redisServer.set(values.get(i), values.get(i + 1));
//            }
//            response.setType(Response.Type.SINGLE);
//            response.addValue(RedisDB.OK.OK);
//            return true;
//        } else if (Command.MSETNX == cmd) {
//
//        } else if (Command.SET == cmd) {
//            if (key == null || values.size() == 0) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'set' command");
//                return true;
//            } else if (values.size() >= 2) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR syntax error");
//            }
//            redisServer.set(key, values.get(0));
//            response.setType(Response.Type.SINGLE);
//            response.addValue(RedisDB.OK.OK);
//            return true;
//        } else if (Command.SETBIT == cmd) {
//            if (key == null || values.size() != 2) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("");
//                return true;
//            }
//            //TODO 这里同gitBit一样。是对byte进行操作。没有太理解。
//
//        } else if (Command.SETEX == cmd) {
//
//        } else if (Command.SETNX == cmd) {
//
//        } else if (Command.SETRANGE == cmd) {
//            if (key == null || values.size() != 2) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'setrange' command");
//                return true;
//            }
//            try {
//                int pos = Integer.parseInt(new String(values.get(0)));
//                String val = new String(values.get(1));
//                String old = redisServer.get(key);
//                if (old == null || old.isEmpty()) {
//                    old = "";
//                }
//                StringBuffer newStr = new StringBuffer(old);
//                //TODO append byte的问题没有解决。
//
//            } catch (NumberFormatException e) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR value is not an integer or out of range");
//                return true;
//            }
//        } else if (Command.STRLEN == cmd) {
//            if (key == null || values.size() != 0) {
//                response.setType(Response.Type.ERROR);
//                response.addValue("ERR wrong number of arguments for 'strlen' command");
//                return true;
//            }
//            int val = redisServer.strlen(key);
//            response.setType(Response.Type.INT);
//            response.addValue(val);
//            return true;
//        }
//        return false;
//    }
}
