package org.redis.server.rpcimpl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.redis.server.handlers.ISortedSetHandler;
import org.redis.server.protocol.Command;
import org.redis.server.protocol.Options;
import org.redis.server.protocol.Response;
import org.redis.server.serverapi.AbstractServer;
import org.redis.server.util.ByteArray;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author LuoJie on 2015/11/15.
 */
public class SortedSetHandler extends ISortedSetHandler {
    private AbstractServer redisServer;

    public SortedSetHandler(AbstractServer redisServer) {
        this.redisServer = redisServer;
    }
    @Override
    protected boolean handleSortedSetCommands(Command cmd, byte[] key, List<byte[]> values, Response response) {
        if (Command.ZADD == cmd) {
            if (key == null || values.size() % 2 != 0) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR syntax error");
                return true;
            }
            if (values.size() == 2) {
                long val = redisServer.zadd(key, ByteArray.bytesTolong(values.get(0)), values.get(1));
                response.setType(Response.Type.INT);
                response.addValue(val);
            } else {
                Map<Double, byte[]> map = Maps.newHashMap();
                for (int i = 0; i < values.size(); i += 2) {
                    map.put((double)ByteArray.bytesTolong(values.get(i)), values.get(i + 1));
                }

                long val = redisServer.zadd(key, map);
                response.setType(Response.Type.INT);
                response.addValue(val);
            }
        } else if (Command.ZCARD == cmd) {
            if (key == null) {
                if (values != null && !values.isEmpty()) {
                    response.setType(Response.Type.ERROR);
                    response.addValue("ERR wrong number of arguments for 'zcard' command");
                    return true;
                }
            }
            long val = redisServer.zcard(key);
            response.setType(Response.Type.INT);
            response.addValue(val);
        } else if (Command.ZCOUNT == cmd) {
            if (key == null || values.size() != 2) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'zcount' command");
                return true;
            }
            long val = redisServer.zcount(
                    key, ByteArray.bytesTolong(values.get(0)), ByteArray.bytesTolong(values.get(1)));
            response.setType(Response.Type.INT);
            response.addValue(val);
        } else if (Command.ZINCRBY == cmd) {
            if (key == null || values.size() != 2) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'zincrby' command");
                return true;
            }
            double val = redisServer.zincrby(
                    key, (double)ByteArray.bytesTolong(values.get(0)), values.get(1));
            response.setType(Response.Type.BULK);
            response.addValue(String.valueOf(val));
        } else if (Command.ZRANGE == cmd) {
            if (key == null || values.size() < 2 || values.size() > 3) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'zrange' command");
                return true;
            }
            List<byte[]> vals = Lists.newArrayList();
            if (values.size() == 2) {
                vals.addAll(redisServer.zrange(key,
                        ByteArray.bytesToInt(values.get(0)),
                        ByteArray.bytesToInt(values.get(1))));
            } else if (values.size() == 3) {
                if (!Arrays.equals(Options.WITHSCORES.raw,
                        new String(values.get(2)).toUpperCase().getBytes())) {
                    response.setType(Response.Type.ERROR);
                    response.addValue("ERR syntax error");
                    return true;
                }
                vals.addAll(redisServer.zrangeWithScores(key,
                        ByteArray.bytesToInt(values.get(0)),
                        ByteArray.bytesToInt(values.get(1))));
            } else {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR syntax error");
                return true;
            }
            response.setType(Response.Type.MBULK);
            response.setValues(vals);
        } else if (Command.ZRANGEBYSCORE == cmd) {
            if (key == null || values.size() < 2) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'zrangebyscore' command");
                return true;
            }
            List<byte[]> vals = Lists.newArrayList();
            if (values.size() == 2) {
                if (ByteArray.isNumber(values.get(0))) {
                    vals.addAll(redisServer.zrangeByScore(key,
                            ByteArray.bytesToInt(values.get(0)),
                            ByteArray.bytesToInt(values.get(1))));
                } else {
                    vals.addAll(redisServer.zrangeByScore(key, values.get(0), values.get(1)));
                }
            } else if (values.size() == 3) {
                if (!Arrays.equals(Options.WITHSCORES.raw,
                        new String(values.get(2)).toUpperCase().getBytes())) {
                    response.setType(Response.Type.ERROR);
                    response.addValue("ERR syntax error");
                    return true;
                }
                if (ByteArray.isNumber(values.get(0))) {
                    vals.addAll(redisServer.zrangeByScoreWithScores(key,
                            ByteArray.bytesTolong(values.get(0)),
                            ByteArray.bytesTolong(values.get(1))));
                } else {
                    vals.addAll(redisServer.zrangeByScoreWithScores(key,
                            values.get(0),
                            values.get(1)));
                }
            } else if (values.size() == 5) {
                if (!Arrays.equals(Options.LIMIT.raw,
                        new String(values.get(2)).toUpperCase().getBytes())) {
                    response.setType(Response.Type.ERROR);
                    response.addValue("ERR syntax error");
                    return true;
                }
                vals.addAll(redisServer.zrangeByScore(key,
                        ByteArray.bytesTolong(values.get(0)),
                        ByteArray.bytesTolong(values.get(1)),
                        ByteArray.bytesToInt(values.get(3)),
                        ByteArray.bytesToInt(values.get(4))));
            } else if (values.size() == 6) {
                if (!Arrays.equals(Options.WITHSCORES.raw,
                        new String(values.get(2)).toUpperCase().getBytes()) ||
                        !Arrays.equals(Options.LIMIT.raw,
                                new String(values.get(3)).toUpperCase().getBytes())) {
                    response.setType(Response.Type.ERROR);
                    response.addValue("ERR syntax error");
                    return true;
                }
                vals.addAll(redisServer.zrangeByScoreWithScores(key,
                        ByteArray.bytesTolong(values.get(0)),
                        ByteArray.bytesTolong(values.get(1)),
                        ByteArray.bytesToInt(values.get(4)),
                        ByteArray.bytesToInt(values.get(5))));
            } else {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR syntax error");
                return true;
            }
            response.setType(Response.Type.MBULK);
            response.setValues(vals);
        } else if (Command.ZRANK == cmd) {
            if (key == null || values.size() != 1) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'zrank' command");
                return true;
            }
            long val = redisServer.zrank(key, values.get(0));
            response.setType(Response.Type.INT);
            response.addValue(val);
        } else if (Command.ZREM == cmd) {
            if (key == null || values.isEmpty()) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'zrem' command");
                return true;
            }
            long val = redisServer.zrem(key, ByteArray.appendList(new byte[0], values));
            response.setType(Response.Type.INT);
            response.addValue(val);
        } else if (Command.ZREMRANGEBYSCORE == cmd) {
            if (key == null || values.size() != 2) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'zremrangebyscore' command");
                return true;
            }
            long val = redisServer.zremrangeByScore(key,
                    ByteArray.bytesTolong(values.get(0)),
                    ByteArray.bytesTolong(values.get(1)));
            response.setType(Response.Type.INT);
            response.addValue(val);
        } else if (Command.ZREVRANGE == cmd) {
            if (key == null || values.size() < 2 || values.size() > 3) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'zrevrange' command");
                return true;
            }
            List<byte[]> vals = Lists.newArrayList();
            if (values.size() == 2) {
                vals.addAll(redisServer.zrevrange(key,
                        ByteArray.bytesToInt(values.get(0)),
                        ByteArray.bytesToInt(values.get(1))));
            } else if (values.size() == 3) {
                if (!Arrays.equals(Options.WITHSCORES.raw,
                        new String(values.get(2)).toUpperCase().getBytes())) {
                    response.setType(Response.Type.ERROR);
                    response.addValue("ERR syntax error");
                    return true;
                }
                vals.addAll(redisServer.zrevrangeWithScores(key,
                        ByteArray.bytesToInt(values.get(0)),
                        ByteArray.bytesToInt(values.get(1))));
            }
            response.setType(Response.Type.MBULK);
            response.setValues(vals);
        } else if (Command.ZREVRANGEBYSCORE == cmd) {
            if (key == null || values.size() < 2) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'zrevrangebyscore' command");
                return true;
            }
            List<byte[]> vals = Lists.newArrayList();
            if (values.size() == 2) {
                vals.addAll(redisServer.zrevrangeByScore(key,
                        ByteArray.bytesToInt(values.get(0)),
                        ByteArray.bytesToInt(values.get(1))));
            } else if (values.size() == 3) {
                if (!Arrays.equals(Options.WITHSCORES.raw,
                        new String(values.get(2)).toUpperCase().getBytes())) {
                    response.setType(Response.Type.ERROR);
                    response.addValue("ERR syntax error");
                    return true;
                }
                vals.addAll(redisServer.zrevrangeByScoreWithScores(key,
                        ByteArray.bytesTolong(values.get(0)),
                        ByteArray.bytesTolong(values.get(1))));
            } else if (values.size() == 5) {
                if (!Arrays.equals(Options.LIMIT.raw,
                        new String(values.get(2)).toUpperCase().getBytes())) {
                    response.setType(Response.Type.ERROR);
                    response.addValue("ERR syntax error");
                    return true;
                }
                vals.addAll(redisServer.zrevrangeByScore(key,
                        ByteArray.bytesTolong(values.get(0)),
                        ByteArray.bytesTolong(values.get(1)),
                        ByteArray.bytesToInt(values.get(3)),
                        ByteArray.bytesToInt(values.get(4))));
            } else if (values.size() == 6) {
                if (!Arrays.equals(Options.WITHSCORES.raw,
                        new String(values.get(2)).toUpperCase().getBytes()) ||
                        !Arrays.equals(Options.LIMIT.raw,
                                new String(values.get(3)).toUpperCase().getBytes())) {
                    response.setType(Response.Type.ERROR);
                    response.addValue("ERR syntax error");
                    return true;
                }
                vals.addAll(redisServer.zrevrangeByScoreWithScores(key,
                        ByteArray.bytesTolong(values.get(0)),
                        ByteArray.bytesTolong(values.get(1)),
                        ByteArray.bytesToInt(values.get(4)),
                        ByteArray.bytesToInt(values.get(5))));
            } else {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR syntax error");
                return true;
            }
            response.setType(Response.Type.MBULK);
            response.setValues(vals);
        } else if (Command.ZSCORE == cmd) {
            if (key == null || values.size() != 1) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'zscore' command");
                return true;
            }
            Double val = redisServer.zscore(key, values.get(0));
            response.setType(Response.Type.BULK);
            response.addValue(String.valueOf(val));
        } else {
            return false;
        }
        return true;
    }
}
