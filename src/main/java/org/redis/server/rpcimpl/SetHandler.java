package org.redis.server.rpcimpl;

import org.redis.server.handlers.ISetHandler;
import org.redis.server.protocol.Command;
import org.redis.server.protocol.Response;
import org.redis.server.serverapi.AbstractServer;

import java.util.*;

/**
 * @author LuoJie
 * @date 2015/11/15.
 * @Modify LiMingji
 * @date 2015/11/22
 */
public class SetHandler extends ISetHandler {
    private AbstractServer redisServer;

    public SetHandler(AbstractServer redisServer) {
        this.redisServer = redisServer;
    }

    @Override
    protected boolean handleSetCommands(Command cmd, byte[] key, List<byte[]> values, Response response) {
        if (Command.SADD == cmd) {
            if (key == null || values.size() == 0) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'sadd' command");
                return true;
            }
            long val = redisServer.sadd(key, values.toArray(new byte[values.size()][]));
            response.setType(Response.Type.INT);
            response.addValue(val);
            return true;
        } else if (Command.SREM == cmd) {
            if (key == null || values.size() == 0) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'srem' command");
                return true;
            }
            long val = redisServer.srem(key, values.toArray(new byte[values.size()][]));
            response.setType(Response.Type.INT);
            response.addValue(val);
            return true;
        } else if (Command.SMEMBERS == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'smembers' command");
                return true;
            }

            // TODO 判空
            Set<String> vals = new HashSet<>(redisServer.smembers(key));
            if (vals == null) {
                // TODO
                response.setType(Response.Type.MBULK);
                response.setValues(new ArrayList<byte[]>());
            } else {
                response.setType(Response.Type.MBULK);
                response.addValue(vals);
                vals.clear();
            }
            return true;
        } else if (Command.SISMEMBER == cmd) {
            if (key == null || values.size() != 1) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'sismember' command");
                return true;
            }
            long val = redisServer.sismember(key, values.get(0)) ? 1 : 0;
            response.setType(Response.Type.INT);
            response.addValue(val);
            return true;
        } else if (Command.SCARD == cmd) {
            if (key == null || values.size() > 0) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'scard' command");
                return true;
            }
            Long val = redisServer.scard(key);
            if (val == null) {
                response.setType(Response.Type.INT);
                response.addValue("0");
            } else {
                response.setType(Response.Type.INT);
                response.addValue(val);
            }
            return true;
        } else if (Command.SMOVE == cmd) {
            if (key == null || values.size() != 2) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'smove' command");
                return true;
            }
            if (!redisServer.sismember(key, values.get(1))) {
                response.setType(Response.Type.INT);
                response.addValue(0L);
                return true;
            }
            redisServer.srem(key, values.get(1));
            long val = redisServer.sadd(values.get(0), values.get(1));
            response.setType(Response.Type.INT);
            response.addValue(val);
            return true;
        } else if (Command.SPOP == cmd) {
            if (key == null || values.size() > 0) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'spop' command");
                return true;
            }
            byte[] val = redisServer.spop(key);
            if (val == null) {
                response.setType(Response.Type.BULK);
                response.addNullValue();
            } else {
                response.setType(Response.Type.BULK);
                response.addValue(val);
            }
            return true;
        } else if (Command.SRANDMEMBER == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'srandmember' command");
                return true;
            }
            if (values.size() > 1) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR syntax error");
                return true;
            }
            List<String> list = new LinkedList<>();
            if (values.size() == 0) {
                list = redisServer.srandmember(key, 0, true);
            } else if (values.size() == 1) {
                try {
                    list = redisServer.srandmember(key, Integer.parseInt(new String(values.get(0))), false);
                } catch (NumberFormatException e) {
                    response.setType(Response.Type.ERROR);
                    response.addValue("ERR value is not an integer or out of range");
                    return true;
                }
            }
            // TODO
            if (list == null || list.size() == 0) {
                response.setType(Response.Type.BULK);
                response.addValue("(empty list or set)");
            } else {
                response.setType(Response.Type.MBULK);
                response.addValue(list);
            }
            return true;
        } else if (Command.SINTER == cmd) {
            // TODO 抽象出一个集合操作类
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'sinter' command");
                return true;
            }
            Set<String> set = new HashSet<>(redisServer.smembers(key));
            for (byte[] val : values) {
                Set<String> tmp = redisServer.smembers(val);
                if (tmp != null) {
                    set.retainAll(tmp);
                }
            }
            // TODO
            if (set.size() == 0) {
                response.setType(Response.Type.BULK);
                response.addValue("(empty list or set)");
            } else {
                response.setType(Response.Type.MBULK);
                response.addValue(set);
                set.clear();
            }
            return true;
        } else if (Command.SINTERSTORE == cmd) {
            if (key == null || values.size() == 0) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'sinterstore' command");
                return true;
            }
            redisServer.sclear(key);
            Set<String> set = new HashSet<>(redisServer.smembers(values.get(0)));
            for (int i = 1; i < values.size(); i++) {
                Set<String> tmp = redisServer.smembers(values.get(i));
                if (tmp != null) {
                    set.retainAll(tmp);
                }
            }
            // TODO
            if (set.size() == 0) {
                response.setType(Response.Type.BULK);
                response.addValue("(empty list or set)");
            } else {
                for (String val : set) {
                    redisServer.sadd(key, val.getBytes());
                }
                response.setType(Response.Type.INT);
                response.addValue(set.size());
                set.clear();
            }
            return true;
        } else if (Command.SUNION == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'sunion' command");
                return true;
            }
            values.add(key);
            Set<String> set = new HashSet<>();
            for (byte[] val : values) {
                Set<String> tmp = redisServer.smembers(val);
                if (tmp != null) {
                    set.addAll(tmp);
                }
            }

            // TODO
            if (set.size() == 0) {
                response.setType(Response.Type.BULK);
                response.addValue("(empty list or set)");
            } else {
                response.setType(Response.Type.MBULK);
                response.addValue(set);
                set.clear();
            }
            return true;
        } else if (Command.SUNIONSTORE == cmd) {
            if (key == null || values.size() == 0) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'sunionstore' command");
                return true;
            }
            redisServer.sclear(key);
            Set<String> set = new HashSet<>();
            for (byte[] val : values) {
                Set<String> tmp = redisServer.smembers(val);
                if (tmp != null && !tmp.isEmpty()) {
                    set.addAll(tmp);
                }
            }
            if (set.size() == 0) {
                response.setType(Response.Type.BULK);
                response.addValue("(empty list or set)");
            } else {
                for (String val : set) {
                    redisServer.sadd(key, val.getBytes());
                }
                response.setType(Response.Type.INT);
                response.addValue(set.size());
                set.clear();
            }
            return true;
        } else if (Command.SDIFF == cmd) {
            if (key == null || values.size() == 0) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'sdiff' command");
                return true;
            }
            Set<String> setA = new HashSet<>(redisServer.smembers(key));
            if (setA.isEmpty()) {
                response.setType(Response.Type.BULK);
                response.addValue("(empty list or set)");
                return true;
            }
            for (byte[] val : values) {
                Set<String> setB = redisServer.smembers(val);
                if (setB == null || setB.size() == 0) {
                    continue;
                }
                setA.removeAll(setB);
            }
            if (setA.isEmpty()) {
                response.setType(Response.Type.MBULK);
                response.addValue("(empty list or set)");
            } else {
                response.setType(Response.Type.MBULK);
                response.addValue(setA);
            }
            return true;
        } else if (Command.SDIFFSTORE == cmd) {
            if (key == null || values.size() <= 1) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'sdiffstore' command");
                return true;
            }
            redisServer.sclear(key);
            Set<String> setA = new HashSet<>(redisServer.smembers(values.get(0)));
            if (setA.isEmpty()) {
                response.setType(Response.Type.INT);
                response.addValue(0L);
                return true;
            }
            for (int i = 1; i < values.size(); i++) {
                Set<String> setB = redisServer.smembers(values.get(i));
                if (setB == null || setB.size() == 0) {
                    continue;
                }
                setA.removeAll(setB);
            }
            long count = 0;
            for (String val : setA) {
                redisServer.sadd(key, val.getBytes());
                count += 1;
            }
            response.setType(Response.Type.INT);
            response.addValue(count);
            return true;
        }
        return false;
    }
}
