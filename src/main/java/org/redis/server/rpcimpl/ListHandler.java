package org.redis.server.rpcimpl;

import org.redis.server.handlers.IListHandler;
import org.redis.server.protocol.Command;
import org.redis.server.protocol.Response;
import org.redis.server.serverapi.AbstractServer;
import org.redis.server.util.ByteArray;

import java.util.List;

/**
 * @author LuoJie on 2015/11/15.
 */
public class ListHandler extends IListHandler {
    private AbstractServer redisServer;

    public ListHandler(AbstractServer redisServer) {
        this.redisServer = redisServer;
    }

    @Override
    protected boolean handleListCommands(Command cmd, byte[] key, List<byte[]> values, Response response) {
        if (Command.LPOP == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'lpop' command");
                return true;
            }
            byte[] val = redisServer.lpop(key);
            response.setType(Response.Type.BULK);
            if (val == null) response.addNullValue();
            else response.addValue(val);
        } else if (Command.BLPOP == cmd) {
            if (key == null /** || !values.isEmpty() **/
                    ) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'blpop' command");
                return true;
            }
            byte[] val = redisServer.blpop(key);
            response.setType(Response.Type.BULK);
            response.addValue(val);
        } else if (Command.RPOP == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'rpop' command");
                return true;
            }
            byte[] val = redisServer.rpop(key);
            response.setType(Response.Type.BULK);
            response.addValue(val);
        } else if (Command.LRANGE == cmd) {
            if (values.size() != 2) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'lrange' command");
                return true;
            }
            int start = 0;
            int stop = 0;
            try {
                start = ByteArray.bytesToInt(values.get(0));
                stop = ByteArray.bytesToInt(values.get(1));
            } catch (NumberFormatException e) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR value is not an integer or out of range");
                return true;
            }
            List<byte[]> vals = redisServer.lrange(key, start, stop);
            response.setType(Response.Type.MBULK);
            response.setValues(vals);
        } else if (Command.LPUSH == cmd) {
            if (key == null || values == null || values.isEmpty()) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'lpush' command");
                return true;
            }
            Long val = redisServer.lpush(key, values.toArray(new byte[values.size()][]));
            response.setType(Response.Type.INT);
            response.addValue(val);
        } else if (Command.RPUSH == cmd) {
            if (key == null || values == null || values.isEmpty()) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'rpush' command");
                return true;
            }
            long val = redisServer.rpush(key, values.toArray(new byte[values.size()][]));
            response.setType(Response.Type.INT);
            response.addValue(val);
        } else if (Command.LPUSHX == cmd) {
            if (key == null || values == null || values.isEmpty()) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'lpush' command");
                return true;
            }
            Long val = redisServer.lpushx(key, values.get(0));
            response.setType(Response.Type.INT);
            response.addValue(val);
        } else if (Command.RPUSHX == cmd) {
            if (key == null || values == null || values.isEmpty()) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'lpush' command");
                return true;
            }
            Long val = redisServer.rpushx(key, values.get(0));
            response.setType(Response.Type.INT);
            response.addValue(val);
        } else if (Command.LLEN == cmd) {
            if (key == null) {
                response.setType(Response.Type.ERROR);
                response.addValue("ERR wrong number of arguments for 'llen' command");
                return true;
            }
            Long val = redisServer.llen(key);
            response.setType(Response.Type.INT);
            response.addValue(val);
        } else {
            return false;
        }
        return true;
    }
}
