package org.redis.server.exception;

/**
 * @author mingji on 09/18/7
 */
public class WrongRedisEncodingException extends RuntimeException {
    public static final String EXCEPTION_MESSAGE = "WRONG Redis Encoding Type";

    public WrongRedisEncodingException() {
        super(EXCEPTION_MESSAGE);
    }
}
