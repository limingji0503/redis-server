package org.redis.server.exception;

/**
 * @author roger on 16/1/7
 */
public class WrongTypeException extends RuntimeException {
    public static final String EXCEPTION_MESSAGE = "WRONGTYPE Operation against a key holding the wrong kind of value";

    public WrongTypeException() {
        super(EXCEPTION_MESSAGE);
    }
}
