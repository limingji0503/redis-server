package org.redis.server.exception;

public class NotImplementedCommandException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NotImplementedCommandException() {
		
	}
	public NotImplementedCommandException(String exception) {
		super(exception);
	}
}
