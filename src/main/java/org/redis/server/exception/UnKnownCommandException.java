package org.redis.server.exception;

public class UnKnownCommandException extends Exception {

	private static final long serialVersionUID = 7251134305536476279L;

	public UnKnownCommandException(String exception) {
		super(exception);
	}
}
