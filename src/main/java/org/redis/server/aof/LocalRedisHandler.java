package org.redis.server.aof;

import org.apache.log4j.Logger;
import org.redis.server.handlers.CommandHandler;
import org.redis.server.handlers.HandlerMap;
import org.redis.server.protocol.Command;
import org.redis.server.protocol.Response;
import org.redis.server.protocol.SimpleRequest;

import java.util.List;

/**
 * @author roger on 15/12/28
 */
public class LocalRedisHandler {
    private Logger logger = Logger.getLogger(LocalRedisHandler.class);
    private HandlerMap handlerMap;

    public LocalRedisHandler(HandlerMap handlerMap) {
        this.handlerMap = handlerMap;
    }

    public void messageReceived(SimpleRequest request) throws Exception {
        Command cmd = request.getCmd();
        Response response = new Response();
        if (cmd == null) {
            String exception = "unknown command.";
            logger.warn(exception);
            throw new IllegalArgumentException(exception);
        } else {
            byte[] key = request.getKey();
            List<byte[]> values = request.getVals();

            boolean cmdFound = false;
            CommandHandler handler = handlerMap.getHandler(cmd);
            if (handler != null) {
                cmdFound = handler.handleCommands(cmd, key, values, response);
            }
            if (!cmdFound) {
                String exception = "Command " + cmd + " not found!";
                logger.warn(exception);
                throw new IllegalArgumentException(exception);
            }
        }
    }
}
