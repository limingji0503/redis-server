package org.redis.server.aof;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.redis.server.abstractAOF.AOFConst;
import org.redis.server.abstractAOF.IAOF;
import org.redis.server.abstractAOF.Serializer;
import org.redis.server.typeapi.BinaryRedisDB;
import org.redis.server.typeapi.RedisType;
import org.redis.server.conf.Const;
import org.redis.server.handlers.HandlerMap;
import org.redis.server.protocol.Command;
import org.redis.server.protocol.SimpleRequest;
import org.redis.server.type.*;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author roger on 15/12/28
 */
public class AOFHandler implements IAOF {
    public static final Logger logger = Logger.getLogger(AOFHandler.class);
    private File appendOnlyFile;
    private Serializer serializer;
    private LocalRedisHandler redisHandler;
    private AtomicBoolean rewriteState;
    private CyclicBarrier barrier;

    public AOFHandler(HandlerMap handlerMap, String aofDir) {
        redisHandler = new LocalRedisHandler(handlerMap);
        serializer = new JsonSerializer();
        rewriteState = new AtomicBoolean(false);
        barrier = new CyclicBarrier(2);
        appendOnlyFile = new File(aofDir);
        if (!appendOnlyFile.exists())
            try {
                appendOnlyFile.createNewFile();
            } catch (IOException e) {
                logger.warn(ExceptionUtils.getStackTrace(e));
            }
    }

    private void bulkAppend(File file, List<SimpleRequest> requests) {
        StringBuilder sd = new StringBuilder();
        for (SimpleRequest request : requests) {
            sd.append(serializer.encode(request)).append("\n");
        }
        if (sd.length() > 0) {
            FileWriter fw = null;
            try {
                if (!file.exists()) {
                    file.createNewFile();
                }
                fw = new FileWriter(file, true);
                fw.write(sd.toString());
            } catch (IOException e) {
                logger.warn(ExceptionUtils.getStackTrace(e));
            } finally {
                if (fw != null)
                    try {
                        fw.close();
                    } catch (IOException e) {
                        logger.warn(ExceptionUtils.getStackTrace(e));
                    }
            }
        }
    }

    @Override
    public void writeAOF(List<SimpleRequest> requests) {
        if (requests.isEmpty()) return;

        if (rewriteState.get())
            try {
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                logger.warn(ExceptionUtils.getStackTrace(e));
            }

        bulkAppend(appendOnlyFile, requests);
    }

    @Override
    public void loadAOF(BinaryRedisDB db) {
        BufferedReader buf = null;
        try {
            if (!appendOnlyFile.exists()) {
                appendOnlyFile.createNewFile();
            }
            buf = new BufferedReader(new FileReader(appendOnlyFile));
            String line;
            while ((line = buf.readLine()) != null ){
                if (!StringUtils.isBlank(line))
                    loadToDB(db, line);
            }
        } catch (Exception e){
            logger.warn(ExceptionUtils.getStackTrace(e));
        } finally {
            if (buf != null){
                try{
                    buf.close();
                } catch (IOException e) {
                    logger.warn(ExceptionUtils.getStackTrace(e));
                }
            }
            logger.info("AppendOnlyFile load complete.");
        }
    }

    @Override
    public void rewrite(BinaryRedisDB db) throws Exception {
        rewriteState.set(true);
        File rewriteFile = new File(AOFConst.REDIS_AOF_REWRITE_FILENAME);
        long time = System.currentTimeMillis();
        try {
            Map<String, RedisType> map = db.getAll();
            for (Map.Entry<String, RedisType> entry : map.entrySet()) {
                RedisType type = entry.getValue();
                byte[] key = entry.getKey().getBytes();
                try {
                    type.lock();
                    List<SimpleRequest> requests = Lists.newArrayList();
                    if (type instanceof RedisList) {
                        RedisList list = (RedisList)type;
                        for (int i = 0 ; ;i++) {
                            byte[] val = list.lindexOf(i);
                            if (Arrays.equals(val, Const.BLANCK_BYTES)) break;

                            List<byte[]> vals = Lists.newArrayList();
                            vals.add(val);
                            requests.add(new SimpleRequest(Command.LPUSH, key, vals));
                        }
                    } else if (type instanceof RedisHash) {
                        RedisHash hash = (RedisHash)type;
                        List<byte[]> keyvalues = hash.hgetAll();

                        for (int i = 0; i < keyvalues.size(); i += 2) {
                            List<byte[]> vals = Lists.newArrayList();
                            vals.add(keyvalues.get(i));
                            vals.add(keyvalues.get(i + 1));
                            requests.add(new SimpleRequest(Command.HSET, key, vals));
                        }
                    } else if (type instanceof RedisString) {
                        RedisString str = (RedisString)type;
                        String val = str.get();
                        if (StringUtils.isBlank(val)) continue;

                        List<byte[]> vals = Lists.newArrayList();
                        vals.add(val.getBytes());
                        requests.add(new SimpleRequest(Command.SET, key, vals));
                    } else if (type instanceof RedisSet) {
                        RedisSet set = (RedisSet)type;
                        Set<String> members = set.smemebers();

                        for (String member : members) {
                            List<byte[]> vals = Lists.newArrayList();
                            vals.add(member.getBytes());
                            requests.add(new SimpleRequest(Command.SADD, key, vals));
                        }
                    } else if (type instanceof RedisZSet) {
                        RedisZSet zset = (RedisZSet)type;
                        List<byte[]> scoresAndMembers = zset.zrangeByScoreWithScores(
                                key, Const.REDIS_POSITIVE_INF, Const.REDIS_NEGETIVE_INF);

                        for (int i = 0; i < scoresAndMembers.size(); i += 2) {
                            List<byte[]> vals = Lists.newArrayList();
                            vals.add(scoresAndMembers.get(i + 1));
                            vals.add(scoresAndMembers.get(i));
                            requests.add(new SimpleRequest(Command.HSET, key, vals));
                        }
                    }

                    bulkAppend(rewriteFile, requests);
                } finally {
                    type.unlock();
                }
            }

            String aofName = appendOnlyFile.getName();
            rewriteFile.renameTo(new File(aofName));
            appendOnlyFile = new File(aofName);
        } finally {
            logger.info("AOF rewrite complete in "
                    + (System.currentTimeMillis() - time) + " milliseconds.");
            barrier.await();
            rewriteState.set(false);
        }
    }

    @Override
    public void close() throws Exception {

    }

    private void loadToDB(BinaryRedisDB db, String line) throws Exception {
        SimpleRequest request = serializer.decode(line);
        redisHandler.messageReceived(request);
    }
}
