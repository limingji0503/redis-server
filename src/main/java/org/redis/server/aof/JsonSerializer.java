package org.redis.server.aof;

import com.google.gson.Gson;
import org.redis.server.abstractAOF.Serializer;
import org.redis.server.protocol.Request;
import org.redis.server.protocol.SimpleRequest;

/**
 * @author LuoJie on 2015/12/28.
 */
public class JsonSerializer implements Serializer {
    private static Gson gson = new Gson();

    @Override
    public SimpleRequest decode(String line) {
        return gson.fromJson(line, SimpleRequest.class);
    }

    @Override
    public String encode(SimpleRequest request) {
        return gson.toJson(request);
    }
}
