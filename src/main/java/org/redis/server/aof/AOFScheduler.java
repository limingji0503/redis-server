package org.redis.server.aof;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.redis.server.abstractAOF.AOFConst;
import org.redis.server.abstractAOF.IAOF;
import org.redis.server.abstractAOF.ModifyCmds;
import org.redis.server.typeapi.BinaryRedisDB;
import org.redis.server.conf.RedisConfig;
import org.redis.server.conf.Const;
import org.redis.server.handlers.HandlerMap;
import org.redis.server.protocol.Request;
import org.redis.server.protocol.SimpleRequest;
import org.redis.server.util.AsyncActionsQueue;
import org.redis.server.util.ByteArray;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author roger on 15/12/28
 */
public class AOFScheduler {
    public static final Logger logger = Logger.getLogger(AOFScheduler.class);
    private volatile boolean isReady = false;
    private Timer timer;
    private IAOF aof;

    private BinaryRedisDB db;
    private AsyncActionsQueue<SimpleRequest> queue;

    public AOFScheduler(HandlerMap handlerMap, final BinaryRedisDB db, RedisConfig conf) {
        String fileName = conf.get(Const.REDIS_AOF_FILENAME) == null ?
                conf.get(Const.REDIS_AOF_FILENAME) : AOFConst.REDIS_AOF_FILENAME;
        this.db = db;

        String aof_open = conf.get(Const.REDIS_AOF_OPEN);
        String aof_interval = conf.get(Const.REDIS_AOF_INTERVAL);
        String flush_size = conf.get(Const.REDIS_AOF_FLUSHSIZE);
        String rewrite_interval = conf.get(Const.REDIS_AOF_REWRITE_INTERVAL);

        if (aof_open == null) {
            isReady = false;
            logger.info("redis aof-mode off.");
        } else if (aof_open.toLowerCase().equals(AOFConst.YES)) {
            long interval = ByteArray.isNumber(aof_interval)
                    ? AOFConst.REDIS_AOF_INTERVAL : Long.valueOf(aof_interval);
            long flushSize = flush_size != null && ByteArray.isNumber(flush_size)
                    ? Long.valueOf(flush_size) : AOFConst.REDIS_AOF_FLUSHSIZE;
            long rewriteInterval = rewrite_interval != null && ByteArray.isNumber(rewrite_interval)
                    ? Long.valueOf(rewrite_interval) : AOFConst.REDIS_AOF_REWRITE_INTERVAL;
            if (interval != 0) {
                aof = new AOFHandler(handlerMap, fileName);
                timer = new Timer();
                queue = new AsyncActionsQueue<SimpleRequest>(flushSize, interval) {
                    @Override
                    public void action(List<SimpleRequest> list) {
                        try {
                            aof.writeAOF(list);
                        } catch (Exception e) {
                            logger.warn(ExceptionUtils.getStackTrace(e));
                        }
                    }
                };
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            aof.rewrite(db);
                        } catch (Exception e) {
                            logger.warn(ExceptionUtils.getStackTrace(e));
                        }
                    }
                }, rewriteInterval, rewriteInterval);
                isReady = true;
                logger.info("redis aof-mode on.");
            } else {
                isReady = false;
                logger.info("redis aof-mode off.");
            }
        } else if (!aof_open.toLowerCase().equals(AOFConst.NO)){
            isReady = false;
            logger.warn("redis.aof.open config error.");
        }
    }

    public void loadAOF() {
        if (isReady) aof.loadAOF(db);
    }

    public void startAOF() {
        if (isReady) queue.start();
    }

    public void stopAOF() {
        if (isReady) queue.stop();
    }

    public void acceptCmd(Request request) {
        if (isReady) {
            if (ModifyCmds.isModification(request.getCommand())) {
                queue.put(request.toSimple());
            }
        }
    }
}
