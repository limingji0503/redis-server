package org.redis.server.typeapi;

import redis.clients.util.SafeEncoder;

import java.util.List;

/**
 * @author LuoJie
 * @date 2015/11/13.
 * @link http://doc.redisfans.com/
 */
public abstract class BinaryRedisList extends RedisType {

    protected static enum InnerCommand {
        BEFORE, AFTER;

        public final byte[] raw;
        InnerCommand() {
            raw = SafeEncoder.encode(this.name());
        }
    }

    /**
     *
     * @param timeout time unit is SECOND, 0 means always waiting. -1 means no waiting.
     * @return element
     */
    public abstract byte[] blpop(long timeout);

    /**
     *
     * @param timeout time unit is SECOND, 0 means always waiting. -1 means no waiting.
     * @return element
     */
    public abstract byte[] brpop(long timeout);

    /**
     *
     * @param destination
     * @param timeout
     * @return a list which size is 2: elements brpoped and timeout
     */
    public abstract List<byte[]> brpopLpush(BinaryRedisList destination, long timeout);

    /**
     *
     * @param index
     * @return value at the suffix of the list.
     */
    public abstract byte[] lindexOf(int index);

    /**
     *
     * @param command must be RedisList.InnerCommand
     * @param pivot
     * @param value
     * @return size of list, -1 means no pivot, 0 means no list
     */
    public abstract long linsert(byte[] command, int pivot, byte[] value);

    /**
     *
     * @return size of list
     */
    public abstract long llen();

    /**
     *
     * @return value
     */
    public abstract byte[] lpop();

    /**
     *
     * @param values
     * @return size of the current list.
     */
    public abstract long lpush(byte[][] values);

    /**
     *
     * @param start
     * @param stop
     * @return
     */
    public abstract List<byte[]> lrange(int start, int stop);

    /**
     *
     * @param count
     * @param value
     * @return count of element that success to remove; empty list always return 0.
     */
    public abstract long lrem(int count, byte[] value);

    /**
     *
     * @param index
     * @param value
     * @return OK or error info
     */
    public abstract BinaryRedisDB.OK lset(int index, byte[] value);

    /**
     *
     * @param start
     * @param stop
     * @return OK or error info
     */
    public abstract BinaryRedisDB.OK ltrim(int start, int stop);

    /**
     *
     * @return
     */
    public abstract byte[] rpop();

    /**
     *
     * @param destination
     * @return value rpoped from source list.
     */
    public abstract byte[] rpopLpush(BinaryRedisList destination);

    /**
     *
     * @param values
     * @return OK or error info
     */
    public abstract long rpush(byte[][] values);

    /**
     *
     * @param values
     * @return OK or error info
     */
    public abstract BinaryRedisDB.OK lpushx(byte[][] values);

    /**
     *
     * @param values
     * @return OK or error info
     */
    public abstract BinaryRedisDB.OK rpushx(byte[][] values);
}
