package org.redis.server.typeapi;

import org.redis.server.type.RedisDB;
import redis.clients.util.SafeEncoder;

import java.util.List;
import java.util.Set;

/**
 * @author LuoJie
 * @date 2015/11/13.
 * @link http://doc.redisfans.com/
 */
public abstract class BinaryRedisHash extends RedisType {
    protected static enum InnerCommand {
        MATCH, COUNT;

        public final byte[] raw;
        InnerCommand() {
            raw = SafeEncoder.encode(this.name());
        }
    }

    /**
     * Remove the fields
     * @param fields the fields to be removed
     * @return counts of the fields
     */
    public abstract long hdel(byte[][] fields);

    /**
     *
     * @param field
     * @return if the field exists return trues
     */
    public abstract boolean hexists(byte[] field);

    /**
     *
     * @param field
     * @return value of the field, if field does not exists, return null
     */
    public abstract byte[] hget(byte[] field);

    /**
     *
     * @return all the values of all the fields
     */
    public abstract List<byte[]> hgetAll();

    /**
     *
     * @param field
     * @param increment
     * @return
     */
    public abstract RedisDB.OK hincrby(byte[] field, long increment);

    /**
     *
     * @param field
     * @param increment
     * @return
     */
    public abstract RedisDB.OK hincrbyFloat(byte[] field, float increment);

    /**
     *
     * @return
     */
    public abstract Set<String> hkeys();

    /**
     *
     * @return the counts of fields in this map, return 0 if the map does not exists.
     */
    public abstract long hlen();

    /**
     *
     * @param fields
     * @return
     */
    public abstract List<byte[]> hmget(byte[][] fields);

    /**
     *
     * @param fieldsAndValues
     * @return
     */
    public abstract RedisDB.OK hmset(byte[][] fieldsAndValues);

    /**
     *
     * @param field
     * @param value
     * @return
     */
    public abstract boolean hset(byte[] field, byte[] value);

    /**
     *
     * @param field
     * @param value
     * @return
     */
    public abstract boolean hsetTnx(byte[] field, byte[] value);

    /**
     *
     * @return
     */
    public abstract List<byte[]> hvals();

    /**
     *
     * @param cmds
     * @return
     */
    public abstract List<byte[]> hscan(byte[][] cmds);
}
