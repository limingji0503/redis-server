package org.redis.server.typeapi;

import java.util.List;
import java.util.Set;

/**
 * @author LuoJie
 * @date 2015/11/13.
 * @link http://doc.redisfans.com/
 *
 *
 * @modify LiMingji
 * @date 2015/11/15
 */
public abstract class BinaryRedisSet extends RedisType {

    /**
     * Add the specified members to the set stored at key.Specified members that
     * are already a member of this set are ignored.
     * If key does not exist, a new set is created before adding the specified members.
     * An error is returned when the value stored at key is not a set.
     *
     * @param fields
     * @return the number of elements that were added to the set,
     * not including all the elements already present into the set.
     */
    public abstract long sadd(byte[][] fields);

    /**
     * Returns the set cardinality (number of elements) of the set stored at key.
     *
     * @return the cardinality (number of elements) of the set, or 0 if key does not exist.
     */
    public abstract long scard();

    /**
     * Returns the members of the set resulting from the difference between the first set and
     * all the successive sets.
     * <p/>
     * For examples:
     * key1 = {a,b,c,d}
     * key2 = {c}
     * key3 = {a,c,e}
     * SDIFF key1 key2 key3 = {b,d}
     *
     * @param values
     * @return list with members of the resulting set.
     */
    public abstract List<byte[]> sdiff(byte[][] values);

    /**
     * This command is equal to SDIFF, but instead of returning the resulting set, it is stored in destination.
     * If destination already exists, it is overwritten.
     *
     * @param values
     * @return the number of elements in the resulting set.
     */
    public abstract int sdiffstroe(byte[][] values);

    /**
     * For example:
     * key1 = {a,b,c,d}
     * key2 = {c}
     * key3 = {a,c,e}
     * SINTER key1 key2 key3 = {c}
     *
     * @param values
     * @return Returns the members of the set resulting from the intersection of all the given sets.
     */
    public abstract List<byte[]> sinter(byte[][] values);

    /**
     * This command is equal to SINTER, but instead of returning the resulting set,
     * it is stored in destination.
     * If destination already exists, it is overwritten.
     *
     * @param values
     * @return the number of elements in the resulting set.
     */
    public abstract int sintersotre(byte[][] values);

    /**
     * Returns if member is a member of the set stored at key.
     *
     * @param values
     * @return Integer reply, specifically:
     * 1 if the element is a member of the set.
     * 0 if the element is not a member of the set, or if key does not exist.
     */
    public abstract Boolean sismember(byte[] values);

    /**
     * Returns all the members of the set value stored at key.
     * This has the same effect as running SINTER with one argument key.
     *
     * @return all elements of the set.
     */
    public abstract Set<String> smemebers();

    /**
     * Move member from the set at source to the set at destination. This operation is atomic.
     * In every given moment the element will appear to be a member of source or destination for other clients.
     * <p/>
     * If the source set does not exist or does not contain the specified element,
     * no operation is performed and 0 is returned. Otherwise, the element is removed from the source set and added to the destination set.
     * When the specified element already exists in the destination set, it is only removed from the source set.
     * <p/>
     * An error is returned if source or destination does not hold a set value.
     *
     * @param values
     * @return Integer reply, specifically:
     * 1 if the element is moved.
     * 0 if the element is not a member of source and no operation was performed.
     */
    public abstract int smove(byte[][] values);

    /**
     * Removes and returns one or more random elements from the set value store at key.
     * This operation is similar to SRANDMEMBER, that returns one or more random elements from a set but does not remove it.
     *
     * @return Bulk string reply: the removed element, or nil when key does not exist.
     */
    public abstract byte[] spop();

    /**
     * When called with just the key argument, return a random element from the set value stored at key.
     *
     * @return Bulk string reply: without the additional count argument the command
     * returns a Bulk Reply with the randomly selected element, or nil when key does not exist.
     * <p/>
     * Array reply: when the additional count argument is passed the command returns an array of elements,
     * or an empty array when key does not exist.
     */
    public abstract List<String> srandmember(int n, boolean isNoArgs);

    /**
     * Remove the specified members from the set stored at key.
     * Specified members that are not a member of this set are ignored.
     * If key does not exist, it is treated as an empty set and this command returns 0.
     * An error is returned when the value stored at key is not a set.
     *
     * @param values
     * @return Integer reply: the number of members that were removed from the set,
     * not including non existing members.
     */
    public abstract long srem(byte[][] values);

    /**
     * Returns the members of the set resulting from the union of all the given sets.
     * key1 = {a,b,c,d}
     * key2 = {c}
     * key3 = {a,c,e}
     * SUNION key1 key2 key3 = {a,b,c,d,e}
     * <p/>
     * Keys that do not exist are considered to be empty sets.
     *
     * @param values
     * @return Array reply: list with members of the resulting set.
     */
    public abstract List<byte[]> sunion(byte[][] values);

    /**
     * This command is equal to SUNION, but instead of returning the resulting set,
     * it is stored in destination.
     * If destination already exists, it is overwritten.
     *
     * @param values
     * @return Integer reply: the number of elements in the resulting set.
     */
    public abstract int sunionstore(byte[][] values);

    /**
     * See SCAN for SSCAN documentation.
     * @refrernce http://redis.io/commands/sscan
     *
     * @param values
     * @return
     */
    public abstract List<byte[]> sscan(byte[][] values);

    /**
     * remove all elements in the set
     * @return
     */
    public abstract void sclear();
}
