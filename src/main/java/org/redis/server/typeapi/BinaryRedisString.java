package org.redis.server.typeapi;

import org.redis.server.exception.WrongRedisEncodingException;

/**
 * @author LuoJie
 * @date 2015/11/13.
 * @link http://doc.redisfans.com/
 * @modify LiMingji
 * @date 2015/11/15
 * @link http://redis.io/commands
 */
public abstract class BinaryRedisString extends RedisType {

    public enum RedisStringEncoding {
        REDIS_ENCODING_INT, REDIS_ENCODING_STRING
    }

    public String getRedisEncodingName (RedisStringEncoding encoding){
        if (encoding == RedisStringEncoding.REDIS_ENCODING_STRING){
            return "string";
        }else if(encoding == RedisStringEncoding.REDIS_ENCODING_INT){
            return "int";
        }else {
            throw new WrongRedisEncodingException();
        }
    }

    /**
     * If key already exists and is a string, this command appends the value at the end of the string.
     * If key does not exist it is created and set as an empty string, so APPEND will be similar to SET in this special case.
     *
     * @param values
     * @return Integer reply: the length of the string after the append operation.
     */
    public abstract int append(byte[] values);

    /**
     * Count the number of set bits (population counting) in a string.
     * By default all the bytes contained in the string are examined.
     * It is possible to specify the counting operation only in an interval passing the additional arguments start and end.
     *
     * @param values
     * @return
     */
    public abstract int bitcount(byte[][] values);

    /**
     * Perform a bitwise operation between multiple keys (containing string values) and store the result in the destination key.
     *
     * @param values
     * @return The size of the string stored in the destination key, that is equal to the size of the longest input string.
     */
    public abstract int bitop(byte[][] values);

    /**
     * Decrements the number stored at key by one. If the key does not exist, it is set to 0 before performing the operation.
     * An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.
     * This operation is limited to 64 bit signed integers.
     *
     * @param key
     * @return Integer reply: the value of key after the decrement
     */
    public abstract int decr(byte[] key);

    /**
     * Decrements the number stored at key by decrement.
     * If the key does not exist, it is set to 0 before performing the operation.
     * An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.
     * This operation is limited to 64 bit signed integers.
     *
     * @param fields
     * @return Integer reply: the value of key after the decrement
     */
    public abstract int decrby(byte[][] fields);


    /**
     * Get the value of key. If the key does not exist the special value nil is returned.
     * An error is returned if the value stored at key is not a string,
     * because GET only handles string values.
     *
     * @return Bulk string reply: the value of key, or nil when key does not exist.
     */
    public abstract String get();

    /**
     * Returns the bit value at offset in the string value stored at key.
     * When offset is beyond the string length, the string is assumed to be a contiguous space with 0 bits.
     * When key does not exist it is assumed to be an empty string,
     * so offset is always out of range and the value is also assumed to be a contiguous space with 0 bits.
     *
     * @param key
     * @return Integer reply: the bit value stored at offset.
     */
    public abstract int getbit(byte[] key);

    /**
     * Returns the substring of the string value stored at key, determined by the offsets start and end (both are inclusive).
     * Negative offsets can be used in order to provide an offset starting from the end of the string.
     * So -1 means the last character, -2 the penultimate and so forth.
     * The function handles out of range requests by limiting the resulting range to the actual length of the string.
     *
     * @param fields
     * @return Bulk string reply
     */
    public abstract byte[] getrange(byte[][] fields);

    /**
     * Atomically sets key to value and returns the old value stored at key.
     * Returns an error when key exists but does not hold a string value.
     *
     * @param fields
     * @return Bulk string reply: the old value stored at key, or nil when key did not exist.
     */
    public abstract byte[] getset(byte[][] fields);

    /**
     * Increments the number stored at key by one.
     * If the key does not exist, it is set to 0 before performing the operation.
     * An error is returned if the key contains a value of the wrong type or contains a string
     * that can not be represented as integer. This operation is limited to 64 bit signed integers.
     *
     * @param value
     * @return Integer reply: the value of key after the increment
     */
    public abstract int incr(byte[] value);

    /**
     * Increments the number stored at key by increment.
     * If the key does not exist, it is set to 0 before performing the operation.
     * An error is returned if the key contains a value of the wrong type or contains a string
     * that can not be represented as integer. This operation is limited to 64 bit signed integers.
     * See INCR for extra information on increment/decrement operations.
     *
     * @param fields
     * @return Integer reply: the value of key after the increment
     */
    public abstract int incrby(byte[][] fields);


    /**
     * Increment the string representing a floating point number stored at key by the specified increment.
     * If the key does not exist, it is set to 0 before performing the operation.
     * An error is returned if one of the following conditions occur:
     * The key contains a value of the wrong type (not a string).
     * The current key content or the specified increment are not parsable as a double precision floating point number.
     *
     * @param fields
     * @return Bulk string reply: the value of key after the increment.
     */
    public abstract byte[] incrfloat(byte[][] fields);

    /**
     * Returns the values of all specified keys.
     * For every key that does not hold a string value or does not exist, the special value nil is returned.
     * Because of this, the operation never fails.
     *
     * @param keys
     * @return Array reply: list of values at the specified keys.
     */
    public abstract byte[][] mget(byte[][] keys);

    /**
     * Sets the given keys to their respective values.
     * MSET replaces existing values with new values, just as regular SET.
     * See MSETNX if you don't want to overwrite existing values.
     * MSET is atomic, so all given keys are set at once.
     * It is not possible for clients to see that some of the keys were updated while others are unchanged.
     *
     * @param keys
     * @return Simple string reply: always OK since MSET can't fail.
     */
    public abstract byte[][] mset(byte[][] keys);

    /**
     * Sets the given keys to their respective values.
     * MSETNX will not perform any operation at all even if just a single key already exists.
     * Because of this semantic MSETNX can be used in order to set different keys representing different fields of
     * an unique logic object in a way that ensures that either all the fields or none at all are set.
     * MSETNX is atomic, so all given keys are set at once. It is not possible
     * for clients to see that some of the keys were updated while others are unchanged.
     *
     * @param fields
     * @return Integer reply, specifically:
     * 1 if the all the keys were set.
     * 0 if no key was set (at least one key already existed).
     */
    public abstract int msetnx(byte[][] fields);

    /**
     * PSETEX works exactly like SETEX with the sole difference that
     * the expire time is specified in milliseconds instead of seconds.
     *
     * @param fields
     * @return "OK" if succeed
     */
    public abstract byte[] psetex(byte[][] fields);

    /**
     * Set key to hold the string value. If key already holds a value, it is overwritten, regardless of its type.
     * Any previous time to live associated with the key is discarded on successful SET operation.
     *
     * @param fields
     * Simple string reply: OK if SET was executed correctly.
     * Null reply: a Null Bulk Reply is returned if the SET operation was not performed
     * because the user specified the NX or XX option but the condition was not met.
     */
    public abstract void set(byte[] fields);

    /**
     * Sets or clears the bit at offset in the string value stored at key.
     * The bit is either set or cleared depending on value, which can be either 0 or 1.
     * When key does not exist, a new string value is created.
     * The string is grown to make sure it can hold a bit at offset.
     * The offset argument is required to be greater than or equal to 0,
     * and smaller than 232 (this limits bitmaps to 512MB). When the string at key is grown,
     * added bits are set to 0.
     *
     * @param fields
     * @return
     */
    public abstract int setbit(byte[][] fields);

    /**
     * Set key to hold the string value and set key to timeout after a given number of seconds.
     * This command is equivalent to executing the following commands:
     *
     * @param fields
     * @return Simple string reply
     */
    public abstract byte[] setex(byte[][] fields);

    /**
     * Set key to hold string value if key does not exist. In that case,
     * it is equal to SET. When key already holds a value,
     * no operation is performed. SETNX is short for "SET if Not eXists".
     *
     * @param fields
     * @return Integer reply, specifically:
     * 1 if the key was set
     * 0 if the key was not set
     */
    public abstract int setnx(byte[][] fields);

    /**
     * Overwrites part of the string stored at key, starting at the specified offset,
     * for the entire length of value. If the offset is larger than the current length of the string at key,
     * the string is padded with zero-bytes to make offset fit. Non-existing keys are considered as empty strings,
     * so this command will make sure it holds a string large enough to be able to set value at offset.
     * Note that the maximum offset that you can set is 2^29 -1 (536870911), as Redis Strings are limited to 512 megabytes.
     * If you need to grow beyond this size, you can use multiple keys.
     *
     * @param fields
     * @return Integer reply: the length of the string after it was modified by the command.
     */
    public abstract int setrange(byte[][] fields);

    /**
     * Returns the length of the string value stored at key.
     * An error is returned when key holds a non-string value.
     *
     * @return Integer reply: the length of the string at key, or 0 when key does not exist.
     */
    public abstract int strlen();
}


