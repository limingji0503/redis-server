package org.redis.server.typeapi;

import java.util.List;
import java.util.Map;

/**
 * @author LuoJie
 * @date 2015/11/13.
 * @link http://doc.redisfans.com/
 */
public abstract class BinaryRedisZSet extends RedisType {


    public abstract long zadd(byte[] key, double score, byte[] member);

    public abstract long zadd(byte[] key, Map<Double, byte[]> scoreMembers);

    public abstract long zcard(byte[] key);

    public abstract long zcount(byte[] key, double min, double max);

    public abstract long zcount(byte[] key, byte[] min, byte[] max);

    public abstract double zincrby(byte[] key, double score, byte[] member);

    public abstract List<byte[]> zrange(byte[] key, long start, long end);

    public abstract List<byte[]> zrangeByScore(byte[] key, double min, double max);

    public abstract List<byte[]> zrangeByScore(byte[] key, byte[] min, byte[] max);

    public abstract List<byte[]> zrangeByScore(byte[] key, double min, double max, int offset,
                               int count);

    public abstract List<byte[]> zrangeByScore(byte[] key, byte[] min, byte[] max, int offset,
                               int count);

    public abstract Long zrank(byte[] key, byte[] member);

    public abstract long zrem(byte[] key, byte[]... members);

    public abstract long zremrangeByRank(byte[] key, long start, long end);

    public abstract long zremrangeByScore(byte[] key, double start, double end);

    public abstract long zremrangeByScore(byte[] key, byte[] start, byte[] end);

    public abstract List<byte[]> zrevrange(final byte[] key, final long start,
                           final long end);

    public abstract List<byte[]> zrevrangeByScore(byte[] key, double max, double min);

    public abstract List<byte[]> zrevrangeByScore(byte[] key, byte[] max, byte[] min);

    public abstract List<byte[]> zrevrangeByScore(byte[] key, double max, double min,
                                  int offset, int count);

    public abstract List<byte[]> zrangeWithScores(byte[] key, int start, int end);

    public abstract List<byte[]> zrevrangeWithScores(byte[] key, int start, int end);

    public abstract List<byte[]> zrangeByScoreWithScores(byte[] key, double min, double max);

    public abstract List<byte[]> zrevrangeByScoreWithScores(byte[] key, double max, double min);

    public abstract List<byte[]> zrangeByScoreWithScores(byte[] key, double min, double max,
                                         int offset, int count);

    public abstract List<byte[]> zrevrangeByScore(byte[] key, byte[] max, byte[] min,
                                 int offset, int count);

    public abstract List<byte[]> zrangeByScoreWithScores(byte[] key, byte[] min, byte[] max);

    public abstract List<byte[]> zrangeByScoreWithScores(byte[] key, byte[] min, byte[] max,
                                         int offset, int count);

    public abstract List<byte[]> zrevrangeByScoreWithScores(byte[] key, double max, double min,
                                            int offset, int count);

    public abstract List<byte[]> zrevrangeByScoreWithScores(byte[] key, byte[] max, byte[] min,
                                          int offset, int count);

    public abstract long zrevrank(byte[] key, byte[] member);

    public abstract double zscore(byte[] key, byte[] member);

    public abstract long zunionstore(final byte[] dstkey, final String... sets);

    public abstract long zinterstore(final byte[] dstkey, final String... sets);

}
