package org.redis.server.typeapi;

import java.util.Map;
import java.util.Set;

/**
 * @author LuoJie
 * @date 2015/11/13.
 */
public abstract class BinaryRedisDB {

    public abstract Object get(byte[] key);

    public abstract boolean delete(byte[] key);

    public abstract BinaryRedisHash getHash(byte[] key, boolean init);

    public abstract BinaryRedisList getList(byte[] key, boolean init);

    public abstract BinaryRedisSet getSet(byte[] key, boolean init);

    public abstract BinaryRedisString getString(byte[] key, boolean init);

    public abstract BinaryRedisZSet getSortedSet(byte[] key, boolean init);

    public abstract Set<String> getAllKeys();

    public abstract Map<String, RedisType> getAll();

    /**
     * @author LuoJie
     */
    public static final class OK {
        public static final byte[] OK = "OK".getBytes();
        private final boolean ok;
        private final byte[] info;
        private long aLong;
        private float aFloat;

        public OK() {
            this.ok = true;
            this.info = OK;
            this.aLong = 0L;
            this.aFloat = 0F;
        }

        public OK(byte[] info) {
            this.ok = false;
            this.info = (info == null) ? new byte[0] : info;
            this.aLong = 0L;
            this.aFloat = 0F;

        }

        public OK setLong(long val) {
            this.aLong = val;
            return this;
        }

        public long getLong() {
            return this.aLong;
        }

        public OK setFloat(float aFloat) {
            this.aFloat = aFloat;
            return this;
        }

        public float getFloat() {
            return this.aFloat;
        }

        public byte[] isOk() {
            return ok ? OK : info;
        }
    }
}
