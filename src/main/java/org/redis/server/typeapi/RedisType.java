package org.redis.server.typeapi;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author roger on 15/12/28
 */
public abstract class RedisType {

    private long ttl = -1L;
    private AtomicBoolean isLock = new AtomicBoolean(false);

    /**
     * Set unix time when the key survives until
     * @param ttl expire time
     */
    public void setTTL(long ttl) {
        this.ttl = ttl;
    }

    public long getTTL() {
        return ttl;
    }

    public void await() {
        while (!isLock.get());
    }

    public boolean lock() {
        return isLock.getAndSet(true);
    }

    public boolean unlock() {
        return isLock.getAndSet(false);
    }
}
