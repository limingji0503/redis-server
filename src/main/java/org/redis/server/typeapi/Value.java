package org.redis.server.typeapi;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author LuoJie
 * @date 2015/11/13.
 */
public class Value<T> {
    private volatile T data;
    private AtomicBoolean lock;

    public Value(T data) {
        this.data = data;
        lock = new AtomicBoolean(false);
    }
    private void waitForLock() {
        while (!lock.getAndSet(true));
    }

    private void returnLock() {
        lock.set(false);
    }

    public void set(T data) {
        waitForLock();
        this.data = data;
        returnLock();
    }

    public T get() {
        return data;
    }
}
