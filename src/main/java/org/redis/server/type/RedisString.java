package org.redis.server.type;

import org.redis.server.rawtype.RawInteger;
import org.redis.server.rawtype.RawString;
import org.redis.server.typeapi.BinaryRedisString;

/**
 * Created by LiMingji on 15/11/28.
 */
public class RedisString extends BinaryRedisString {

    private RawInteger            integer;
    private RawString             string;
    private RedisStringEncoding   encoding;

    public RedisString(){
        // 默认低层编码为integer
        this.integer  = new RawInteger();
        this.string   = null;
        this.encoding = integer.getEncoding();
    }

    // 切换低层编码。将编码方式由integer转成string
    // 有三种情况下需要对低层编码进行切换：
    // 1. 需要保存的数字是浮点数的时候。
    // 2. 数字太大超过integer的长度的时候。
    // 3. 需要进行string的操作的时候，如append
    private void integerToString() {
        if (encoding == RedisStringEncoding.REDIS_ENCODING_INT){
            this.encoding   = RedisStringEncoding.REDIS_ENCODING_STRING;
            this.string     = new RawString(integer);
            this.integer    = null;
        }
    }

    @Override
    public int append(byte[] values) {
        integerToString();
        if (encoding == RedisStringEncoding.REDIS_ENCODING_STRING) {
            return string.append(values);
        }
        return -1;
    }

    @Override
    public int bitcount(byte[][] values) {
        return 0;
    }

    @Override
    public int bitop(byte[][] values) {
        return 0;
    }

    @Override
    public int decr(byte[] key) {
        return 0;
    }

    @Override
    public int decrby(byte[][] fields) {
        return 0;
    }

    @Override
    public String get() {
        return "";
    }

    @Override
    public int getbit(byte[] key) {
        return 0;
    }

    @Override
    public byte[] getrange(byte[][] fields) {
        return new byte[0];
    }

    @Override
    public byte[] getset(byte[][] fields) {
        return new byte[0];
    }

    @Override
    public int incr(byte[] value) {
        return 0;
    }

    @Override
    public int incrby(byte[][] fields) {
        return 0;
    }

    @Override
    public byte[] incrfloat(byte[][] fields) {
        return new byte[0];
    }

    @Override
    public byte[][] mget(byte[][] keys) {
        return new byte[0][];
    }

    @Override
    public byte[][] mset(byte[][] keys) {
        return new byte[0][];
    }

    @Override
    public int msetnx(byte[][] fields) {
        return 0;
    }

    @Override
    public byte[] psetex(byte[][] fields) {
        return new byte[0];
    }

    @Override
    public void set(byte[] fields) {
        return;
    }

    @Override
    public int setbit(byte[][] fields) {
        return 0;
    }

    @Override
    public byte[] setex(byte[][] fields) {
        return new byte[0];
    }

    @Override
    public int setnx(byte[][] fields) {
        return 0;
    }

    @Override
    public int setrange(byte[][] fields) {
        return 0;
    }

    @Override
    public int strlen() {
        return 0;
    }
}
