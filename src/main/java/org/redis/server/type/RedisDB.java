package org.redis.server.type;

import com.google.common.collect.Maps;
import org.redis.server.exception.WrongTypeException;
import org.redis.server.typeapi.*;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

/**
 * @author roger on 15/11/13.
 */
public class RedisDB extends BinaryRedisDB {
    private ConcurrentMap<String, RedisType> keys;

    public RedisDB() {
        keys = Maps.newConcurrentMap();
    }

    private boolean expired(String key, RedisType type) {
        boolean result = type.getTTL() != -1L && System.currentTimeMillis() > type.getTTL();
        if (result) keys.remove(key);
        return result;
    }

    @Override
    public RedisType get(byte[] key) {
        return keys.get(key);
    }

    @Override
    public boolean delete(byte[] key) {
        return keys.remove(new String(key)) != null;
    }

    @Override
    public BinaryRedisHash getHash(byte[] key, boolean init) {
        String keyStr = new String(key);
        RedisType result;
        if (!keys.containsKey(keyStr)) {
            if (!init) return null;
            keys.putIfAbsent(keyStr, new RedisHash());
        }

        result = keys.get(keyStr);
        if (!result.getClass().equals(RedisHash.class)) throw new WrongTypeException();
        return expired(keyStr, result) ? null : (BinaryRedisHash)result;
    }

    @Override
    public BinaryRedisList getList(byte[] key, boolean init) {
        String keyStr = new String(key);
        RedisType result;
        if (!keys.containsKey(keyStr)) {
            if (!init) return null;
            keys.putIfAbsent(keyStr, new RedisList());
        }

        result = keys.get(keyStr);
        if (!result.getClass().equals(RedisList.class)) throw new WrongTypeException();
        return expired(keyStr, result) ? null : (BinaryRedisList)result;
    }

    @Override
    public BinaryRedisSet getSet(byte[] key, boolean init) {
        String keyStr = new String(key);
        RedisType result;
        if (!keys.containsKey(keyStr)) {
            if (!init) {
                return null;
            }
            keys.putIfAbsent(keyStr, new RedisSet());
        }
        result = keys.get(keyStr);
        if (!result.getClass().equals(RedisSet.class)) {
            throw new WrongTypeException();
        }
        return expired(keyStr, result) ? null : (BinaryRedisSet) result;
    }

    @Override
    public BinaryRedisString getString(byte[] key, boolean init) {
        String keyStr = new String(key);
        RedisType result;
        if (!keys.containsKey(keyStr)) {
            if (!init) {
                return null;
            }
            keys.putIfAbsent(keyStr, new RedisString());
        }
        result = keys.get(keyStr);
        if (!result.getClass().equals(RedisString.class)) {
            throw new WrongTypeException();
        }
        return expired(keyStr, result) ? null : (BinaryRedisString) result;
    }

    @Override
    public BinaryRedisZSet getSortedSet(byte[] key, boolean init) {
        return null;
    }

    @Override
    public Set<String> getAllKeys() {
        return keys.keySet();
    }

    @Override
    public Map<String, RedisType> getAll() {
        return keys;
    }
}
