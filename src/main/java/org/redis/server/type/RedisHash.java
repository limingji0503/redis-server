package org.redis.server.type;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.redis.server.typeapi.BinaryRedisHash;
import org.redis.server.util.ByteArray;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

/**
 * @author LuoJie
 * @date 2015/11/14.
 */
public class RedisHash extends BinaryRedisHash {
    private ConcurrentMap<String, byte[]> map;

    public RedisHash() {
        map = Maps.newConcurrentMap();
    }

    @Override
    public long hdel(byte[][] fields) {
        long count = 0L;
        for (byte[] filed : fields) {
            if (map.remove(new String(filed)) != null)
                count ++;
        }
        return count;
    }

    @Override
    public boolean hexists(byte[] field) {
        return map.containsKey(new String(field));
    }

    @Override
    public byte[] hget(byte[] field) {
        return map.get(new String(field));
    }

    @Override
    public List<byte[]> hgetAll() {
        List<byte[]> rel = Lists.newLinkedList();
        for (String key : map.keySet()) {
            rel.add(key.getBytes());
            rel.add(map.get(key));
        }
        return rel;
    }

    @Override
    public RedisDB.OK hincrby(byte[] field, long increment) {
        String key = new String(field);
        byte[] val = map.get(key);
        if (val == null) {
            return new RedisDB.OK(
                    String.valueOf(increment).getBytes());
        }
        try {
            int i = ByteArray.bytesToInt(val);
            i += increment;
            map.put(key, ByteArray.intToBytes(i));
            return new RedisDB.OK().setLong(i);
        } catch (NumberFormatException e) {
            return new RedisDB.OK(e.getMessage().getBytes());
        }
    }

    @Override
    public RedisDB.OK hincrbyFloat(byte[] field, float increment) {
        String key = new String(field);
        byte[] val = map.get(key);
        if (val == null) {
            return new RedisDB.OK(
                    String.valueOf(increment).getBytes());
        }
        try {
            float f = Float.valueOf(new String(val));
            f += increment;
            map.put(key, String.valueOf(f).getBytes());
            return new RedisDB.OK().setFloat(f);
        } catch (NumberFormatException e) {
            return new RedisDB.OK(e.getMessage().getBytes());
        }
    }

    @Override
    public Set<String> hkeys() {
        return map.keySet();
    }

    @Override
    public long hlen() {
        return map.size();
    }

    @Override
    public List<byte[]> hmget(byte[][] fields) {
        List<byte[]> rel = Lists.newArrayList();

        for (byte[] field : fields) {
            byte[] val = map.get(new String(field));
            if (val != null) {
                rel.add(val);
            }
        }
        return rel;
    }

    @Override
    public RedisDB.OK hmset(byte[][] fieldsAndValues) {
        for(int i = 0; i < fieldsAndValues.length; i += 2) {
            if (i + 1 > fieldsAndValues.length - 1) break;
            map.put(new String(fieldsAndValues[i]), fieldsAndValues[i+1]);
        }
        return new RedisDB.OK();
    }

    @Override
    public boolean hset(byte[] field, byte[] value) {
        map.put(new String(field), value);
        return true;
    }

    @Override
    public boolean hsetTnx(byte[] field, byte[] value) {
        map.put(new String(field), value);
        return true;
    }

    @Override
    public List<byte[]> hvals() {
        return (List<byte[]>)map.values();
    }

    @Override
    public List<byte[]> hscan(byte[][] cmds) {
        return null;
    }
}
