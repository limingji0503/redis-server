package org.redis.server.type;

import junit.framework.Test;
import junit.framework.TestSuite;
import junit.textui.TestRunner;
import org.redis.server.rawtype.SkipListTest;

public class TestType extends TestSuite {

    public static Test testAll(){
        TestSuite suite = new TestSuite("跳跃表测试");
        suite.addTestSuite(SkipListTest.class);
        return suite;
    }

    public static void main(String args[]){
        TestRunner.run(testAll());
    }
}
