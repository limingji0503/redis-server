package org.redis.server.type;

import com.google.common.collect.Sets;
import org.redis.server.typeapi.BinaryRedisSet;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by LiMingji on 2015/11/22.
 */
public class RedisSet extends BinaryRedisSet {
    private ConcurrentHashMap<String, byte[]> sets;
    
    Set<String> set = Sets.newConcurrentHashSet();

    public RedisSet() {
        sets = new ConcurrentHashMap<>();
    }

    @Override
    public long sadd(byte[][] fields) {
        long count = 0;
        if (fields == null) {
            return 0;
        }
        for (byte[] val : fields) {
            String valStr = new String(val);
            if (!sets.containsKey(valStr)) {
                sets.put(valStr, new byte[0]);
                count += 1;
            }
        }
        return count;
    }

    @Override
    public long scard() {
        return sets.size();
    }

    @Override
    public List<byte[]> sdiff(byte[][] values) {
        return null;
    }

    @Override
    public int sdiffstroe(byte[][] values) {
        return 0;
    }

    @Override
    public List<byte[]> sinter(byte[][] values) {
        return null;
    }

    @Override
    public int sintersotre(byte[][] values) {
        return 0;
    }

    @Override
    public Boolean sismember(byte[] values) {
        return sets.containsKey(new String(values));
    }

    @Override
    public Set<String> smemebers() {
        return sets.keySet();
    }

    @Override
    public int smove(byte[][] values) {
        return 0;
    }

    @Override
    public byte[] spop() {
        List<String> list = new ArrayList<>();
        list.addAll(sets.keySet());
        if (list.size() == 0) {
            return null;
        }

        Collections.shuffle(list);
        sets.remove(list.get(0));
        return list.get(0).getBytes();
    }

    @Override
    public List<String> srandmember(int n, boolean isNoArgs) {
        LinkedList<String> list = new LinkedList<>();
        list.addAll(sets.keySet());
        if (list.size() == 0) {
            return null;
        }
        if (list.size() <= n) {
            return list;
        }
        Collections.shuffle(list);
        if (isNoArgs) {
            List<String> listWithOneElement = new ArrayList<>();
            listWithOneElement.add(list.get(0));
            return listWithOneElement;
        }
        if (n >= 0) {
            List<String> listReturn = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                listReturn.add(list.pop());
            }
            return listReturn;
        } else if (n < 0) {
            n = -n;
            List<String> listReturn = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                listReturn.add(list.getFirst());
                Collections.shuffle(list);
            }
            return listReturn;
        }
        return null;
    }

    @Override
    public long srem(byte[][] values) {
        long count = 0;
        if (values == null) {
            return count;
        }
        for (byte[] val : values) {
            String valStr = new String(val);
            if (sets.containsKey(valStr)) {
                sets.remove(valStr);
                count += 1;
            }
        }
        return count;
    }

    @Override
    public void sclear() {
        sets.clear();
    }

    @Override
    public List<byte[]> sunion(byte[][] values) {
        return null;
    }

    @Override
    public int sunionstore(byte[][] values) {
        return 0;
    }

    @Override
    public List<byte[]> sscan(byte[][] values) {
        return null;
    }
}
