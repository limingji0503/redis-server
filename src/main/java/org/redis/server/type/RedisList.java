package org.redis.server.type;

import com.google.common.collect.Lists;
import org.redis.server.typeapi.BinaryRedisDB;
import org.redis.server.typeapi.BinaryRedisList;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * @author roger
 * @date 15/11/13.
 */
public class RedisList extends BinaryRedisList {
    private ConcurrentLinkedDeque<byte[]> queue;

    public RedisList() {
        queue = new ConcurrentLinkedDeque<byte[]>();
    }

    @Override
    public byte[] blpop(long timeout) {
        byte[] val = null;
        long entryTime = System.currentTimeMillis() / 1000;
        while((val = queue.getFirst())!=null) {
            if (System.currentTimeMillis() / 1000 - entryTime > timeout)
                break;
        }
        return val;
    }

    @Override
    public byte[] brpop(long timeout) {
        byte[] val = null;
        long entryTime = System.currentTimeMillis() / 1000;
        while((val = queue.getLast())!=null) {
            if (System.currentTimeMillis() / 1000 - entryTime > timeout)
                break;
        }
        return val;
    }

    @Override
    public List<byte[]> brpopLpush(BinaryRedisList destination, long timeout) {
        return null;
    }

    @Override
    public byte[] lindexOf(int index) {
        if (index < 0 || index > queue.size()) return new byte[0];

        Iterator<byte[]> it = queue.iterator();
        for (int i = 0; it.hasNext(); i ++) {
            byte[] value = it.next();
            if (index == i) return value;
        }
        return new byte[0];
    }

    @Override
    public long linsert(byte[] command, int pivot, byte[] value) {
        return 0;
    }

    @Override
    public long llen() {
        return queue.size();
    }

    @Override
    public byte[] lpop() {
        return queue.pollFirst();
    }

    @Override
    public long lpush(byte[][] values) {
        if (values != null) {
            for (byte[] val : values) {
                queue.addFirst(val);
            }
        }
        return queue.size();
    }

    @Override
    public List<byte[]> lrange(int start, int stop) {
        List<byte[]> result = Lists.newArrayList();
        if (start < 0 || start > stop) return result;
        else if (stop > queue.size()) stop = queue.size();
        else if (stop < 0) stop = queue.size() + stop;

        Iterator<byte[]> it = queue.iterator();
        for(int i = 0; it.hasNext(); i++ ) {
            byte[] val = it.next();
            if (i >= start && i <= stop) {
                result.add(val);
            }
        }
        return result;
    }

    @Override
    public long lrem(int count, byte[] value) {
        Iterator<byte[]> it = queue.iterator();
        int i = 0;
        for (; it.hasNext() && !(i > count) ; ) {
            byte[] val = it.next();
            if (Arrays.equals(val, value)) {
                i ++;
                it.remove();
            }
        }
        return i;
    }

    @Override
    public BinaryRedisDB.OK lset(int index, byte[] value) {
        return null;
    }

    @Override
    public BinaryRedisDB.OK ltrim(int start, int stop) {
        if (start < 0) return new BinaryRedisDB.OK("ERR start suffix out of index.".getBytes());
        stop = (stop > queue.size()) ? queue.size() : stop;

        Iterator<byte[]> it = queue.iterator();
        for (int i = 0; it.hasNext(); i++) {
            it.next();
            if (i > start && i < stop) continue;
            it.remove();
        }
        return new BinaryRedisDB.OK();
    }

    @Override
    public byte[] rpop() {
        return queue.pollLast();
    }

    @Override
    public byte[] rpopLpush(BinaryRedisList destination) {
        return new byte[0];
    }

    @Override
    public long rpush(byte[][] values) {
        if (values != null) {
            for (byte[] val : values)
                queue.addLast(val);
        }
        return queue.size();
    }

    @Override
    public BinaryRedisDB.OK lpushx(byte[][] values) {
        return null;
    }

    @Override
    public BinaryRedisDB.OK rpushx(byte[][] valuess) {
        return null;
    }
}