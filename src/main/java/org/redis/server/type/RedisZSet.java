package org.redis.server.type;

import org.redis.server.typeapi.BinaryRedisZSet;

import java.util.List;
import java.util.Map;

/**
 * @author roger on 15/12/29
 */
public class RedisZSet extends BinaryRedisZSet {

    @Override
    public long zadd(byte[] key, double score, byte[] member) {
        return 0;
    }

    @Override
    public long zadd(byte[] key, Map<Double, byte[]> scoreMembers) {
        return 0;
    }

    @Override
    public long zcard(byte[] key) {
        return 0;
    }

    @Override
    public long zcount(byte[] key, double min, double max) {
        return 0;
    }

    @Override
    public long zcount(byte[] key, byte[] min, byte[] max) {
        return 0;
    }

    @Override
    public double zincrby(byte[] key, double score, byte[] member) {
        return 0;
    }

    @Override
    public List<byte[]> zrange(byte[] key, long start, long end) {
        return null;
    }

    @Override
    public List<byte[]> zrangeByScore(byte[] key, double min, double max) {
        return null;
    }

    @Override
    public List<byte[]> zrangeByScore(byte[] key, byte[] min, byte[] max) {
        return null;
    }

    @Override
    public List<byte[]> zrangeByScore(byte[] key, double min, double max, int offset, int count) {
        return null;
    }

    @Override
    public List<byte[]> zrangeByScore(byte[] key, byte[] min, byte[] max, int offset, int count) {
        return null;
    }

    @Override
    public Long zrank(byte[] key, byte[] member) {
        return null;
    }

    @Override
    public long zrem(byte[] key, byte[]... members) {
        return 0;
    }

    @Override
    public long zremrangeByRank(byte[] key, long start, long end) {
        return 0;
    }

    @Override
    public long zremrangeByScore(byte[] key, double start, double end) {
        return 0;
    }

    @Override
    public long zremrangeByScore(byte[] key, byte[] start, byte[] end) {
        return 0;
    }

    @Override
    public List<byte[]> zrevrange(byte[] key, long start, long end) {
        return null;
    }

    @Override
    public List<byte[]> zrevrangeByScore(byte[] key, double max, double min) {
        return null;
    }

    @Override
    public List<byte[]> zrevrangeByScore(byte[] key, byte[] max, byte[] min) {
        return null;
    }

    @Override
    public List<byte[]> zrevrangeByScore(byte[] key, double max, double min, int offset, int count) {
        return null;
    }

    @Override
    public List<byte[]> zrangeWithScores(byte[] key, int start, int end) {
        return null;
    }

    @Override
    public List<byte[]> zrevrangeWithScores(byte[] key, int start, int end) {
        return null;
    }

    @Override
    public List<byte[]> zrangeByScoreWithScores(byte[] key, double min, double max) {
        return null;
    }

    @Override
    public List<byte[]> zrevrangeByScoreWithScores(byte[] key, double max, double min) {
        return null;
    }

    @Override
    public List<byte[]> zrangeByScoreWithScores(byte[] key, double min, double max, int offset, int count) {
        return null;
    }

    @Override
    public List<byte[]> zrevrangeByScore(byte[] key, byte[] max, byte[] min, int offset, int count) {
        return null;
    }

    @Override
    public List<byte[]> zrangeByScoreWithScores(byte[] key, byte[] min, byte[] max) {
        return null;
    }

    @Override
    public List<byte[]> zrangeByScoreWithScores(byte[] key, byte[] min, byte[] max, int offset, int count) {
        return null;
    }

    @Override
    public List<byte[]> zrevrangeByScoreWithScores(byte[] key, double max, double min, int offset, int count) {
        return null;
    }

    @Override
    public List<byte[]> zrevrangeByScoreWithScores(byte[] key, byte[] max, byte[] min, int offset, int count) {
        return null;
    }

    @Override
    public long zrevrank(byte[] key, byte[] member) {
        return 0;
    }

    @Override
    public double zscore(byte[] key, byte[] member) {
        return 0;
    }

    @Override
    public long zunionstore(byte[] dstkey, String... sets) {
        return 0;
    }

    @Override
    public long zinterstore(byte[] dstkey, String... sets) {
        return 0;
    }

}
