package org.redis.server.rawtype;

import org.redis.server.type.RedisString;

/**
 * Redis String 低层实现之一
 * Created by mingji on 9/18/17.
 */
public class RawString extends RedisString {

    private StringBuffer string;
    private RedisStringEncoding encoding;

    public RawString(){
        this.string   = new StringBuffer("");
        this.encoding = RedisStringEncoding.REDIS_ENCODING_STRING;
    }

    public RawString(RawInteger integer){
        this.string     = new StringBuffer(integer.toString());
        this.encoding   = RedisStringEncoding.REDIS_ENCODING_STRING;
    }

    public RedisStringEncoding getEncoding(){
        return this.encoding;
    }

    @Override
    public int append(byte[] values) {
        this.string = this.string.append(new String(values));
        return this.string.length();
    }
}
