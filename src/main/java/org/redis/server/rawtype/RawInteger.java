package org.redis.server.rawtype;

import org.redis.server.type.RedisString;

import java.util.NoSuchElementException;

/**
 * Redis String 低层实现之一
 * Created by mingji on 9/18/17.
 */

public class RawInteger extends RedisString {

    private Integer integer;
    private RedisStringEncoding encoding;

    public RawInteger() {
        this.integer  = new Integer(0);
        this.encoding = RedisStringEncoding.REDIS_ENCODING_INT;
    }

    public RedisStringEncoding getEncoding() {
        return this.encoding;
    }

    @Override
    public int append(byte[] values) {
        throw new NoSuchElementException();
    }

    @Override
    public int bitcount(byte[][] values) {

        return 0;
    }

    @Override
    public String toString() {
        return this.integer + "";
    }
}
