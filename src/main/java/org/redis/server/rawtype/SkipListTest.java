package org.redis.server.rawtype;

import junit.framework.TestCase;
import org.redis.server.rawtype.SkipList;

public class SkipListTest extends TestCase {

    public static void testSkipList(){
        SkipList skipList = SkipList.zslCreate();
        System.out.println(skipList.toString());
        skipList.zslInsert(4.0, "4.0");
        System.out.println(skipList.toString());
        skipList.zslInsert(3.0, "3.0");
        System.out.println(skipList.toString());
        skipList.zslInsert(2.0, "2.0");
        System.out.println(skipList.toString());
        skipList.zslInsert(1.0, "1.0");
        System.out.println(skipList.toString());

        assertEquals("zsl is in range", true, skipList.zslIsInRange(3.0, "3.0"));

        skipList.zslDelete(3.0, "3.0");
        System.out.println(skipList.toString());

        assertEquals("zsl not in range", false, skipList.zslIsInRange(3.0, "3.0"));
    }
}
