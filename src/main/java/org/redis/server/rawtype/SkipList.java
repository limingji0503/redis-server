package org.redis.server.rawtype;

import org.apache.log4j.Logger;
import org.redis.server.util.TypeUtil;

import java.util.Random;

/**
 * Created by mingji on 9/12/17.
 */

class SkipLevel {
    public SkipNode forward;
    public int span;
    protected SkipLevel(){
        this.forward = null;
        this.span    = 0;
    }
}

class SkipNode {
    public Object object;
    public double source;
    public SkipNode backward;
    public SkipLevel[] level;

    private SkipNode(int level, double source, Object obj){
        this.source = source;
        this.object = obj;
        this.level  = new SkipLevel[level];
        for(int i=0; i < level; i ++){
            this.level[i] = new SkipLevel();
        }
    }

    public static SkipNode zslCreateNode(int level, double source, Object obj){
        return new SkipNode(level, source, obj);
    }

    @Override
    public String toString() {
        return String.format("SkipNode source:%.2f, level:%d", this.source, this.level.length);
    }
}

class SkipRangeSpec {
    double min, max;
    int minEx, maxEx;  /* are min or max exclusive? */
}

public class SkipList {
    public static final int ZSKIPLIST_MAXLEVEL = 6; /* Should be enough for 2^32 elements */

    public static final Random rand = new Random(System.currentTimeMillis());

    private Logger logger = Logger.getLogger(SkipList.class);

    public SkipNode header, tail;
    public int length;
    public int level;

    private SkipList(){
        this.length = 0;
        this.level  = 1;

        this.header = SkipNode.zslCreateNode(ZSKIPLIST_MAXLEVEL, 0, "HEADER ");
        for (int i=0; i < ZSKIPLIST_MAXLEVEL; i ++ ){
            this.header.level[i].forward = null;
            this.header.level[i].span    = 0;
        }
        this.header.backward = null;
        this.tail = null;
    }

    public static SkipList zslCreate(){
        return new SkipList();
    }

    // TODO lmj power law alike distribution 或者采用某种方法来进行生成随机数
    private int zslRandomLevel(){
//      return rand.nextInt(ZSKIPLIST_MAXLEVEL - 1);
        // 这里返回的level至少应该为1.不能为0
        return rand.nextInt(4) + 1;
    }

    public void zslInsert(double source, Object obj){
        SkipNode   currentNode = this.header;
        SkipNode[] update      = new SkipNode[ZSKIPLIST_MAXLEVEL];
        for (int i = 0; i < ZSKIPLIST_MAXLEVEL; i++) {
            update[i] = SkipNode.zslCreateNode(0, 0.0, null);
        }
        int[] rank = new int[ZSKIPLIST_MAXLEVEL];

        // 找到每层应该插入的位置
        for (int i = this.level - 1; i >= 0; i--) {
            /* store rank that is crossed to reach the insert position */
            rank[i] = i == (this.level - 1) ? 0 : rank[i + 1];
            if (currentNode.level[i].forward != null &&
                    (currentNode.level[i].forward.source < source||
                    (currentNode.level[i].forward.source == source && TypeUtil.compareStingObjects(currentNode.level[i].forward.object, obj) < 0 ))) {
                rank[i] += currentNode.level[i].span;
                currentNode = currentNode.level[i].forward;
            }
            update[i] = currentNode;
        }
        int newLevel = this.zslRandomLevel();
        logger.info("随机出来的数字是: " + newLevel);
        // 如果随机出来的层数高于当前跳跃表中的最高层，那么补齐高出来层数的backward
        if(newLevel > this.level) {
            for(int i= this.level; i < newLevel; i ++ ){
                rank[i]   = 0;
                update[i] = this.header;
                update[i].level[i].span = this.length;
            }
            this.level = newLevel;
        }
        // 将newNode插入到现有的SkipList中。
        SkipNode newNode = SkipNode.zslCreateNode(newLevel, source, obj);
        for (int i=0; i < newLevel; i ++ ){
            newNode.level[i].forward   = update[i].level[i].forward;
            update[i].level[i].forward = newNode;

            // update span
            newNode.level[i].span   = update[i].level[i].span - (rank[0] - rank[i]);
            update[i].level[i].span = (rank[0] - rank[i]) + 1;
        }
        // increment span for untouchable levels
        for (int i = newLevel; i < this.level; i++) {
            update[i].level[i].span++;
        }
        // 更新backward
        newNode.backward = update[0] == header ? null : update[0];
        if (newNode.level[0].forward != null) {
            newNode.level[0].forward.backward = newNode;
        } else {
            this.tail = newNode;
        }
        this.length += 1;
    }

    private void zslDeleteNode(SkipNode targetNode, SkipNode[] update) {
        if (targetNode == null) {
            return;
        }
        for (int i = 0; i < this.level; i++) {
            if(update[i].level[i].forward == targetNode ) {
                update[i].level[i].span   += targetNode.level[i].span - 1;
                update[i].level[i].forward = targetNode.level[i].forward;
            } else {
                update[i].level[i].span -= 1;
            }
        }
        if(targetNode.level[0].forward != null){
            targetNode.level[0].forward.backward = targetNode.backward;
        } else {
            this.tail = targetNode.backward;
        }
        // 从上往下删除空白层
        while (this.level > 1 && this.header.level[this.level - 1].forward == null)
            this.level--;
        this.length -= 1;
    }

    public int zslDelete(double source, Object obj){
        SkipNode   curNode  = this.header;
        SkipNode[] update   = new SkipNode[ZSKIPLIST_MAXLEVEL];
        for (int i = 0; i < ZSKIPLIST_MAXLEVEL; i++) {
            update[i] = SkipNode.zslCreateNode(0, 0.0, null);
        }
        // 找到每层要删除节点的前置节点
        for (int i = this.level-1; i >=0; i -- ){
            if (curNode.level[i].forward != null &&
                    (curNode.level[i].forward.source < source ||
                            curNode.level[i].forward.source == source && TypeUtil.compareStingObjects(curNode.level[i].forward.object, obj) < 0)) {
                curNode = curNode.level[i].forward;
            }
            update[i] = curNode;
        }
        curNode = curNode.level[0].forward;
        if (curNode.source == source && TypeUtil.compareStingObjects(curNode.object, obj) == 0){
            this.zslDeleteNode(curNode, update);
            return 1;
        }
        // not found
        return 0;
    }

    public SkipNode zslFind(double source, Object obj){
        if (this.length == 0)
            return null;
        SkipNode curNode = this.header;
        while(curNode != null){
            curNode = curNode.level[0].forward;
            if (curNode.source == source && TypeUtil.compareStingObjects(curNode.object, obj) == 0){
                return curNode;
            }
            if (curNode.source > source || (curNode.source == source && TypeUtil.compareStingObjects(curNode.object, obj) > 0 )){
                return null;
            }
        }
        return null;
    }

    public boolean zslIsInRange(double source, Object obj) {
        return zslFind(source, obj) != null;
    }

    @Override
    public String toString() {
        String formatRet = new String();
        formatRet += String.format("SkipList length: %d, level:%d\n", this.length, this.level);

        for (int i = this.level - 1; i >= 0; i--) {
            SkipNode curNode = this.header;
            // to contains the header
            for (int j = this.length - 1 + 1; j >= 0; j--) {
                if (curNode != null && curNode.level.length > i){
                    formatRet += String.format(" %.2f(%d) ->", curNode.source, curNode.level[i].span);
                } else {
                    formatRet += " ******* ->";
                }
                curNode = curNode.level[0].forward;
            }
            formatRet += '\n';
        }

        SkipNode curNode = this.header;
        for (int j = this.length - 1 + 1; j >= 0; j--) {
            if (curNode != null) {
                formatRet += " " + curNode.object + " ->";
            }
            curNode = curNode.level[0].forward;
        }
        return formatRet+"\n";
    }
}
