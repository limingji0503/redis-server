package example;

import org.redis.server.util.AsyncActionsQueue;
import org.redis.server.util.Go;
import redis.clients.jedis.Jedis;

import java.util.List;

/**
 * @author LuoJie on 2015/11/7
 */
public class RedisClient {
    static final Jedis jedis = new Jedis("localhost", 8080);

    public static void main(String[] args) throws Exception {

        final AsyncActionsQueue<String> queue = new AsyncActionsQueue<String>(1000, 1000) {
            @Override
            public void action(List<String> list) {
                System.out.println(list);
            }
        }.start();

        new Thread() {
            int i = 0;
            public void run() {
                while(true) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                    queue.put("" + (i++));
                }
            }
        }.start();
    }

    public static void test0() throws InterruptedException {
        while(!Thread.currentThread().isInterrupted()) {
            System.out.println(jedis.lpop("roger"));
            Thread.sleep(1000);
        }
    }

    public static void test1() throws Exception {
        Go.startThreadPool();
        Go.x("test-1", RedisClient.class, "test");
        jedis.lpush("roger", "hehe", "haha", "heihei", "houhou");
        System.out.println(jedis.lrange("roger", 0, 12));
        System.out.println(jedis.llen("roger"));
        System.out.println(jedis.rpop("roger"));
        Thread.sleep(1000);
        Go.stopThreadPool();
    }
}
