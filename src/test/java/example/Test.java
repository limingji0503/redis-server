package example;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

/**
 * @author roger on 15/12/29
 * 据说李洺吉会请我吃饭
 */
public class Test {

    public static void main(String[] args) {
        solution("010010");

        List<Interval> intervals = Lists.newArrayList();
        intervals.add(new Interval(4, 6));
        intervals.add(new Interval(3, 3));
        intervals.add(new Interval(1, 2));
        intervals.add(new Interval(5, 10));

        System.out.println(solution3(intervals));
    }

    public static List<Interval> solution3(List<Interval> intervals) {
        while (true) {
            int operate = 0;
            for (int j = 0; j < intervals.size() - 1 && j >= 0; j++) {
                Interval pre = intervals.get(j);
                Interval after = intervals.get(j + 1);
                if (pre.end < after.start || after.end < pre.start) {
                    if (pre.start > after.start) {
                        intervals.set(j, after);
                        intervals.set(j + 1, pre);
                        operate++;
                    }
                    continue;
                }
                if (pre.end < after.end) {
                    pre.end = after.end;
                }
                if (pre.start >= after.start) {
                    pre.start = after.start;
                }
                intervals.remove(j + 1);
                operate++;
                j--;
            }
            if (operate == 0) {
                break;
            }
        }
        return intervals;
    }

    public static List<Interval> solution2(List<Interval> intervals) {
        sortting(intervals, 0, intervals.size() - 1);
        int len = intervals.size();
        for (int i = 0; i < len; i++) {
            if (i == len - 1) break;
            Interval pre = intervals.get(i);
            Interval after = intervals.get(i + 1);
            if (pre.end >= after.start) {
                pre.end = pre.end > after.end ? pre.end : after.end;
                intervals.remove(i + 1);
                i--;
                len = intervals.size();
            }
        }
        return intervals;
    }

    public static void sortting(List<Interval> intervals, int i, int j) {
        if (i>=j) return;
        int offset = qsort(intervals, i, j);
        sortting(intervals, i, offset);
        sortting(intervals, offset + 1, j);
    }

    public static int qsort(List<Interval> intervals, int i, int j) {
        int val = intervals.get(i).start;
        while (i < j) {
            while (i < j && intervals.get(j).start >= val) j--;
            while (i < j && intervals.get(i).start < val) i++;
            Interval interval = intervals.get(i);
            intervals.set(i, intervals.get(j));
            intervals.set(j, interval);
        }
        return i;
    }

    public static class Interval {
        int start;
        int end;

        public Interval() {

        }

        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public String toString() {
            return "[" + start + ", " + end + "]";
        }
    }

    public static List<String> solution(String s) {
        int offset = 0;
        final int STATE_A = 0;
        final int STATE_B = 1;
        final int STATE_C = 2;
        final int STATE_D = 3;
        final int STATE_EXIT = -1;
        int a = 1, b = 1, c = 1;
        int len = s.length();

        int CURRENT_STATE = STATE_A;
        List<String> list = new ArrayList<>();
        while (CURRENT_STATE != STATE_EXIT) {

            switch (CURRENT_STATE) {
                case STATE_A : {
                    if (a > 3 || len <= offset + a) {
                        CURRENT_STATE = STATE_EXIT;
                    } else if (offset > len) {
                        CURRENT_STATE = STATE_EXIT;
                    } else {
                        if (len - a > 9) {
                            a++;
                        } else {
                            boolean isException = false;
                            if (s.charAt(offset) == '0' && a > 1) {
                                isException = true;
                            } else if (a == 3) {
                                if (s.charAt(offset) > '2')
                                    isException = true;
                                else if (s.charAt(offset) == '2' && s.charAt(offset + 1) > '5')
                                    isException = true;
                                else if (s.charAt(offset) == '2' && s.charAt(offset + 1) == '5'
                                        && s.charAt(offset + 2) > '5') isException = true;
                            }
                            if (isException) {
                                CURRENT_STATE = STATE_EXIT;
                                break;
                            }
                            offset += a;
                            CURRENT_STATE = STATE_B;
                        }
                    }
                    break;
                }
                case STATE_B : {
                    if (b > 3 || len <= offset + b) {
                        b = 1;
                        offset -= a;
                        a++;
                        CURRENT_STATE = STATE_A;
                    } else if (offset > len) {
                        CURRENT_STATE = STATE_EXIT;
                    } else {
                        if (len - b - a > 6) {
                            b++;
                        } else {
                            boolean isException = false;
                            if (s.charAt(offset) == '0' && b > 1) {
                                isException = true;
                            } else if (b == 3) {
                                if (s.charAt(offset) > '2')
                                    isException = true;
                                else if (s.charAt(offset) == '2' && s.charAt(offset + 1) > '5')
                                    isException = true;
                                else if (s.charAt(offset) == '2' && s.charAt(offset + 1) == '5'
                                        && s.charAt(offset + 2) > '5') isException = true;
                            }
                            if (isException) {
                                b = 1;
                                offset -= a;
                                a++;
                                CURRENT_STATE = STATE_A;
                                break;
                            }
                            offset +=  b;
                            CURRENT_STATE = STATE_C;
                        }
                    }
                    break;
                }
                case STATE_C : {
                    if (c > 3 || len <= offset + c) {
                        c = 1;
                        offset -= b;
                        b++;
                        CURRENT_STATE = STATE_B;
                    } else if (offset > len) {
                        CURRENT_STATE = STATE_EXIT;
                    } else {
                        if (len - a - b - c > 3) {
                            c++;
                        } else {
                            boolean isException = false;
                            if (s.charAt(offset) == '0' && c > 1) {
                                isException = true;
                            } else if (c == 3) {
                                if (s.charAt(offset) > '2')
                                    isException = true;
                                else if (s.charAt(offset) == '2' && s.charAt(offset + 1) > '5')
                                    isException = true;
                                else if (s.charAt(offset) == '2' && s.charAt(offset + 1) == '5'
                                        && s.charAt(offset + 2) > '5') isException = true;
                            }
                            if (isException) {
                                c = 1;
                                offset -= b;
                                b++;
                                CURRENT_STATE = STATE_B;
                                break;
                            }
                            offset += c;
                            CURRENT_STATE = STATE_D;
                        }
                    }
                    break;
                }
                case STATE_D : {
                    boolean isException = false;
                    if (s.charAt(offset) == '0' && len - offset > 1) {
                        isException = true;
                    } else if (len - offset == 3) {
                        if (s.charAt(offset) > '2')
                            isException = true;
                        else if (s.charAt(offset) == '2' && s.charAt(offset + 1) > '5')
                            isException = true;
                        else if (s.charAt(offset) == '2' && s.charAt(offset + 1) == '5'
                                && s.charAt(offset + 2) > '5') isException = true;
                    }
                    if (isException) {
                        offset -= c;
                        c++;
                        CURRENT_STATE = STATE_C;
                        break;
                    }
                    if (len - offset <= 3 && len > offset) {
                        char[] e = new char[len + 3];
                        for (int i = 0, j = 0; i < len + 3 && j < len; i++) {
                            if (j == a || j == (a + b) || j == (a + b + c)) {
                                e[i] = '.';
                                e[++i] = s.charAt(j++);
                                continue;
                            }
                            e[i] = s.charAt(j++);
                        }
                        list.add(new String(e));
                    }
                    offset -= c;
                    c++;
                    CURRENT_STATE = STATE_C;
                    break;
                }
            }
        }
        return list;
    }
}
